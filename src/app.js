import bodyParser from 'body-parser'
import cors from 'cors'
import express from 'express'
import { NOT_FOUND, OK } from 'http-status'
import jwt from 'jsonwebtoken'
import morgan from 'morgan'
import { secretToken } from './config/secret-token'

process.on('unhandledRejection', (reason: string) => {
  console.error('Reason: ' + reason)
})

const routes = require('./routes')
console.log(secretToken)
const app = express()
const router = express.Router()

app.use(morgan('dev'))

app.use(bodyParser.json())

app.use(bodyParser.urlencoded({ extended: false }))

app.use(cors())

app.use((request, _, next) => {
  if (request.headers && request.headers.authorization) {
    jwt.verify(request.headers.authorization.split(' ')[0], 'unaKonsulta888!?', (error, decode) => {
      if (error) request.user = undefined
      request.user = decode
      next()
    })
  } else {
    request.user = undefined
    next()
  }
})

routes(app, router)

app
  .get('/', function (_, response) {
    return response
      .status(OK)
      .send({
        message: 'Welcome to the Root'
      })
  })

app
  .get('/api', function (_, response) {
    return response
      .status(OK)
      .send({
        message: 'Welcome to the REST API of Hemodialysis'
      })
  })

app
  .get('*', function (_, response) {
    return response
      .status(NOT_FOUND)
      .send({
        message: 'Page not found'
      })
  })

app.use(function (err, req, res, next) {
  const error = err.status || 500
  res.status(error).send({
    message: err.message
  })
})

module.exports = app
