'use strict'

let Patient = require('../../models').Patient
let TemplateMedication = require('../../models').TemplateMedication
let notFoundException = require('../../helpers/exceptions/not-found')

module.exports = {
  index (request, response) {
    return Patient
      .findById(request.params.patientId)
      .then(
        function (patient) {
          if (!patient) {
            notFoundException()
          }

          return patient
            .getTemplateMedications()
            .then(
              function (standingOrderMedications) {
                return response.status(200).send({
                  data: standingOrderMedications
                })
              },

              function (error) {
                return response.status(400).send({
                  message: error.message
                })
              }
            )
        },

        function (error) {
          return response.status(400).send({
            message: error.message
          })
        }
      )
  },

  select (request, response) {
    return TemplateMedication
      .findById(request.params.standingOrderMedicationId)
      .then(
        function (standingOrderMedication) {
          return response.status(200).send({
            data: standingOrderMedication
          })
        },
        function (error) {
          return response.status(400).send({
            message: error.message
          })
        }
      )
  },

  create (request, response) {
    return Patient
      .findById(request.params.patientId)
      .then(
        function (patient) {
          if (!patient) {
            notFoundException()
          }

          return patient
            .createTemplateMedication({
              patient_id: request.params.patientId,
              generic: request.body.generic,
              brand: request.body.brand,
              preparation: request.body.preparation,
              timing: request.body.timing,
              dosage: request.body.dosage,
              route: request.body.route,
              created_by: request.user.user_id,
              updated_by: request.user.user_id
            })
            .then(
              function (standingOrderMedication) {
                return response.status(201).send({
                  data: standingOrderMedication
                })
              },

              function (error) {
                return response.status(400).send({
                  message: error.message
                })
              }
            )
        },

        function (error) {
          return response.status(400).send({
            message: error.message
          })
        }
      )
  },

  update (request, response) {
    return TemplateMedication
      .findById(request.params.standingOrderMedicationId)
      .then(
        function (standingOrderMedication) {
          if (!standingOrderMedication) {
            notFoundException()
          }

          return standingOrderMedication
            .update(request.body, { fields: Object.keys(request.body) })
            .then(
              function () {
                return response.status(200).send({ data: standingOrderMedication })
              },
              function (error) {
                return response.status(400).send({
                  message: error.message
                })
              }
            )
        },
        function (error) {
          return response.status(400).send({
            message: error.message
          })
        }
      )
  },

  destroy (request, response) {
    return TemplateMedication
      .findById(request.params.standingOrderMedicationId)
      .then(
        function (standingOrderMedication) {
          if (!standingOrderMedication) {
            notFoundException()
          }

          return standingOrderMedication
            .destroy()
            .then(
              function () {
                return response.status(200).send({
                  data: standingOrderMedication
                })
              },

              function (error) {
                return response.status(400).send({
                  message: error.message
                })
              }
            )
        },

        function (error) {
          return response.status(400).send({
            message: error.message
          })
        }
      )
  }
}
