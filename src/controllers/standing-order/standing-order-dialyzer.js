'use strict'

let Patient = require('../../models').Patient
let DialyzerBrand = require('../../models').DialyzerBrand
let DialyzerSize = require('../../models').DialyzerSize
let TemplateDialyzer = require('../../models').TemplateDialyzer
let notFoundException = require('../../helpers/exceptions/not-found')

module.exports = {

  index (request, response) {
    return Patient
      .findById(request.params.patientId)
      .then(
        function (patient) {
          if (!patient) {
            notFoundException()
          }

          return patient
            .getTemplateDialyzers({
              where: {
                patient_id: request.params.patientId
              },
              include: [
                {
                  model: DialyzerBrand,
                  as: 'dialyzerBrand',
                  attributes: [
                    'dialyzer_brand_id',
                    'brand_name',
                    'description'
                  ]
                },
                {
                  model: DialyzerSize,
                  as: 'dialyzerSize',
                  attributes: [
                    'dialyzer_size_id',
                    'size_name',
                    'description'
                  ]
                }
              ]
            })
            .then(
              function (standingOrderDialyzers) {
                return response.status(200).send({
                  data: standingOrderDialyzers
                })
              },

              function (error) {
                return response.status(400).send({
                  message: error.message
                })
              }
            )
        },

        function (error) {
          return response.status(400).send({
            message: error.message
          })
        }
      )
  },

  select (request, response) {
    return TemplateDialyzer
      .findById(request.params.standingOrderDialyzerId, {
        include: [
          {
            model: DialyzerBrand,
            as: 'dialyzerBrand',
            attributes: [
              'dialyzer_brand_id',
              'brand_name',
              'description'
            ]
          },
          {
            model: DialyzerSize,
            as: 'dialyzerSize',
            attributes: [
              'dialyzer_size_id',
              'size_name',
              'description'
            ]
          }
        ]
      })
      .then(
        function (standingOrderDialyzer) {
          return response.status(200).send({
            data: standingOrderDialyzer
          })
        },

        function (error) {
          return response.status(400).send({
            message: error.message
          })
        }
      )
  },

  create (request, response) {
    return Patient
      .findById(request.params.patientId)
      .then(
        function (patient) {
          if (!patient) {
            notFoundException()
          }

          return patient
            .createTemplateDialyzer({
              patient_id: request.params.patientId,
              dialyzer_brand_id: request.body.dialyzer_brand_id,
              type: request.body.type,
              dialyzer_size_id: request.body.dialyzer_size_id,
              dialyzer_date: request.body.dialyzer_date,
              count: request.body.count,
              d_t: request.body.d_t,
              created_by: request.user.user_id,
              updated_by: request.user.user_id
            })
            .then(
              function (standingOrderDialyzer) {
                return TemplateDialyzer
                  .findById(standingOrderDialyzer.template_dialyzer_id, {
                    include: [
                      {
                        model: Patient,
                        as: 'patient'
                      },
                      {
                        model: DialyzerBrand,
                        as: 'dialyzerBrand',
                        attributes: [
                          'dialyzer_brand_id',
                          'brand_name',
                          'description'
                        ]
                      },
                      {
                        model: DialyzerSize,
                        as: 'dialyzerSize',
                        attributes: [
                          'dialyzer_size_id',
                          'size_name',
                          'description'
                        ]
                      }
                    ]
                  })
                  .then(
                    function (newStandingOrderDialyzer) {
                      return response.status(201).send({
                        data: newStandingOrderDialyzer
                      })
                    },

                    function (error) {
                      return response.status(400).send({
                        message: error.message
                      })
                    }
                  )
              },

              function (error) {
                return response.status(400).send({
                  message: error.message
                })
              }
            )
        },

        function (error) {
          return response.status(400).send({
            message: error.message
          })
        }
      )
  },

  update (request, response) {
    return TemplateDialyzer
      .findById(request.params.standingOrderDialyzerId)
      .then(
        function (standingOrderDialyzer) {
          if (!standingOrderDialyzer) {
            notFoundException()
          }

          return standingOrderDialyzer
            .update(request.body, { fields: Object.keys(request.body) })
            .then(
              function () {
                return TemplateDialyzer
                  .findById(standingOrderDialyzer.template_dialyzer_id, {
                    include: [
                      {
                        model: DialyzerBrand,
                        as: 'dialyzerBrand',
                        attributes: [
                          'dialyzer_brand_id',
                          'brand_name',
                          'description'
                        ]
                      },
                      {
                        model: DialyzerSize,
                        as: 'dialyzerSize',
                        attributes: [
                          'dialyzer_size_id',
                          'size_name',
                          'description'
                        ]
                      }
                    ]
                  })
                  .then(
                    function () {
                      return response.status(200).send({ data: standingOrderDialyzer })
                    },

                    function (error) {
                      return response.status(400).send({
                        message: error.message
                      })
                    }
                  )
              },

              function (error) {
                return response.status(400).send({
                  message: error.message
                })
              }
            )
        },
        function (error) {
          return response.status(400).send({
            message: error.message
          })
        }
      )
  },

  destroy (request, response) {
    return TemplateDialyzer
      .findById(request.params.standingOrderDialyzerId)
      .then(
        function (standingOrderDialyzer) {
          if (!standingOrderDialyzer) {
            notFoundException()
          }

          return standingOrderDialyzer
            .destroy()
            .then(
              function () {
                return response.status(200).send({
                  data: standingOrderDialyzer
                })
              },

              function (error) {
                return response.status(400).send({
                  message: error.message
                })
              }
            )
        },

        function (error) {
          return response.status(400).send({
            message: error.message
          })
        }
      )
  }
}
