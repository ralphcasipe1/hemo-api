'use strict'

let Patient = require('../../models').Patient
let TemplateParameter = require('../../models').TemplateParameter
let notFoundException = require('../../helpers/exceptions/not-found')

module.exports = {
  index (request, response) {
    return Patient
      .findById(request.params.patientId)
      .then(
        function (patient) {
          return patient
            .getTemplateParameters()
            .then(
              function (standingOrderParameters) {
                return response
                  .status(200)
                  .send({
                    data: standingOrderParameters
                  })
              },

              function (error) {
                return response
                  .status(400)
                  .send({
                    message: error.message
                  })
              }
            )
        },

        function (error) {
          return response.status(400).send({
            message: error.message
          })
        }
      )
  },

  select (request, response) {
    return TemplateParameter
      .findById(request.params.standingOrderParameterId)
      .then(
        function (standingOrderParameter) {
          return response.status(200).send({
            data: standingOrderParameter
          })
        },
        function (error) {
          return response.status(400).send({
            message: error.message
          })
        }
      )
  },

  create (request, response) {
    return Patient
      .findById(request.params.patientId)
      .then(
        function (patient) {
          notFoundException(patient)

          return patient
            .createTemplateParameter({
              patient_id: request.params.patientId,
              duration: request.body.duration,
              blood_flow_rate: request.body.blood_flow_rate,
              dialysate_bath: request.body.dialysate_bath,
              dialysate_additive: request.body.dialysate_additive,
              dialysate_flow_rate: request.body.dialysate_flow_rate,
              created_by: request.user.user_id,
              updated_by: request.user.user_id
            })
            .then(
              function (standingOrderParameter) {
                return response.status(201).send({
                  data: standingOrderParameter
                })
              },

              function (error) {
                return response.status(400).send({
                  message: error.message
                })
              }
            )
        },

        function (error) {
          return response.status(400).send({
            message: error.message
          })
        }
      )
  },

  update (request, response) {
    return TemplateParameter
      .findById(request.params.standingOrderParameterId)
      .then(
        function (standingOrderParameter) {
          if (!standingOrderParameter) {
            notFoundException()
          }

          return standingOrderParameter
            .update(request.body, { fields: Object.keys(request.body) })
            .then(
              function () {
                return response.status(200).send({ data: standingOrderParameter })
              },
              function (error) {
                return response.status(400).send({
                  message: error.message
                })
              }
            )
        },
        function (error) {
          return response.status(400).send({
            message: error.message
          })
        }
      )
  },

  destroy (request, response) {
    return TemplateParameter
      .findById(request.params.standingOrderParameterId)
      .then(
        function (standingOrderParameter) {
          if (!standingOrderParameter) {
            notFoundException()
          }

          return standingOrderParameter
            .destroy()
            .then(
              function () {
                return response.status(200).send({
                  data: standingOrderParameter
                })
              },
              function (error) {
                return response.status(400).send({
                  message: error.message
                })
              }
            )
        },

        function (error) {
          return response.status(400).send({
            message: error.message
          })
        }
      )
  }
}
