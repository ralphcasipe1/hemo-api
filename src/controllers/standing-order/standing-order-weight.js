import { OK } from 'http-status'

let Patient = require('../../models').Patient
let TemplateWeight = require('../../models').TemplateWeight
let notFoundException = require('../../helpers/exceptions/not-found')

module.exports = {
  index (request, response, next) {
    Patient
      .findById(request.params.patientId)
      .then(patient => {
        if (!patient) {
          notFoundException()
        }

        return patient
          .getTemplateWeights()
          .then(standingOrderWeights => response.statu(OK).send({ data: standingOrderWeights }))
      }
      )
      .catch(error => next(error))
  },

  select (request, response, next) {
    TemplateWeight
      .findById(request.params.standingOrderWeightId)
      .then(standingOrderWeight => {
        if (!standingOrderWeight) {
          notFoundException()
        }

        return response.status(200).send({
          data: standingOrderWeight
        })
      })
      .catch(error => next(error))
  },

  create (request, response, next) {
    Patient
      .findById(request.params.patientId)
      .then(patient => {
        if (!patient) {
          notFoundException()
        }
        return patient
          .createTemplateWeight({
            patient_id: request.params.patientId,
            dry_weight: request.body.dry_weight,
            created_by: request.user.user_id,
            updated_by: request.user.user_id
          })
          .then(standingOrderWeight => response.status(201).send({ data: standingOrderWeight }))
      }
      )
      .catch(error => next(error))
  },

  update (request, response, next) {
    TemplateWeight
      .findById(request.params.standingOrderWeightId)
      .then(standingOrderWeight => {
        if (!standingOrderWeight) {
          notFoundException()
        }

        return standingOrderWeight
          .update(request.body, { fields: Object.keys(request.body) })
          .then(() => response.status(200).send({ data: standingOrderWeight }))
      })
      .catch(error => next(error))
  },

  destroy (request, response, next) {
    TemplateWeight
      .findById(request.params.standingOrderWeightId)
      .then(standingOrderWeight => {
        if (!standingOrderWeight) {
          notFoundException()
        }

        return standingOrderWeight
          .destroy()
          .then(() => response.status(200).send({ data: standingOrderWeight }))
      })
      .catch(error => response.status(400).send({ message: error.message }))
  }

}
