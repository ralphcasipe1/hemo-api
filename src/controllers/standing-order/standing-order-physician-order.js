'use strict'

let Patient = require('../../models').Patient
let TemplatePhysicianOrder = require('../../models').TemplatePhysicianOrder
let notFoundException = require('../../helpers/exceptions/not-found')

module.exports = {
  index (request, response) {
    return Patient
      .findById(request.params.patientId)
      .then(
        function (patient) {
          notFoundException(patient)

          return patient
            .getTemplatePhysicianOrders()
            .then(
              function (standingOrderPhysicianOrders) {
                return response
                  .status(200)
                  .send({
                    data: standingOrderPhysicianOrders
                  })
              },
              function (error) {
                return response
                  .status(400)
                  .send({
                    message: error.message
                  })
              }
            )
        },
        function (error) {
          return response.status(400).send({
            message: error.message
          })
        }
      )
  },

  select (request, response) {
    return TemplatePhysicianOrder
      .findById(request.params.standingOrderPhysicianOrderId)
      .then(
        function (standingOrderPhysicianOrder) {
          notFoundException(standingOrderPhysicianOrder)
          return response
            .status(200)
            .send({
              data: standingOrderPhysicianOrder
            })
        },
        function (error) {
          return response.status(400).send({
            message: error.message
          })
        }
      )
  },

  create (request, response) {
    return Patient
      .findById(request.params.patientId)
      .then(
        function (patient) {
          return patient
            .createTemplatePhysicianOrder()
            .then(
              function (standingOrderPhysicianOrder) {
                return response.status(201).send({
                  data: standingOrderPhysicianOrder
                })
              },

              function (error) {
                return response.status(400).send({
                  message: error.message
                })
              }
            )
        },

        function (error) {
          return response.status(400).send({
            message: error.message
          })
        }
      )
  },

  update (request, response) {
    return TemplatePhysicianOrder
      .findById(request.params.standingOrderPhysicianOrderId)
      .then(
        function (standingOrderPhysicianOrder) {
          notFoundException(standingOrderPhysicianOrder)

          return standingOrderPhysicianOrder
            .update(request.body, { fields: Object.keys(request.body) })
            .then(
              function () {
                return response.status(200).send({ data: standingOrderPhysicianOrder })
              },
              function (error) {
                return response.status(400).send({
                  message: error.message
                })
              }
            )
        },
        function (error) {
          return response.status(400).send({
            message: error.message
          })
        }
      )
  },

  destroy (request, response) {
    return TemplatePhysicianOrder
      .findById(request.params.standingOrderPhysicianOrderId)
      .then(
        function (standingOrderPhysicianOrder) {
          notFoundException(standingOrderPhysicianOrder)
          return standingOrderPhysicianOrder
            .destroy()
            .then(
              function () {
                return response.status(200).send({
                  data: standingOrderPhysicianOrder
                })
              },

              function (error) {
                return response.status(400).send({
                  message: error.message
                })
              }
            )
        },

        function (error) {
          return response.status(400).send({
            message: error.message
          })
        }
      )
  }
}
