import { create } from './create'
import { destroy } from './destroy'
import { findAll } from './find-all'
import { findOne } from './find-one'
import { update } from './update'

export const standingOrderAntiCoagulant = {
  create,
  destroy,
  index: findAll,
  select: findOne,
  update
}
