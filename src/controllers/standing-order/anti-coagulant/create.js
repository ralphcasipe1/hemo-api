import { CREATED } from 'http-status'

const Patient = require('../../../models').Patient
const notFoundException = require('../../../helpers/exceptions/not-found')

export const create = (request, response, next) => {
  return Patient
    .findById(request.params.patientId)
    .then(patient => {
      if (!patient) {
        notFoundException()
      }
      return patient
        .createTemplateAntiCoagulant({
          patient_id: request.params.patientId,
          nss_flushing: request.body.nss_flushing,
          nss_flushing_every: request.body.nss_flushing_every,
          lmwh_iv: request.body.lmwh_iv,
          lmwh_iv_iu: request.body.lmwh_iv_iu,
          ufh_iv: request.body.ufh_iv,
          ufh_iv_iu: request.body.ufh_iv_iu,
          ufh_iv_iu_every: request.body.ufh_iv_iu_every,
          bleeding_desc: request.body.bleeding_desc,
          created_by: request.user.user_id,
          updated_by: request.user.user_id
        })
        .then(standingOrderAntiCoagulant => response.status(CREATED).send({
          data: standingOrderAntiCoagulant
        }))
	  })
    .catch(error => next(error))
}
