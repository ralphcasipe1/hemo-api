import { OK } from 'http-status'

const Patient = require('../../../models').Patient
const notFoundException = require('../../../helpers/exceptions/not-found')

export const findAll = (request, response, next) => (
  Patient
    .findById(request.params.patientId)
    .then(patient => {
      if (!patient) {
        notFoundException()
      }

      return patient
        .getTemplateAntiCoagulants()
        .then(standingOrderAntiCoagulants => (
      		response.status(OK).send({
            data: standingOrderAntiCoagulants
          })
        ))
    })
    .catch(error => next(error))
)
