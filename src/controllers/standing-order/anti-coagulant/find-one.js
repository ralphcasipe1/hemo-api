import { OK } from 'http-status'

const TemplateAntiCoagulant = require('../../../models').TemplateAntiCoagulant
const notFoundException = require('../../../helpers/exceptions/not-found')

export const findOne = (request, response, next) => {
  return TemplateAntiCoagulant
    .findById(request.params.standingOrderAntiCoagulantId)
    .then(standingOrderAntiCoagulant => {
      if (!standingOrderAntiCoagulant) {
        notFoundException()
      }

      return response.status(OK).send({
        data: standingOrderAntiCoagulant
      })
    })
    .catch(error => next(error))
}
