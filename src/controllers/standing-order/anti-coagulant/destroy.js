import { OK } from 'http-status'

const TemplateAntiCoagulant = require('../../../models').TemplateAntiCoagulant
const notFoundException = require('../../../helpers/exceptions/not-found')

export const destroy = (request, response, next) => {
  return TemplateAntiCoagulant
    .findById(request.params.standingOrderAntiCoagulantId)
    .then(standingOrderAntiCoagulant => {
      if (!standingOrderAntiCoagulant) {
        notFoundException()
      }

      return standingOrderAntiCoagulant
        .destroy()
        .then(() => response.status(OK).send({
          data: standingOrderAntiCoagulant
        }))
    })
    .catch(error => next(error))
}
