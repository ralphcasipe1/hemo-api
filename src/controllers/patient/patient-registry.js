const Parameter = require('../../models').Parameter
const Patient = require('../../models').Patient
const PatientRegistry = require('../../models').PatientRegistry
const notFoundException = require('../../helpers/exceptions/not-found')

module.exports = {

  index (request, response) {
    return PatientRegistry
      .findAll({
        include: [
          {
            model: Parameter,
            as: 'parameter'
          },
          {
            model: Patient,
            as: 'patient'
          }
        ],
        limit: 15,
        where: {
          patient_id: request.params.patientId
        },
        order: [['created_at', 'DESC']]
      })
      .then(
        function (patientRegistry) {
          return response.status(200).send({
            data: patientRegistry
          })
        },

        function (error) {
          return response.status(400).send({
            message: error.message
          })
        }
      )
  },

  select (request, response) {
    return PatientRegistry
      .findById(request.params.patientRegistryId, {
        include: [
          {
            model: Parameter,
            as: 'parameter'
          },
          {
            model: Patient,
            as: 'patient'
          }
        ]
      })
      .then(
        function (patientRegistry) {
          if (!patientRegistry) {
            notFoundException()
          }

          return response.status(200).send({
            data: patientRegistry
          })
        },

        function (error) {
          return response.status(400).send({
            message: error.message
          })
        }
      )
  },

  create (request, response) {
    return PatientRegistry
      .findOrCreate({
        where: {
          registry_no: request.body.registry_no.toString()
        },
        defaults: {
          patient_id: request.params.patientId,
          created_by: request.user.user_id,
          updated_by: request.user.user_id
        }
      })
      .spread(
        function (patientRegistry, isCreate) {
          if (isCreate) {
            return response.status(200).send({
              exist: false,
              data: patientRegistry
            })
          }

          return response.status(201).send({
            message: 'Patient registry already exists',
            exist: true,
            data: patientRegistry
          })
        },

        function (error) {
          return response.status(400).send({
            message: error.message
          })
        }
      )
  }
}
