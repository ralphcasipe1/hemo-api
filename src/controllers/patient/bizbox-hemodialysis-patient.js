'use strict'

let sequelize = require('../../config/bizbox-config')
let Patient = require('../../models').Patient

module.exports = {

  index (request, response) {
    return sequelize
      .query(
        `	SELECT PK_psPatRegisters
					,	a.FK_emdPatients
					,	a.registrydate
					,	registrystatus
					,	dischdate
					,	f.lastname
					,	f.firstname
					,	f.middlename
					,	dbo.udf_GetPatID(a.FK_emdPatients) HospitalNo
					,	b.FK_psRooms as RoomNo
					,	c.description as NrsStation
					,	d.gender
					,	d.civilstatus
					,	dbo.udf_GetPrimaryAttendDoctor(a.PK_psPatRegisters) AS AttendingDoctor
					, dbo.udf_GetPrimaryAdmitDoctor(a.PK_psPatRegisters) AS AdmittingDoctor
					,	(SELECT TOP 1 cc.PK_psDatacenter
              
              FROM emddoctors aa 
              
              INNER JOIN psdctrledgers bb 
              	ON aa.pk_emddoctors = bb.fk_emddoctors        
              INNER JOIN psdatacenter cc 
              	ON aa.pk_emddoctors = cc.pk_psdatacenter     
              WHERE bb.fk_pspatregisters = a.PK_psPatRegisters 
              	AND bb.fk_emdconsultanttypes = 1002
          ) AS dr_id
          
          FROM psPatRegisters a
          
          LEFT JOIN psAdmissions b
          	ON a.PK_psPatRegisters = b.FK_psPatRegisters
          LEFT JOIN mscNrstation c 
          	ON b.FK_mscNrstation = c.PK_mscNrstation
          LEFT JOIN psPersonaldata d
          	ON d.PK_psPersonalData = a.FK_emdPatients
          INNER JOIN (
          	SELECT fk_pspatregisters
          	
          	FROM pspatitem t2
          	
          	WHERE FK_mscItemCategory = 1009
          		AND  (renqty-retqty) > 0
          ) e
          	ON a.PK_psPatRegisters = e.FK_psPatRegisters
          LEFT JOIN psPersonaldata f
          ON a.FK_emdPatients = f.PK_psPersonalData
          
          WHERE a.pattrantype = 'O' 
	          AND   (registrystatus <> 'X')
	          AND   registrydate Between '${request.params.date} 00:00:00' and '${request.params.date} 23:59:59'
	          AND   a.FK_emdPatients <> 1002
          
          ORDER BY f.lastname`,
        {
        	type: sequelize.QueryTypes.SELECT
        }
      )
      .then(
        function (bizboxPatient) {
          return Patient
            .findAll()
            .then(
              function (localPatients) {
                return response.status(200).send({
                  data: bizboxPatient
                    .filter(
                      function (bizboxPatient) {
                        return localPatients
                          .map(
                            function (localPatient) {
                              return +localPatient.bizbox_patient_no
                            }
                          )
                          .indexOf(bizboxPatient.FK_emdPatients) === -1
                      }
                    )
                })
              },
              function (error) {
                return response.status(400).send({
                  message: error.message
                })
              }
            )
        },

        function (error) {
          return response.status(400).send({
            message: error.message
          })
        }
      )
  },

  select (request, response) {
    sequelize.query(
      `
        SELECT top 1 * FROM
          ( SELECT PK_psPatRegisters
          , a.FK_emdPatients
          , a.registrydate
          , registrystatus
          , dischdate
          , f.lastname
          , f.firstname
          , f.middlename
          , dbo.udf_GetPatID(a.FK_emdPatients) HospitalNo
          , b.FK_psRooms as RoomNo
          , c.description as NrsStation
          , d.gender
          , d.civilstatus
          , dbo.udf_GetPrimaryAttendDoctor(a.PK_psPatRegisters) AS AttendingDoctor
          , dbo.udf_GetPrimaryAdmitDoctor(a.PK_psPatRegisters) AS AdmittingDoctor
          , (SELECT TOP 1 cc.PK_psDatacenter
              
              FROM emddoctors aa 
              
              INNER JOIN psdctrledgers bb 
                ON aa.pk_emddoctors = bb.fk_emddoctors        
              INNER JOIN psdatacenter cc 
                ON aa.pk_emddoctors = cc.pk_psdatacenter     
              WHERE bb.fk_pspatregisters = a.PK_psPatRegisters 
                AND bb.fk_emdconsultanttypes = 1002
          ) AS dr_id

          FROM psPatRegisters a

          LEFT JOIN psAdmissions b
            ON a.PK_psPatRegisters = b.FK_psPatRegisters
          LEFT JOIN mscNrstation c
            ON b.FK_mscNrstation = c.PK_mscNrstation
          LEFT JOIN psPersonaldata d
            ON d.PK_psPersonalData = a.FK_emdPatients
          INNER JOIN (
              SELECT fk_pspatregisters

              FROM pspatitem t2
              
              WHERE FK_mscItemCategory = 1009
                AND  (renqty-retqty) > 0
          ) e
            ON a.PK_psPatRegisters = e.FK_psPatRegisters
          LEFT JOIN psPersonaldata f
            ON a.FK_emdPatients = f.PK_psPersonalData
          
          WHERE a.pattrantype = 'O'
            AND   (registrystatus <> 'X')
            AND   a.FK_emdPatients <> 1002
            AND   a.FK_emdPatients= :patientId) AS patient
        WHERE dr_id is not null
        ORDER BY patient.registrydate DESC
      `,
      {
        replacements: {
          patientId: request.params.patientId
        },
        type: sequelize.QueryTypes.SELECT
      }
    )
      .then(
        (bizboxPatients) => response.status(200).send({
          data: bizboxPatients.length === 0
            ? {}
            : bizboxPatients.reduce((bizboxPatient) => bizboxPatient)
        }),
        (error) => response.status(400).send({
          message: error.message
        })
      )
  }
}
