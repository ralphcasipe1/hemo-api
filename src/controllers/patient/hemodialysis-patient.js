'use strict'

import type { $Request, $Response, NextFunction } from 'express'
import { CREATED, OK } from 'http-status'
import _ from 'lodash'
import { Op } from 'sequelize'

const Patient = require('../../models').Patient
const PatientRegistry = require('../../models').PatientRegistry
const StandingOrderAntiCoagulant = require('../../models').TemplateAntiCoagulant
const StandingOrderDialyzer = require('../../models').TemplateDialyzer
const StandingOrderMedication = require('../../models').TemplateMedication
const StandingOrderParameter = require('../../models').TemplateParameter
const StandingOrderWeight = require('../../models').TemplateWeight
const notFoundException = require('../../helpers/exceptions/not-found')

module.exports = {
  index (request: $Request, response: $Response, next: NextFunction): Patient {
    const sortQueryKeys: Array<string> = Object.keys(request.query)

    // const sortQueryValues: Array<string> = Object.values(request.query)

    const stringifiedSortQueryKeys: string = sortQueryKeys.toString()

    /* let orderValue
    if (stringifiedSortQueryKeys === 'createdAt' || stringifiedSortQueryKeys === 'lastName') {
      orderValue = [_.snakeCase(sortQueryKeys.toString()), sortQueryValues.toString()]
    } */

    let whereClause
    if (stringifiedSortQueryKeys === 'fullName') {
      whereClause = {
        [Op.or]: [
          {
            first_name: {
              [Op.iLike]: `%${request.query.fullName}%`
            }
          },
          {
            last_name: {
              [Op.iLike]: `%${request.query.fullName}%`
            }
          },
          {
            middle_name: {
              [Op.iLike]: `%${request.query.fullName}%`
            }
          }
        ]
      }
    }

    const limit = 25
    let currentPage = +request.query.page || 1
    const offset = limit * (currentPage - 1)
    const nextUrl = `${request.get('host')}/api/patients/?page=${currentPage + 1}`
    const previousUrl = `${request.get('host')}/api/patients/?page=${currentPage - 1}`

    return Patient
      .findAndCountAll({
        include: {
          model: PatientRegistry,
          as   : 'patientRegistries'
        },
        limit,
        offset,
        order   : [['last_name', 'ASC']],
        where   : whereClause || null,
        // order   : orderValue ? [orderValue] : null,
        distinct: true
      })
      .then((patients: Array<Patient>): Promise<$Response> => {
        const totalPages = Math.ceil(patients.count / 25)

        return response.status(OK).send({
          meta: {
            limit,
            currentPage,
            totalPages,
            totalCount: patients.count,
            next      : currentPage === totalPages ? undefined : nextUrl,
            previous  : currentPage === 1 ? undefined : previousUrl
          },
          data: patients.rows
        })
      })
      .catch((error: Error): Promise<NextFunction> => next(error))
  },

  select (request, response, next) {
    return Patient
      .findById(request.params.patientId, {
        include: [
          {
            model: PatientRegistry,
            as   : 'patientRegistries'
          },
          {
            model: StandingOrderAntiCoagulant,
            as   : 'templateAntiCoagulants'
          },
          {
            model: StandingOrderDialyzer,
            as   : 'templateDialyzers'
          },
          {
            model: StandingOrderMedication,
            as   : 'templateMedications'
          },
          {
            model: StandingOrderParameter,
            as   : 'templateParameters'
          },
          {
            model: StandingOrderWeight,
            as   : 'templateWeights'
          }
        ]
      })
      .then(patient => {
        if (!patient) {
          notFoundException()
        }

        return response.status(OK).send({
          data: patient
        })
      })
      .catch(error => next(error))
  },

  create (request, response, next) {
    return Patient
      .findOrCreate({
        where: {
          bizbox_patient_no: request.body.bizbox_patient_no.toString()
        },
        defaults: {
          first_name         : request.body.first_name,
          middle_name        : request.body.middle_name,
          last_name          : request.body.last_name,
          age                : request.body.age,
          sex                : request.body.sex,
          civil_status       : request.body.civil_status,
          room_no            : request.body.room_no,
          hospital_no        : request.body.hospital_no,
          attending_physician: request.body.attending_physician,
          created_by         : request.user.user_id,
          updated_by         : request.user.user_id
        }
      })
      .spread((patient, isCreate) => {
        if (isCreate) {
          return response.status(OK).send({
            exist: false,
            data : patient
          })
        }
        return response.status(CREATED).send({
          message: 'Patient already exists',
          exist  : true,
          data   : patient
        })
      })
      .catch(error => next(error))
  }
}
