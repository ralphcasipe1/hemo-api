import type { NextFunction, $Request, $Response } from 'express'
import { CREATED, OK } from 'http-status'
import md5 from 'md5'

const notFoundException = require('../helpers/exceptions/not-found')
const User = require('../models').User

interface IUser {
  user_id: number,
  first_name: string,
  middle_name: string,
  last_name: string,
  id_number: string,
  is_admin: boolean,
  is_doctor: boolean,
  created_by: number,
  updated_by: number,
  deleted_by: number | void,
  created_at: string,
  updated_at: string,
  deleted_at: string | void
}

module.exports = {
  index (
    _: void,
    response: $Response,
    next: NextFunction
  ): Promise<{data: IUser}> {
    return User
      .findAll({
        attributes: [
          'user_id',
          'first_name',
          'middle_name',
          'last_name',
          'id_number',
          'is_admin',
          'is_doctor',
          'created_at',
          'updated_at',
          'deleted_at'
        ],
        order: [
          ['deleted_at', 'DESC']
        ],
        paranoid: false
      })
      .then((users: Array<IUser>): $Response => response.status(OK).send({ data: users }))
      .catch((error: Error): mixed => next(error))
  },

  select (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): Promise<{data: IUser}> {
    return User
      .findById(request.params.userId, {
        attributes: [
          'user_id',
          'first_name',
          'middle_name',
          'last_name',
          'id_number',
          'is_admin',
          'is_doctor',
          'created_at',
          'updated_at'
        ]
      })
      .then((user: IUser): $Response => {
        if (!user) {
          notFoundException()
        }

        return response.status(OK).send({ data: user })
      })
      .catch((error: Error): mixed => next(error))
  },

  create (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): Promise<{data: IUser}> {
    let password: string

    if (request.body.password === null || request.body.password === undefined) {
      password = md5(request.body.id_number)
    } else {
      password = md5(request.body.password)
    }

    return User
      .create({
        first_name : request.body.first_name,
        middle_name: request.body.middle_name,
        last_name  : request.body.last_name,
        id_number  : request.body.id_number,
        password,
        is_admin   : request.body.is_admin,
        is_doctor  : request.body.is_doctor
      })
      .then((user: IUser): $Response => response.status(CREATED).send({ data: user }))
      .catch((error: Error): mixed => next(error))
  },

  update (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): Promise<{data: IUser}> {
    return User
      .findById(request.params.userId)
      .then((user: IUser): $Response => {
        if (!user) {
          notFoundException()
        }

        return user
          .update(
            request.body,
            { fields: Object.keys(request.body) }
          )
          .then((): $Response => response.status(OK).send({ data: user }))
      })
      .catch((error: Error): mixed => next(error))
  },

  destroy (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): Promise<{data: IUser}> {
    return User
      .findById(request.params.userId)
      .then((user: IUser): $Response => {
        if (!user) {
          notFoundException()
        }
        return user
          .destroy()
          .then((): $Response => response.status(OK).send({ data: user }))
      })
      .catch((error: Error): mixed => next(error))
  }
}
