'use strict'

let notFoundException = require('../../../helpers/exceptions/not-found')
let NurseNote = require('../../../models').NurseNote
let PatientRegistry = require('../../../models').PatientRegistry

module.exports = {
  create (req, res) {
    return NurseNote.create({
      patient_registry_id: req.params.patientRegistryId,
      note_desc: req.body.note_desc,
      created_by: req.user.user_id,
      updated_by: req.user.user_id
    })
      .then(
        (nurseNote) => (
          NurseNote.findById(nurseNote.nurse_note_id, {
            attributes: [
              'nurse_note_id',
              'patient_registry_id',
              'note_desc',
              'created_at',
              'updated_at'
            ]
          })
            .then(() => res.status(200).send({ data: nurseNote }))
        )
      )
      .catch(error => res.status(400).send({ message: error.message }))
  },

  destroy (req, res) {
    return NurseNote.findById(req.params.nurseNoteId).then((nurseNote) => {
      if (!nurseNote) {
        notFoundException()
      }
      return nurseNote.destroy().then(() => res.status(202).send({
        data: nurseNote,
        message: 'Nurse note successfully deleted'
      }), (err) => res.status(400).send({
        message: err.message
      }))
    })
  },

  index (req, res) {
    return NurseNote.findAll({
      where: {
        patient_registry_id: req.params.patientRegistryId
      },
      order: [
        [
          'updated_at',
          'ASC'
        ]
      ]
      /* include: [
        {
          model: User,
          as: 'updatedBy',
        },
      ], */
    })
      .then(
        (nurseNotes) => res
          .status(200)
          .send({
            data: nurseNotes
          }),
        (err) => res
          .status(400)
          .send({
            message: err.message
          })
      )
  },

  select (req, res) {
    return NurseNote.findById(req.params.nurseNoteId).then((nurseNote) => res.status(200).send({
      data: nurseNote
    }), (err) => res.status(400).send({
      message: err.message
    }))
  },

  update (req, res) {
    return NurseNote.findById(req.params.nurseNoteId).then((nurseNote) => {
      if (!nurseNote) {
        notFoundException()
      }
      return nurseNote
        .update(req.body, { fields: Object.keys(req.body) })
        .then(() => res.status(200).send({ data: nurseNote }), (err) => res.status(400).send({
          message: err
        }))
    }, (err) => res.status(400).send({
      message: err
    }))
  }
}
