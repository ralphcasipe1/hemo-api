'use strict'

let notFoundException = require('../../../helpers/exceptions/not-found')
let VitalSign = require('../../../models').VitalSign
let PatientRegistry = require('../../../models').PatientRegistry
let User = require('../../../models').User

module.exports = {
  create (req, res, next) {
    return VitalSign.create({
      patient_registry_id: req.params.patientRegistryId,
      blood_pressure: req.body.blood_pressure,
      cardiac_arrest: req.body.cardiac_arrest,
      respiratory_rate: req.body.respiratory_rate,
      temperature: req.body.temperature,
      oxygen_saturation: req.body.oxygen_saturation,
      arterial_pressure: req.body.arterial_pressure,
      venous_pressure: req.body.venous_pressure,
      transmembrane_pressure: req.body.transmembrane_pressure,
      blood_flow_rate: req.body.blood_flow_rate,
      blood_glucose: req.body.blood_glucose,
      remarks: req.body.remarks,
      created_by: req.user.user_id,
      updated_by: req.user.user_id
    }).then(
      (vitalSign) => {
        VitalSign.findById(vitalSign.vital_sign_id, {
          include: [
            {
              model: User,
              attributes: [
                'first_name',
                'middle_name',
                'last_name',
                'id_number'
              ]
            }
          ],
          attributes: [
            'vital_sign_id',
            'patient_registry_id',
            'blood_pressure',
            'cardiac_arrest',
            'respiratory_rate',
            'temperature',
            'oxygen_saturation',
            'arterial_pressure',
            'venous_pressure',
            'transmembrane_pressure',
            'blood_flow_rate',
            'blood_glucose',
            'remarks',
            'created_at',
            'updated_at',
            'deleted_at'
          ]
        })
          .then(
            () =>
              res.status(200).send({
                data: vitalSign
              })
          )
      }
    )
      .catch(error => next(error))
  },

  destroy (req, res) {
    return VitalSign.findById(req.params.vitalSignId).then((vitalSign) => {
      if (!vitalSign) {
        notFoundException()
      }
      return vitalSign.destroy().then(
        () =>
          res.status(200).send({
            data: vitalSign,
            message: 'Vital Sign deleted successfully'
          }),
        (err) =>
          res.status(400).send({
            message: err.message
          })
      )
    })
  },

  index (req, res) {
    return VitalSign.all({
      limit: 15,
      order: [
        [
          'created_at',
          'ASC'
        ]
      ],
      where: {
        patient_registry_id: req.params.patientRegistryId
      },
      include: [
        {
          model: User,

          attributes: [
            'first_name',
            'middle_name',
            'last_name',
            'id_number'
          ]
        }
      ],
      attributes: [
        'vital_sign_id',
        'patient_registry_id',
        'blood_pressure',
        'cardiac_arrest',
        'respiratory_rate',
        'temperature',
        'oxygen_saturation',
        'arterial_pressure',
        'venous_pressure',
        'transmembrane_pressure',
        'blood_flow_rate',
        'blood_glucose',
        'remarks',
        'created_at',
        'updated_at',
        'deleted_at'
      ]
    }).then(
      (vitalSigns) =>
        res.status(200).send({
          data: vitalSigns
        }),
      (err) =>
        res.status(400).send({
          message: err.message
        })
    )
  },

  select (req, res) {
    return VitalSign.findById(req.params.vitalSignId).then(
      (vitalSign) =>
        res.status(200).send({
          data: vitalSign
        }),
      (err) =>
        res.status(400).send({
          message: err.message
        })
    )
  },

  update (req, res) {
    return VitalSign.findById(req.params.vitalSignId).then(
      (vitalSign) => {
        if (!vitalSign) {
          notFoundException()
        }
        return vitalSign.update(req.body, { fields: Object.keys(req.body) }).then(
          () => res.status(200).send({ data: vitalSign }),
          (err) =>
            res.status(400).send({
              message: err
            })
        )
      },
      (err) =>
        res.status(400).send({
          message: err
        })
    )
  }
}
