'use strict'

let notFoundException = require('../../../helpers/exceptions/not-found')
let Mobility = require('../../../models').Mobility
let PatientRegistry = require('../../../models').PatientRegistry
let PreAssessment = require('../../../models').PreAssessment

module.exports = {
  create (request, response, next) {
    return PreAssessment
      .create({
        patient_registry_id: request.params.patientRegistryId,
        mobility_id        : request.body.mobility_id,
        hbs_ag             : request.body.hbs_ag,
        anti_hbs           : request.body.anti_hbs,
        anti_hcv           : request.body.anti_hcv,
        anti_hiv           : request.body.anti_hiv,
        antigen_date       : request.body.antigen_date,
        created_by         : request.user.user_id,
        updated_by         : request.user.user_id
      })
      .then((preAssessment) =>
        PreAssessment
          .findById(preAssessment.pre_assessment_id, {
            include: [
              {
                model     : Mobility,
                as        : 'mobility',
                attributes: [
                  'name',
                  'description',
                  'mobility_id'
                ]
              }
            ],
            attributes: [
              'pre_assessment_id',
              'patient_registry_id',
              'mobility_id',
              'hbs_ag',
              'anti_hbs',
              'anti_hcv',
              'anti_hiv',
              'antigen_date',
              'created_at',
              'updated_at'
            ]
          })
          .then((data) => response.status(200).send({ data }))
      )
      .catch((error: Error) => next(error))
  },

  destroy (request, response, next) {
    return PreAssessment
      .findById(request.params.preAssessmentId)
      .then((preAssessment) => {
        if (!preAssessment) {
          notFoundException()
        }
        return preAssessment
          .destroy()
          .then(() => response.status(202).send({ message: 'Pre-assessment deleted successfully' }))
      })
      .catch((error: Error) => next(error))
  },

  index (request, response, next) {
    return PatientRegistry
      .findById(request.params.patientRegistryId)
      .then((patientRegistry) => {
        if (!patientRegistry) {
          notFoundException()
        }
        return patientRegistry
          .getPreAssessment({
            include: [
              {
                model     : Mobility,
                as        : 'mobility',
                attributes: [
                  'name',
                  'description',
                  'mobility_id'
                ]
              }
            ]
          })
          .then((preAssessment) => response.status(200).send({ data: preAssessment }))
      })
      .catch((error: Error) => next(error))
  },

  update (request, response, next) {
    return PreAssessment
      .findById(request.params.preAssessmentId)
      .then((preAssessment) => {
        if (!preAssessment) {
          return response.status(404).send({
            message: 'No pre-assessment found'
          })
        }
        return preAssessment
          .update(request.body, { fields: Object.keys(request.body) })
          .then(() => response.status(200).send({ data: preAssessment }))
      })
      .catch((error: Error) => next(error))
  }
}
