'use strict'

let notFoundException = require('../../../helpers/exceptions/not-found')
let Abnormal = require('../../../models').Abnormal
let PatientRegistry = require('../../../models').PatientRegistry
let Subjective = require('../../../models').Subjective

module.exports = {
  create (req, res, next) {
    return PatientRegistry
      .findById(req.params.patientRegistryId)
      .then((patientRegistry) => {
        if (!patientRegistry) {
          notFoundException()
        }
        return patientRegistry
          .setSubjectives(req.body.subjective_id, {
            through: {
              created_by: req.user.user_id,
              updated_by: req.user.user_id
            }
          })
          .then(
            () => PatientRegistry
              .findById(req.params.patientRegistryId)
              .then(() => {
                if (!patientRegistry) {
                  notFoundException()
                }
                return patientRegistry
                  .getSubjectives()
                  .then((data) => res.status(201).send({ data }))
              })
          )
      })
      .catch((error: Error) => next(error))
  },

  index (req, res, next) {
    return PatientRegistry
      .findById(req.params.patientRegistryId)
      .then((patientRegistry) => {
        if (!patientRegistry) {
          notFoundException()
        }
        return patientRegistry
          .getSubjectives()
          .then((data) => res.status(201).send({ data }))
      })
      .catch((error: Error) => next(error))
  },

  select (req, res, next) {
    return PatientRegistry
      .findById(req.params.patientRegistryId)
      .then((patientRegistry) => {
        if (!patientRegistry) {
          notFoundException()
        }
        return patientRegistry
          .getSubjective({
            where: {
              subjective_id: req.params.subjectiveId
            }
          })
          .then((data) => res.status(201).send({ data }))
      })
      .catch((error: Error) => next(error))
  },

  destroy (req, res, next) {
    return PatientRegistry
      .findById(req.params.patientRegistryId)
      .then((patientRegistry) => {
        if (!patientRegistry) {
          notFoundException()
        }
        return patientRegistry
          .setSubjectives([])
          .then((data) => res.status(201).send({ data: data.reduce((nest) => nest.concat(data)) }))
      })
      .catch((error: Error) => next(error))
  }
}
