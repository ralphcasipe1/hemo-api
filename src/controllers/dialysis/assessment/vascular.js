'use strict'

let notFoundException = require('../../../helpers/exceptions/not-found')
let Abnormal = require('../../../models').Abnormal
let PatientRegistry = require('../../../models').PatientRegistry
let Vascular = require('../../../models').Vascular

module.exports = {
  create (req, res, next) {
    return PatientRegistry
      .findById(req.params.patientRegistryId)
      .then((patientRegistry) => {
        if (!patientRegistry) {
          notFoundException()
        }
        return patientRegistry
          .createVascular({
            patient_registry_id: req.params.patientRegistryId,
            type               : req.body.type,
            operation_date     : req.body.operation_date,
            surgeon            : req.body.surgeon,
            location           : req.body.location,
            arterial_location  : req.body.arterial_location,
            arterial_needle    : req.body.arterial_needle,
            venous_needle      : req.body.venous_needle,
            is_bruit           : req.body.venous_needle,
            is_thrill          : req.body.is_thrill,
            created_by         : req.user.user_id,
            updated_by         : req.user.user_id
          })
          .then((vascular) => {
            if (req.body.abnormal_id.length === 0 || req.body.abnormal_id === undefined) {
              return Vascular
                .findOne({
                  where: {
                    patient_registry_id: req.params.patientRegistryId
                  }
                })
                .then(() => {
                  if (!vascular) {
                    notFoundException()
                  }
                  return vascular
                    .setAbnormals(req.body.abnormal_id, {
                      through: {
                        created_by: req.user.user_id,
                        updated_by: req.user.user_id
                      }
                    })
                    .then(() => Vascular
                      .findOne({
                        where: {
                          patient_registry_id: req.params.patientRegistryId
                        },
                        include: [
                          {
                            model: Abnormal
                          }
                        ]
                      })
                      .then(() => res.status(201).send({ data: vascular }))
                    )
                })
            }
            return res.status(201).send({
              data: vascular
            })
          })
      })
      .catch((error: Error) => next(error))
  },

  destroy (req, res, next) {
    return Vascular
      .findById(req.params.vascularId)
      .then((vascular) => {
        if (!vascular) {
          notFoundException()
        }
        return vascular
          .destroy()
          .then(() => res.status(200).send({ message: 'Vascular data deleted successfully' }))
      })
      .catch((error: Error) => next(error))
  },

  index (req, res, next) {
    return Vascular
      .findOne({
        where: {
          patient_registry_id: req.params.patientRegistryId
        },
        include: [
          {
            model: Abnormal,
            as   : 'abnormals'
          }
        ]
      })
      .then((vascular) => res.status(200).send({ data: vascular }))
      .catch((error: Error) => next(error))
  },

  select (req, res, next) {
    return Vascular
      .findById(req.params.vascularId)
      .then((vascular) => res.status(200).send({ data: vascular }))
      .catch((error: Error) => next(error))
  },

  update (req, res, next) {
    return Vascular
      .findById(req.params.vascularId)
      .then((vascular) => {
        if (!vascular) {
          notFoundException()
        }
        return vascular
          .update(req.body, { fields: Object.keys(req.body) })
          .then(() => res.status(200).send({ data: vascular }))
      })
      .catch((error: Error) => next(error))
  }
}
