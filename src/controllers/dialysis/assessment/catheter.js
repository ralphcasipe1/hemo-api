'use strict'

let notFoundException = require('../../../helpers/exceptions/not-found')
let Abnormal = require('../../../models').Abnormal
let Catheter = require('../../../models').Catheter
let PatientRegistry = require('../../../models').PatientRegistry

module.exports = {
  create (request, response, next) {
    return PatientRegistry.findById(request.params.patientRegistryId)
      .then(
        function (patientRegistry) {
          if (!patientRegistry) {
            notFoundException()
          }

          return patientRegistry
            .createCatheter({
              patient_registry_id : request.params.patientRegistryId,
              type                : request.body.type,
              position            : request.body.position,
              location            : request.body.location,
              operation_date      : request.body.operation_date,
              physician           : request.body.physician,
              days_situ           : request.body.days_situ,
              is_red_port_inflow  : request.body.is_red_port_inflow,
              is_red_port_outflow : request.body.is_red_port_outflow,
              is_blue_port_inflow : request.body.is_blue_port_inflow,
              is_blue_port_outflow: request.body.is_blue_port_outflow,
              created_by          : request.user.user_id,
              updated_by          : request.user.user_id
            })
        }
      )
      .then(
        function (insertedData) {
          return Catheter.findOne({
            where: {
              catheter_id        : insertedData.catheter_id,
              patient_registry_id: request.params.patientRegistryId
            }
          })
        }
      )
      .then(
        function (catheter) {
          if (!catheter) {
            notFoundException()
          }

          if (request.body.abnormal_id === undefined || request.body.abnormal_id.length === 0) {
            return response.status(201).send({
              data: catheter
            })
          }

          return catheter
            .setAbnormals(request.body.abnormal_id, {
              through: {
                created_by: request.user.user_id,
                updated_by: request.user.user_id
              }
            })
            .then(() =>
              Catheter.findOne({
                include: [{
                  model: Abnormal,
                  as   : 'abnormals'
                }],
                where: {
                  patient_registry_id: request.params.patientRegistryId
                }
              })
                .then(selectedCatheter => response.status(201).send({ data: selectedCatheter }))
            )
        }
      )
      /* .then((insertedData) => {
        console.log(catheter)
        return Catheter.findOne({
          include: [{
            model: Abnormal,
            as   : 'abnormals'
          }],
          where: {
            catheter_id: insertedData.catheter_id
          }
        })
          .then((selectedCatheter) => response.status(201).send({ data: selectedCatheter }))
      }) */
      .catch(function (error) {
        next(error)
      })
  },

  destroy (request, response, next) {
    return Catheter
      .findById(request.params.catheterId)
      .then((catheter) => {
        if (!catheter) {
          notFoundException()
        }

        return catheter
          .destroy()
          .then(() => response.status(200).send({
            message: 'Catheter data deleted successfully'
          }))
      })
      .catch(function (error) {
        next(error)
      })
  },

  index (request, response, next) {
    return PatientRegistry.findById(request.params.patientRegistryId)
      .then((patientRegistry) => {
        if (!patientRegistry) {
          notFoundException()
        }

        return patientRegistry
          .getCatheter({
            include: [
              {
                model: Abnormal,
                as   : 'abnormals'
              }
            ]
          })
          .then((catheter) => response.status(200).send({
            data: catheter
          }), (error) => response.status(400).send({
            message: error.message
          }))
      })
      .catch(function (error) {
        next(error)
      })
  },

  select (request, response, next) {
    return Catheter.findById(request.params.catheterId)
      .then((catheter) => response.status(200).send({
        data: catheter
      }))
      .catch(function (error) {
        next(error)
      })
  },

  update (request, response, next) {
    return Catheter.findById(request.params.catheterId)
      .then((catheter) => {
        if (!catheter) {
          notFoundException()
        }
        return catheter
          .update(request.body, { fields: Object.keys(request.body) })
          .then(() => PatientRegistry
            .findById(request.params.patientRegistryId)
            .then((patientRegistry) => {
              if (!patientRegistry) {
                notFoundException()
              }
              return patientRegistry
                .getCatheter({
                  include: [
                    {
                      model: Abnormal
                    }
                  ]
                })
                .then(() => response.status(200).send({
                  data: catheter
                }))
            })
          )
      })
      .catch(function (error) {
        next(error)
      })
  }
}

/* return PatientRegistry.findById(request.params.patientRegistryId)
      .then((patientRegistry) => {
        notFoundException(response, response, patientRegistry);

        return patientRegistry
          .createCatheter({
            patient_registry_id: request.params.patientRegistryId,
            type: request.body.type,
            position: request.body.position,
            location: request.body.location,
            operation_date: request.body.operation_date,
            physician: request.body.physician,
            days_situ: request.body.days_situ,
            is_red_port_inflow: request.body.is_red_port_inflow,
            is_red_port_outflow: request.body.is_red_port_outflow,
            is_blue_port_inflow: request.body.is_blue_port_inflow,
            is_blue_port_outflow: request.body.is_blue_port_outflow,
            created_by: request.user.user_id,
            updated_by: request.user.user_id,
          })
          .then((catheter) => {
            if (request.body.abnormal === undefined || request.body.abnormal.length === 0) {
              return response.status(201).send({
                data: catheter,
              });
            }

            return Catheter.findOne({
              where: {
                catheter_id: catheter.catheter_id,
                patient_registry_id: request.params.patientRegistryId,
              },
            }).then(() => {
              notFoundException(response, response, catheter)

              return catheter
                .setAbnormals(request.body.abnormal_id, {
                  through: {
                    created_by: request.user.user_id,
                    updated_by: request.user.user_id,
                  },
                })
                .then(() => Catheter.findOne({
                  where: {
                    patient_registry_id: request.params.patientRegistryId,
                  },
                  include: [
                    {
                      model: Abnormal,
                    },
                  ],
                }).then(() => response.status(201).send({
                  data: catheter,
                }), (error) => response.status(400).send({
                  message: error.message,
                })), (error) => response.status(400).send({
                  message: error.message,
                }));
            }, (error) => response.status(400).send({
              message: error.message,
            }));
          }, (error) => response.status(400).send({
            name: error.name,
            message: error.message,
            detail: error.parent.detail,
          }));
      }); */
