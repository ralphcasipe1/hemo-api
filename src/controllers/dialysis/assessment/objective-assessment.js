'use strict'

let notFoundException = require('../../../helpers/exceptions/not-found')
// let Abnormal = require('../../../models').Abnormal
// let Objective = require('../../../models').Objective
let PatientRegistry = require('../../../models').PatientRegistry

module.exports = {
  create (req, res, next) {
    return PatientRegistry
      .findById(req.params.patientRegistryId)
      .then((patientRegistry) => {
        if (!patientRegistry) {
          notFoundException()
        }
        return patientRegistry
          .setObjectives(req.body.objective_id, {
            through: {
              created_by: req.user.user_id,
              updated_by: req.user.user_id
            }
          })
          .then(
            () => PatientRegistry
              .findById(req.params.patientRegistryId)
              .then(() => {
                if (!patientRegistry) {
                  notFoundException()
                }
                return patientRegistry
                  .getObjectives()
                  .then((data) => res.status(201).send({
                    data
                  }))
              })
          )
      })
      .catch((error: Error) => next(error))
  },

  index (req, res, next) {
    return PatientRegistry
      .findById(req.params.patientRegistryId)
      .then((patientRegistry) => {
        if (!patientRegistry) {
          notFoundException()
        }
        return patientRegistry
          .getObjectives()
          .then((data) => res.status(201).send({ data }))
      })
      .catch((error: Error) => next(error))
  },

  select (req, res, next) {
    return PatientRegistry
      .findById(req.params.patientRegistryId)
      .then((patientRegistry) => {
        if (!patientRegistry) {
          notFoundException()
        }
        return patientRegistry
          .getObjectives({
            where: {
              objective_id: req.params.objectiveId
            }
          })
          .then((data) => res.status(201).send({ data }))
      })
      .catch((error: Error) => next(error))
  },

  destroy (req, res, next) {
    return PatientRegistry
      .findById(req.params.patientRegistryId)
      .then((patientRegistry) => {
        if (!patientRegistry) {
          notFoundException()
        }
        return patientRegistry
          .setObjectives([])
          .then((data) => res.status(201).send({
            data: data.reduce((nest) => nest.concat(data))
          }))
      })
      .catch((error: Error) => next(error))
  }
}
