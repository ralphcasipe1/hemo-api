import type { NextFunction, $Request, $Response } from 'express'
import { CREATED, OK } from 'http-status'

const notFoundException = require('../../../helpers/exceptions/not-found')
const Dialyzer = require('../../../models').Dialyzer
const DialyzerBrand = require('../../../models').DialyzerBrand
const DialyzerSize = require('../../../models').DialyzerSize
const PatientRegistry = require('../../../models').PatientRegistry

interface IDialyzer {
  anti_coagulant_id: number,
  patient_registry_id: number,
  type: string,
  dialyzer_date: string,
  count: integer,
  d_t: string,
  created_by: number,
  updated_by: number,
  deleted_by: number | void,
  created_at: string,
  updated_at: string,
  deleted_at: string | void
}
module.exports = {
  create (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): Promise<{data: IDialyzer}> {
    return PatientRegistry
      .findById(request.params.patientRegistryId)
      .then((patientRegistry): $Response => {
        if (!patientRegistry) {
          notFoundException()
        }

        return patientRegistry
          .createDialyzer({
            patient_registry_id: request.params.patientRegistryId,
            dialyzer_brand_id  : request.body.dialyzer_brand_id,
            type               : request.body.type,
            dialyzer_size_id   : request.body.dialyzer_size_id,
            dialyzer_date      : request.body.dialyzer_date,
            count              : request.body.count,
            d_t                : request.body.d_t,
            created_by         : request.user.user_id,
            updated_by         : request.user.user_id
          })
          .then((dialyzer: IDialyzer): IDialyzer => (
            Dialyzer
              .findById(dialyzer.dialyzer_id, {
                include: [
                  {
                    model     : DialyzerBrand,
                    as        : 'dialyzerBrand',
                    attributes: [
                      'dialyzer_brand_id',
                      'brand_name',
                      'description'
                    ]
                  },
                  {
                    model     : DialyzerSize,
                    as        : 'dialyzerSize',
                    attributes: [
                      'dialyzer_size_id',
                      'size_name',
                      'description'
                    ]
                  }
                ],
                attributes: [
                  'dialyzer_id',
                  'patient_registry_id',
                  'type',
                  'dialyzer_date',
                  'count',
                  'd_t'
                ]
              })
              .then((selectedDialyzer: IDialyzer): $Response => response.status(CREATED).send({ data: selectedDialyzer }))
          ))
      })
      .catch((error: Error): mixed => next(error))
  },

  destroy (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): Promise<{data: IDialyzer}> {
    return Dialyzer
      .findById(request.params.dialyzerId)
      .then((dialyzer: IDialyzer): $Response => {
        if (!dialyzer) {
          notFoundException()
        }
        return dialyzer
          .destroy()
          .then((): $Response => response.status(OK).send({ message: 'Dialyzer deleted successfully' }))
      })
      .catch((error: Error): mixed => next(error))
  },

  index (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): Promise<{data: Array<IDialyzer>}> {
    return PatientRegistry
      .findById(request.params.patientRegistryId)
      .then((patientRegistry): $Response => {
        if (!patientRegistry) {
          notFoundException()
        }
        return patientRegistry
          .getDialyzer({
            include: [
              {
                model     : DialyzerBrand,
                as        : 'dialyzerBrand',
                attributes: [
                  'dialyzer_brand_id',
                  'brand_name',
                  'description'
                ]
              },
              {
                model     : DialyzerSize,
                as        : 'dialyzerSize',
                attributes: [
                  'dialyzer_size_id',
                  'size_name',
                  'description'
                ]
              }
            ],
            attributes: [
              'dialyzer_id',
              'patient_registry_id',
              'type',
              'dialyzer_date',
              'count',
              'd_t',
              'created_at',
              'updated_at'
            ]
          })
          .then((dialyzer: IDialyzerSize): $Response => response.status(OK).send({ data: dialyzer }))
      })
      .catch((error: Error): mixed => next(error))
  },

  select (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): Promise<{data: IDialyzer}> {
    return Dialyzer
      .findById(request.params.dialyzerId, {
        include: [
          {
            model     : DialyzerBrand,
            as        : 'dialyzerBrand',
            attributes: [
              'dialyzer_brand_id',
              'brand_name',
              'description'
            ]
          },
          {
            model     : DialyzerSize,
            as        : 'dialyzerSize',
            attributes: [
              'dialyzer_size_id',
              'size_name',
              'description'
            ]
          }
        ],
        attributes: [
          'dialyzer_id',
          'patient_registry_id',
          'type',
          'dialyzer_date',
          'count',
          'd_t'
        ]
      })
      .then((dialyzer: IDialyzer): $Response => response.status(OK).send({ data: dialyzer }))
      .catch((error: Error): mixed => next(error))
  },

  update (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): Promise<{data: IDialyzer}> {
    return Dialyzer
      .findById(request.params.dialyzerId)
      .then((dialyzer: IDialyzer): $Response => {
        if (!dialyzer) {
          notFoundException()
        }
        return dialyzer
          .update(request.body, { fields: Object.keys(request.body) })
          .then((): $Response => (
            Dialyzer.findById(dialyzer.dialyzer_id, {
              include: [
                {
                  model     : DialyzerBrand,
                  as        : 'dialyzerBrand',
                  attributes: [
                    'dialyzer_brand_id',
                    'brand_name',
                    'description'
                  ]
                },
                {
                  model     : DialyzerSize,
                  as        : 'dialyzerSize',
                  attributes: [
                    'dialyzer_size_id',
                    'size_name',
                    'description'
                  ]
                }
              ],
              attributes: [
                'dialyzer_id',
                'patient_registry_id',
                'type',
                'dialyzer_date',
                'count',
                'd_t'
              ]
            })
              .then((): $Response => response.status(201).send({ data: dialyzer }))
          ))
      })
      .catch((error: Error): mixed => next(error))
  }
}
