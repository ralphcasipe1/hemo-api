import type { NextFunction, $Request, $Response } from 'express'
import { CREATED, OK } from 'http-status'

const notFoundException = require('../../../helpers/exceptions/not-found')
const PatientRegistry = require('../../../models').PatientRegistry
const Vaccine = require('../../../models').Vaccine

module.exports = {
  create (request, response) {
    return PatientRegistry
      .findById(request.params.patientRegistryId)
      .then(
        function (patientRegistry) {
          if (!patientRegistry) {
            notFoundException()
          }

          return patientRegistry
            .createVaccine({
              patient_registry_id: request.params.patientRegistryId,
              first_dose_date    : request.body.first_dose_date,
              second_dose_date   : request.body.second_dose_date,
              third_dose_date    : request.body.third_dose_date,
              booster_date       : request.body.booster_date,
              flu_date           : request.body.flu_date,
              pneumonia_spec     : request.body.pneumonia_spec,
              pneumonia_date     : request.body.pneumonia_date,
              remarks            : request.body.remarks,
              created_by         : request.user.user_id,
              updated_by         : request.user.user_id
            })
            .then(
              function (vaccine) {
                return response.status(201).send({
                  data: vaccine
                })
              },

              function (error) {
                return response.status(400).send({
                  message: error.message
                })
              }
            )
        },
        function (erroror) {
          return response.status(400).send({
            message: erroror.message
          })
        })
  },

  destroy (request, response) {
    return Vaccine
      .findById(request.params.vaccineId)
      .then(
        (vaccine) => {
          if (!vaccine) {
            notFoundException()
          }

          return vaccine
            .destroy()
            .then(
              () => response.status(200).send({
                data: vaccine
              }),
              (error) => response.status(400).send({
                message: error.message
              })
            )
        },
        error => response.status(400).send({
          message: error.message
        })
      )
  },

  index (request, response) {
    return PatientRegistry
      .findById(request.params.patientRegistryId)
      .then(
        (patientRegistry) => {
          if (!patientRegistry) {
            notFoundException()
          }

          return patientRegistry
            .getVaccine()
            .then(
              (vaccine) => response.status(200).send({
                data: vaccine
              }),
              (error) => response.status(400).send({
                message: error.message
              })
            )
        },
        error => response.status(400).send({
          message: error.message
        })
      )
  },

  update (request, response) {
    return Vaccine
      .findById(request.params.vaccineId)
      .then(
        (vaccine) => {
          if (!vaccine) {
            notFoundException()
          }

          return vaccine
            .update(
              request.body,
              { fields: Object.keys(request.body) }
            )
            .then(
              () => response.status(200).send({ data: vaccine }),
              (error) => response.status(400).send({
                message: error.message
              })
            )
        },
        (error) => response.status(400).send({
          message: error.message
        })
      )
  }
}
