import type { NextFunction, $Request, $Response } from 'express'
import { CREATED, OK } from 'http-status'

const notFoundException = require('../../../helpers/exceptions/not-found')
const Parameter = require('../../../models').Parameter
const PatientRegistry = require('../../../models').PatientRegistry

module.exports = {

  index (request, response) {
    return PatientRegistry
      .findById(request.params.patientRegistryId)
      .then(
        function (patientRegistry) {
          if (!patientRegistry) {
            notFoundException()
          }

          return patientRegistry
            .getParameter({
              include: [
                {
                  model: PatientRegistry,
                  as   : 'patientRegistry'
                }
              ]
            })
            .then(
              function (parameter) {
                return response.status(200).send({
                  data: parameter
                })
              },

              function (error) {
                return response.status(400).send({
                  message: error.message
                })
              }
            )
        },

        function (error) {
          return response.status(400).send({
            message: error.message
          })
        }
      )
  },

  create (request, response) {
    return PatientRegistry
      .findById(request.params.patientRegistryId)
      .then(
        function (patientRegistry) {
          if (!patientRegistry) {
            notFoundException()
          }

          return patientRegistry
            .createParameter({
              patient_registry_id: request.params.patientRegistryId,
              duration           : request.body.duration,
              blood_flow_rate    : request.body.blood_flow_rate,
              dialysate_bath     : request.body.dialysate_bath,
              dialysate_additive : request.body.dialysate_additive,
              dialysate_flow_rate: request.body.dialysate_flow_rate,
              ufv_goal           : request.body.ufv_goal,
              created_by         : request.user.user_id,
              updated_by         : request.user.user_id
            })
            .then(
              function (parameter) {
                return response.status(201).send({
                  data: parameter
                })
              },

              function (error) {
                return response.status(400).send({
                  message: error.message
                })
              }
            )
        },

        function (error) {
          return response.status(400).send({
            message: error.message
          })
        }
      )
  },

  update (request, response) {
    return Parameter
      .findById(request.params.paramterId)
      .then(
        function (parameter) {
          if (!patientRegistry) {
            notFoundException()
          }

          return parameter
            .update(
              request.body,
              { fields: Object.keys(request.body) }
            )
            .then(
              function () {
                return response.status(200).send({ data: parameter })
              },

              function (error) {
                return response.status(400).send({ message: error.message })
              }
            )
        },

        function (error) {
          return response.status(400).send({ message: error.message })
        }
      )
  },

  destroy (request, response) {
    return Parameter
      .findById(request.params.parameterId)
      .then(
        function (parameter) {
          if (!patientRegistry) {
            notFoundException()
          }

          return parameter
            .destroy()
            .then(
              function () {
                return response.status(200).send({
                  data: parameter
                })
              },

              function (error) {
                return response.status(400).send({
                  message: error.message
                })
              }
            )
        }
      )
  }
}
