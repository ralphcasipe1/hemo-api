import type { NextFunction, $Request, $Response } from 'express'
import { CREATED, OK } from 'http-status'

const notFoundException = require('../../../helpers/exceptions/not-found')
const Weight = require('../../../models').Weight
const PatientRegistry = require('../../../models').PatientRegistry

module.exports = {

  index (request, response) {
    return PatientRegistry
      .findById(request.params.patientRegistryId)
      .then(
        function (patientRegistry) {
          if (!patientRegistry) {
            notFoundException()
          }

          return patientRegistry
            .getWeight()
            .then(
              function (weight) {
                return response.status(200).send({ data: weight })
              },

              function (error) {
                return response.status(400).send({ message: error.message })
              }
            )
        },

        function (error) {
          return response.status(400).send({ message: error.message })
        }
      )
  },

  create (request, response) {
    return PatientRegistry
      .findById(request.params.patientRegistryId)
      .then(
        function (patientRegistry) {
          if (!patientRegistry) {
            notFoundException()
          }

          return patientRegistry
            .createWeight({
              ppatient_registry_id: request.params.patientRegistryId,
              last_post_hd_wt     : request.body.last_post_hd_wt,
              pre_hd_weight       : request.body.pre_hd_weight,
              inter_hd_wt_gain    : request.body.pre_hd_weight - request.body.dry_weight,
              post_hd_weight      : request.body.post_hd_weight,
              net_fluid_removed   : request.body.net_fluid_removed,
              dry_weight          : request.body.dry_weight,
              created_by          : request.user.user_id,
              updated_by          : request.user.user_id
            })
            .then(
              function (weight) {
                return response.status(201).send({ data: weight })
              },

              function (error) {
                return response.status(400).send({ message: error.message })
              }
            )
        },

        function (error) {
          return response.status(400).send({ message: error.message })
        }
      )
  },

  destroy (request, response) {
    return Weight
      .findById(request.params.weightId)
      .then(
        function (weight) {
          if (!weight) {
            notFoundException()
          }

          return weight
            .destroy()
            .then(
              function (weight) {
                return response.status(200).send({
                  data: weight
                })
              },
              function (error) {
                return response.status(400).send({
                  message: error.message
                })
              }
            )
        },
        function (error) {
          return response.status(400).send({
            message: error.message
          })
        }
      )
  },

  update (request, response) {
    return Weight
      .findById(request.params.weightId)
      .then(
        function (weight) {
          if (!weight) {
            notFoundException()
          }

          return weight
            .update(request.body, { fields: Object.keys(request.body) })
            .then(
              function () {
                return response.status(200).send({ data: weight })
              },

              function (error) {
                return response.status(400).send({
                  message: error.message
                })
              }
            )
        },

        function (error) {
          return response.status(400).send({
            message: error.message
          })
        }
      )
  }
}
