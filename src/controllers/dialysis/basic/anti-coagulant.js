import type { NextFunction, $Request, $Response } from 'express'
import { CREATED, OK } from 'http-status'

const notFoundException = require('../../../helpers/exceptions/not-found')
const AntiCoagulant = require('../../../models').AntiCoagulant
const PatientRegistry = require('../../../models').PatientRegistry

interface IAntiCoagulant {
  anti_coagulant_id: number,
  patient_registry_id: number,
  nss_flushing: string,
  nss_flushing_every: string,
  lmwh_iv: string,
  lmwh_iv_iu: string,
  ufh_iv: string,
  ufh_iv_iu: string,
  ufh_iv_iu_every: string,
  bleeding_desc: string,
  created_by: number,
  updated_by: number,
  deleted_by: number | void,
  created_at: string,
  updated_at: string,
  deleted_at: string | void
}

module.exports = {
  create (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): Promise<{data: IAntiCoagulant}> {
    return PatientRegistry
      .findById(request.params.patientRegistryId)
      .then((patientRegistry): $Response => {
        if (!patientRegistry) {
          notFoundException()
        }

        return patientRegistry
          .createAntiCoagulant({
            patient_registry_id: request.params.patientRegistryId,
            nss_flushing       : request.body.nss_flushing,
            nss_flushing_every : request.body.nss_flushing_every,
            lmwh_iv            : request.body.lmwh_iv,
            lmwh_iv_iu         : request.body.lmwh_iv_iu,
            ufh_iv             : request.body.ufh_iv,
            ufh_iv_iu          : request.body.ufh_iv_iu,
            ufh_iv_iu_every    : request.body.ufh_iv_iu_every,
            bleeding_desc      : request.body.bleeding_desc,
            created_by         : request.user.user_id,
            updated_by         : request.user.user_id
          })
          .then((antiCoagulant: IAntiCoagulant): $Response => response.status(CREATED).send({ data: antiCoagulant }))
      })
      .catch((error: Error): mixed => next(error))
  },

  destroy (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): Promise<{data: IAntiCoagulant}> {
    return AntiCoagulant
      .findById(request.params.antiCoagulantId)
      .then((antiCoagulant: IAntiCoagulant): $Response => {
        if (!antiCoagulant) {
          notFoundException()
        }

        return antiCoagulant
          .destroy()
          .then((): $Response => response.status(OK).send({ data: antiCoagulant }))
      })
      .catch((error: Error): mixed => next(error))
  },

  index (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): Promise<{data: Array<IAntiCoagulant>}> {
    return PatientRegistry
      .findById(request.params.patientRegistryId)
      .then((patientRegistry): $Response => {
        if (!patientRegistry) {
          notFoundException()
        }

        return patientRegistry
          .getAntiCoagulant()
          .then((antiCoagulant: IAntiCoagulant): $Response => response.status(OK).send({ data: antiCoagulant }))
      })
      .catch((error: Error): mixed => next(error))
  },

  update (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): Promise<{data: IAntiCoagulant}> {
    return AntiCoagulant
      .findById(request.params.antiCoagulantId)
      .then((antiCoagulant: IAntiCoagulant): $Response => {
        if (!antiCoagulant) {
          notFoundException()
        }

        return antiCoagulant
          .update(request.body, { fields: Object.keys(request.body) })
          .then((): $Response => response.status(OK).send({ data: antiCoagulant }))
      })
      .catch((error: Error): mixed => next(error))
  }
}
