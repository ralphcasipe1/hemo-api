import type { NextFunction, $Request, $Response } from 'express'
import { CREATED, OK } from 'http-status'

const notFoundException = require('../../helpers/exceptions/not-found')
const DialyzerBrand = require('../../models').DialyzerBrand

interface IDialyzerBrand {
  dialyzer_brand_id: number,
  brand_name: string,
  description: string,
  created_by: number,
  updated_by: number,
  deleted_by: number | void,
  created_at: string,
  updated_at: string,
  deleted_at: string | void
}

module.exports = {
  index (
    _: void,
    response: $Response,
    next: NextFunction
  ): Promise<{data: Array<IDialyzerBrand>}> {
    return DialyzerBrand
      .findAll({
        limit: 15,
        order: [
          [
            'created_at',
            'ASC'
          ]
        ]
      })
      .then((dialyzerBrands: Array<IDialyzerBrand>): $Response => response.status(OK).send({ data: dialyzerBrands }))
      .catch((error: Error): mixed => next(error))
  },

  select (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): Promise<{data: IDialyzerBrand}> {
    return DialyzerBrand
      .findById(request.params.dialyzerBrandId)
      .then((dialyzerBrand: IDialyzerBrand): $Response => {
        if (!dialyzerBrand) {
          notFoundException()
        }

        return response.status(OK).send({
          data: dialyzerBrand
        })
      })
      .catch((error: Error): mixed => next(error))
  },

  create (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): Promise<{data: IDialyzerBrand}> {
    return DialyzerBrand
      .create({
        brand_name : request.body.brand_name,
        description: request.body.description,
        created_by : request.user.user_id,
        updated_by : request.user.user_id
      })
      .then((dialyzerBrand: IDialyzerBrand): $Response => response.status(CREATED).send({ data: dialyzerBrand }))
      .catch((error: Error): mixed => next(error))
  },

  update (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): Promise<{data: IDialyzerBrand}> {
    return DialyzerBrand
      .findById(request.params.dialyzerBrandId)
      .then((dialyzerBrand: IDialyzerBrand): $Response => {
        if (!dialyzerBrand) {
          notFoundException()
        }
        return dialyzerBrand
          .update(request.body, { fields: Object.keys(request.body) })
          .then(response.status(OK).send({ data: dialyzerBrand }))
      })
      .catch((error: Error): mixed => next(error))
  },

  destroy (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): NextFunction {
    return DialyzerBrand
      .findById(request.params.dialyzerBrandId)
      .then((dialyzerBrand: IDialyzerBrand): $Response => {
        if (!dialyzerBrand) {
          notFoundException()
        }

        return dialyzerBrand
          .destroy()
          .then(response.status(200).send({ data: dialyzerBrand }))
      })
      .catch((error: Error): mixed => next(error))
  }
}
