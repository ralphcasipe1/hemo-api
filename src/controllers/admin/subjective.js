'use strict'

let notFoundException = require('../../helpers/exceptions/not-found')
let Subjective = require('../../models').Subjective

module.exports = {

  index (request, response) {
    return Subjective
      .findAll()
      .then(
        function (subjectives) {
          return response.status(200).send({
            data: subjectives
          })
        },
        function (error) {
          return response.status(400).send({
            message: error.message
          })
        }
      )
  },

  select (request, response) {
    return Subjective
      .findById(request.params.subjectiveId)
      .then(
        function (subjective) {
          return response.status(200).send({
            data: subjective
          })
        },
        function (error) {
          return response.status(400).send({
            message: error.message
          })
        }
      )
  },

  create (request, response) {
    return Subjective
      .create({
        name: request.body.name,
        description: request.body.description,
        created_by: request.user.user_id,
        updated_by: request.user.user_id
      })
      .then(
        function (subjective) {
          return response.status(201).send({
            data: subjective
          })
        },
        function (error) {
          return response.status(400).send({
            message: error.message
          })
        }
      )
  },

  update (request, response) {
    return Subjective
      .findById(request.params.subjectiveId)
      .then(
        function (subjective) {
          if (!subjective) {
            notFoundException()
          }

          return subjective
            .update(request.body, { fields: Object.keys(request.body) })
            .then(
              function () {
                return response.status(200).send({
                  data: subjective
                })
              },
              function (error) {
                return response.status(400).send({
                  message: error.message
                })
              }
            )
        },
        function (error) {
          return response.status(400).send({
            message: error.message
          })
        }
      )
  },

  destroy (request, response) {
    return Subjective
      .findById(request.params.subjectiveId)
      .then(
        function (subjective) {
          if (!subjective) {
            notFoundException()
          }

          return subjective
            .destroy()
            .then(
              function () {
                return response.status(200).send({
                  data: subjective
                })
              },
              function (error) {
                return response.status(400).send({
                  message: error.message
                })
              }
            )
        },
        function (error) {
          return response.status(400).send({
            message: error.message
          })
        }
      )
  }
}
