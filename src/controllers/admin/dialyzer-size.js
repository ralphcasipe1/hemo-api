import type { NextFunction, $Request, $Response } from 'express'
import { CREATED, OK } from 'http-status'

const notFoundException = require('../../helpers/exceptions/not-found')
const DialyzerSize = require('../../models').DialyzerSize

interface IDialyzerSize {
  dialyzer_size_id: number,
  size_name: string,
  description: string,
  created_by: number,
  updated_by: number,
  deleted_by: number | void,
  created_at: string,
  updated_at: string,
  deleted_at: string | void
}

module.exports = {

  index (
    _: void,
    response: $Response,
    next: NextFunction
  ): Promise<{data: Array<IDialyzerSize>}> {
    return DialyzerSize
      .findAll({
        limit: 15,
        order: [
          [
            'created_at',
            'ASC'
          ]
        ]
      })
      .then((dialyzerSizes: Array<IDialyzerSize>): $Response => response.status(OK).send({ data: dialyzerSizes }))
      .catch((error: Error): mixed => next(error))
  },

  select (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): Promise<{data: IDialyzerSize}> {
    return DialyzerSize
      .findById(request.params.dialyzerSizeId)
      .then((dialyzerSize: IDialyzerSize): $Response => response.status(OK).send({ data: dialyzerSize }))
      .catch((error: Error): mixed => next(error))
  },

  create (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): Promise<{data: IDialyzerSize}> {
    return DialyzerSize
      .create({
        size_name  : request.body.size_name,
        description: request.body.description,
        created_by : request.user.user_id,
        updated_by : request.user.user_id
      })
      .then((dialyzerSize: IDialyzerSize): $Response => response.status(CREATED).send({ data: dialyzerSize }))
      .catch((error: Error): mixed => next(error))
  },

  update (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): Promise<{data: IDialyzerSize}> {
    return DialyzerSize
      .findById(request.params.dialyzerSizeId)
      .then((dialyzerSize: IDialyzerSize): $Response => {
        if (!dialyzerSize) {
          notFoundException()
        }

        return dialyzerSize
          .update(request.body, { fields: Object.keys(request.body) })
          .then((): $Response => response.status(OK).send({ data: dialyzerSize }))
      })
      .catch((error: Error): mixed => next(error))
  },

  destroy (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): Promise<{data: IDialyzerSize}> {
    return DialyzerSize
      .findById(request.params.dialyzerSizeId)
      .then((dialyzerSize: IDialyzerSize): $Response => {
        if (!dialyzerSize) {
          notFoundException()
        }

        return dialyzerSize
          .destroy()
          .then((): $Response => response.status(OK).send({ data: dialyzerSize }))
      })
      .catch((error: Error): mixed => next(error))
  }
}
