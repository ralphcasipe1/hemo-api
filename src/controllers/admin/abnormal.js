import type { NextFunction, $Request, $Response } from 'express'
import { CREATED, OK } from 'http-status'

const notFoundException = require('../../helpers/exceptions/not-found')
const Abnormal = require('../../models').Abnormal

interface IAbnormal {
  abnormal_id: number,
  name: string,
  created_by: number,
  updated_by: number,
  deleted_by: number | void,
  created_at: string,
  updated_at: string,
  deleted_at: string | void
}

module.exports = {
  index (
    _: void,
    response: $Response,
    next: NextFunction
  ): Promise<{data: Array<IAbnormal>}> {
    return Abnormal
      .all({
        limit: 15,
        order: [
          [
            'created_at',
            'ASC'
          ]
        ]
      })
      .then((abnormals: Array<IAbnormal>): $Response => response.status(OK).send({ data: abnormals }))
      .catch((error: Error): mixed => next(error))
  },

  select (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): Promise<{data: IAbnormal}> {
    return Abnormal
      .findById(request.params.abnormalId)
      .then((abnormal: IAbnormal): $Response => {
        if (!abnormal) {
          notFoundException()
        }
        return response.status(OK).send({ data: abnormal })
      })
      .catch((error: Error): mixed => next(error))
  },

  create (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): Promise<{data: IAbnormal}> {
    return Abnormal
      .create({
        name       : request.body.name,
        description: request.body.description,
        created_by : request.user.user_id,
        updated_by : request.user.user_id
      })
      .then((abnormal: IAbnormal): $Response => response.status(CREATED).send({ data: abnormal }))
      .catch((error: Error): mixed => next(error))
  },

  update (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): Promise<{data: IAbnormal}> {
    return Abnormal
      .findById(request.params.abnormalId)
      .then((abnormal: IAbnormal): $Response => {
        if (!abnormal) {
          notFoundException()
        }

        return abnormal
          .update(request.body, { fields: Object.keys(request.body) })
          .then((): $Response => response.status(OK).send({ data: abnormal }))
      })
      .catch((error: Error): mixed => next(error))
  },

  destroy (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): Promise<{data: IAbnormal}> {
    return Abnormal
      .findById(request.params.abnormalId)
      .then((abnormal: IAbnormal): $Response => {
        if (!abnormal) {
          notFoundException()
        }

        return abnormal
          .destroy()
          .then((): $Response => response.status(OK).send({ data: abnormal }))
      })
      .catch((error: Error): mixed => next(error))
  }
}
