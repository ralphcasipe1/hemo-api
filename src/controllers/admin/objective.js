import type { NextFunction, $Request, $Response } from 'express'
import { CREATED, OK } from 'http-status'

const notFoundException = require('../../helpers/exceptions/not-found')
const Objective = require('../../models').Objective

interface IObjective {
  objective_id: number,
  name: string,
  description: string,
  created_by: number,
  updated_by: number,
  deleted_by: number | void,
  created_at: string,
  updated_at: string,
  deleted_at: string | void
}

module.exports = {
  index (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): Promise<{data: Array<IObjective>}> {
    return Objective
      .findAll({
        limit: 15
      })
      .then((objectives: Array<IObjective>): $Response => response.status(OK).send({ data: objectives }))
      .catch((error: Error): mixed => next(error))
  },

  select (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): Promise<{data: IObjective}> {
    return Objective
      .findById(request.params.objectiveId)
      .then((objective: IObjective): $Response => {
        if (!objective) {
          notFoundException()
        }

        return response.status(OK).send({
          data: objective
        })
      })
      .catch((error: Error): mixed => next(error))
  },

  create (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): Promise<{data: IObjective}> {
    return Objective
      .create({
        name       : request.body.name,
        description: request.body.description,
        created_by : request.user.user_id,
        updated_by : request.user.user_id
      })
      .then((objective: IObjective): $Response => response.status(CREATED).send({ data: objective }))
      .catch((error: Error): mixed => next(error))
  },

  update (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): Promise<{data: IObjective}> {
    return Objective
      .findById(request.params.objectiveId)
      .then((objective: IObjective): $Response => {
        return objective
          .update(request.body, { fields: Object.keys(request.body) })
          .then((): $Response => response.status(OK).send({ data: objective }))
      })
      .catch((error: Error): mixed => next(error))
  },

  destroy (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): Promise<{data: IObjective}> {
    return Objective
      .findById(request.params.objectiveId)
      .then((objective: IObjective): $Response => {
        if (!objective) {
          notFoundException()
        }

        return objective
          .destroy()
          .then((): $Response => response.status(OK).send({ data: objective }))
      })
      .catch((error: Error): mixed => next(error))
  }
}
