import type { NextFunction, $Request, $Response } from 'express'
import { CREATED, OK } from 'http-status'

const notFoundException = require('../../helpers/exceptions/not-found')
const Mobility = require('../../models').Mobility

interface IMobility {
  mobility_id: number,
  name: string,
  description: string,
  created_by: number,
  updated_by: number,
  deleted_by: number | void,
  created_at: string,
  updated_at: string,
  deleted_at: string | void
}

module.exports = {
  create (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): Promise<{data: IMobility}> {
    return Mobility
      .create({
        name       : request.body.name,
        description: request.body.description,
        created_by : request.user.user_id,
        updated_by : request.user.user_id
      })
      .then((mobility: IMobility): $Response => response.status(CREATED).send({ data: mobility }))
      .catch((error: Error): mixed => next(error))
  },

  destroy (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): Promise<{data: IMobility}> {
    return Mobility
      .findById(request.params.mobilityId)
      .then((mobility: IMobility): $Response => {
        if (!mobility) {
          notFoundException()
        }

        return mobility
          .destroy()
          .then((): $Response => response.status(OK).send({ data: mobility }))
      })
      .catch((error: Error): mixed => next(error))
  },

  index (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): Promise<{data: Array<IMobility>}> {
    return Mobility
      .findAll({
        limit: 15,
        order: [
          [
            'created_at',
            'ASC'
          ]
        ]
      })
      .then((mobilities: Array<IMobility>): $Response => response.status(OK).send({ data: mobilities }))
      .catch((error: Error): mixed => next(error))
  },

  select (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): Promise<{data: IMobility}> {
    return Mobility
      .findById(request.params.mobilityId)
      .then((mobility: IMobility): $Response => {
        if (!mobility) {
          notFoundException()
        }

        return response.status(200).send({
          data: mobility
        })
      })
      .catch((error: Error): mixed => next(error))
  },

  update (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): Promise<{data: IMobility}> {
    return Mobility
      .findById(request.params.mobilityId)
      .then((mobility: IMobility): $Response => {
        if (!mobility) {
          notFoundException()
        }

        return mobility
          .update(request.body, { fields: Object.keys(request.body) })
          .then((): $Response => response.status(200).send({ data: mobility }))
      })
      .catch((error: Error): mixed => next(error))
  }
}
