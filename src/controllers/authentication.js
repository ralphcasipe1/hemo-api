import type { NextFunction, $Request, $Response } from 'express'
import { OK, UNAUTHORIZED } from 'http-status'
import jwt from 'jsonwebtoken'
import md5 from 'md5'
import { secretToken } from '../config/secret-token'

const User = require('../models').User

module.exports = {
  authenticate (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): Promise<{accessToken: string}> {
    const failedAuthentication = 'Authentication failed. Invalid credentials'

    return User
      .findOne({
        where: {
          id_number: request.body.id_number
        }
      })
      .then((user): $Response => {
        if (!user) {
          return response.status(UNAUTHORIZED).json({
            message: failedAuthentication
          })
        }

        const password = md5(request.body.password)

        if (password !== user.password) {
          return response.status(UNAUTHORIZED).send({
            message: failedAuthentication
          })
        }

        return response.status(OK).send({
          accessToken: jwt.sign(
            {
              id_number  : user.id_number,
              user_id    : user.user_id,
              first_name : user.first_name,
              middle_name: user.middle_name,
              last_name  : user.last_name,
              is_admin   : user.is_admin,
              is_doctor  : user.is_doctor,
              expiry_date: Math.floor(Date.now() / 1000) + (60 * 60)
            },
            secretToken
          )
        })
      })
      .catch((error: Error): mixed => next(error))
  },

  unauthorizedException (
    request: $Request,
    response: $Response,
    next: NextFunction
  ): $Response {
    if (request.user) {
      return next()
    }

    return response.status(UNAUTHORIZED).send({
      message: 'Unauthorized user!'
    })
  }
}

/* return bcrypt.compare(request.body.password, user.password, function (error, bcryptResponse) {
            if (!bcryptResponse) {
              console.log('Wrong credentials')
              return response.status(401).send({
                message: failedAuthentication
              })
            }

            return response.status(200).send({
              accessToken: jwt.sign(
                {
                  id_number: user.id_number,
                  user_id: user.user_id,
                  first_name: user.first_name,
                  middle_name: user.middle_name,
                  last_name: user.last_name,
                  is_admin: user.is_admin,
                  is_doctor: user.is_doctor,
                  expiry_date: Math.floor(Date.now() / 1000) + (60 * 60)
                },
                secretToken
              )
            })
          }) */
