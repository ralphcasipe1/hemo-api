// const standingOrderAntiCoagulant = require('./standing-order/anti-coagulant')
import { standingOrderAntiCoagulant } from './standing-order/anti-coagulant'

const abnormal = require('./admin/abnormal')
const dialyzerBrand = require('./admin/dialyzer-brand')
const dialyzerSize = require('./admin/dialyzer-size')
const mobility = require('./admin/mobility')
const objective = require('./admin/objective')
const subjective = require('./admin/subjective')

const authentication = require('./authentication')

const catheter = require('./dialysis/assessment/catheter')
const objectiveAssessment = require('./dialysis/assessment/objective-assessment')
const preAssessment = require('./dialysis/assessment/pre-assessment')
const vascular = require('./dialysis/assessment/vascular')
const subjectiveAssessment = require('./dialysis/assessment/subjective-assessment')

const antiCoagulant = require('./dialysis/basic/anti-coagulant')
const dialyzer = require('./dialysis/basic/dialyzer')
const parameter = require('./dialysis/basic/parameter')
const vaccine = require('./dialysis/basic/vaccine')
const weight = require('./dialysis/basic/weight')

const medication = require('./dialysis/monitor/medication')
const nurseNote = require('./dialysis/monitor/nurse-note')
const vitalSign = require('./dialysis/monitor/vital-sign')

const bizboxPatient = require('./patient/bizbox-hemodialysis-patient')
const patient = require('./patient/hemodialysis-patient')
const patientRegistry = require('./patient/patient-registry')

const user = require('./user')
const standingOrderDialyzer = require('./standing-order/standing-order-dialyzer')
const standingOrderMedication = require('./standing-order/standing-order-medication')
const standingOrderParameter = require('./standing-order/standing-order-parameter')
const standingOrderPhysicianOrder = require('./standing-order/standing-order-physician-order')
const standingOrderWeight = require('./standing-order/standing-order-weight')

module.exports = {
  abnormalController                   : abnormal,
  antiCoagulantController              : antiCoagulant,
  authenticationController             : authentication,
  bizboxPatientController              : bizboxPatient,
  catheterController                   : catheter,
  dialyzerController                   : dialyzer,
  dialyzerBrandController              : dialyzerBrand,
  dialyzerSizeController               : dialyzerSize,
  medicationController                 : medication,
  mobilityController                   : mobility,
  nurseNoteController                  : nurseNote,
  objectiveAssessmentController        : objectiveAssessment,
  objectiveController                  : objective,
  patientController                    : patient,
  patientRegistryController            : patientRegistry,
  parameterController                  : parameter,
  preAssessmentController              : preAssessment,
  standingOrderAntiCoagulantController : standingOrderAntiCoagulant,
  standingOrderDialyzerController      : standingOrderDialyzer,
  standingOrderMedicationController    : standingOrderMedication,
  standingOrderParameterController     : standingOrderParameter,
  standingOrderPhysicianOrderController: standingOrderPhysicianOrder,
  standingOrderWeightController        : standingOrderWeight,
  subjectiveAssessmentController       : subjectiveAssessment,
  subjectiveController                 : subjective,
  userController                       : user,
  weightController                     : weight,
  vaccineController                    : vaccine,
  vascularController                   : vascular,
  vitalSignController                  : vitalSign
}
