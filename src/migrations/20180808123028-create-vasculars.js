'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('vasculars', {
      vascular_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      patient_registry_id: {
        allowNull: false,
        onDelete: 'CASCADE',
        references: {
          model: 'patient_registries',
          key: 'patient_registry_id',
        },
        type: Sequelize.INTEGER,
      },
      type: {
        type: Sequelize.INTEGER,
      },
      operation_date: {
        type: Sequelize.DATEONLY,
      },
      surgeon: {
        type: Sequelize.STRING(60),
      },
      location: {
        type: Sequelize.INTEGER,
      },
      arterial_location: {
        type: Sequelize.INTEGER,
      },
      arterial_needle: {
        type: Sequelize.INTEGER,
      },
      venous_needle: {
        type: Sequelize.INTEGER,
      },
      is_bruit: {
        type: Sequelize.BOOLEAN,
      },
      is_thrill: {
        type: Sequelize.BOOLEAN,
      },
      created_by: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      updated_by: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      deleted_by: {
        type: Sequelize.INTEGER,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      deleted_at: {
        type: Sequelize.DATE,
      },
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('vasculars');
  }
};
