'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('vasculars_abnormals', {
      vascular_id: {
        allowNull: false,
        references: {
          model: 'vasculars',
          key: 'vascular_id',
        },
        type: Sequelize.INTEGER,
      },
      abnormal_id: {
        allowNull: false,
        references: {
          model: 'abnormals',
          key: 'abnormal_id',
        },
        type: Sequelize.INTEGER,
      },
      created_by: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      updated_by: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('vasculars_abnormals');
  }
};
