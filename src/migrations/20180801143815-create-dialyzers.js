'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('dialyzers', {
      dialyzer_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      patient_registry_id: {
        allowNull: false,
        onDelete: 'CASCADE',
        references: {
          model: 'patient_registries',
          key: 'patient_registry_id',
        },
        type: Sequelize.INTEGER,
      },
      dialyzer_brand_id: {
        allowNull: false,
        onDelete: 'CASCADE',
        references: {
          model: 'dialyzer_brands',
          key: 'dialyzer_brand_id',
        },
        type: Sequelize.INTEGER,
      },
      dialyzer_size_id: {
        allowNull: false,
        onDelete: 'CASCADE',
        references: {
          model: 'dialyzer_sizes',
          key: 'dialyzer_size_id',
        },
        type: Sequelize.INTEGER,
      },
      type: {
        allowNull: false,
        type: Sequelize.STRING(30),
      },
      dialyzer_date: {
        type: Sequelize.DATEONLY,
      },
      count: {
        type: Sequelize.INTEGER,
      },
      d_t: {
        type: Sequelize.STRING,
      },
      created_by: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      updated_by: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      deleted_by: {
        type: Sequelize.INTEGER,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      deleted_at: {
        type: Sequelize.DATE,
      },
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('dialyzers');
  }
};
