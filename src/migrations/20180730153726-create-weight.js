'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('weights', {
      weight_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      patient_registry_id: {
        allowNull: false,
        onDelete: 'CASCADE',
        references: {
          model: 'patient_registries',
          key: 'patient_registry_id',
        },
        type: Sequelize.INTEGER,
      },
      last_post_hd_wt: {
        defaultValue: 0,
        type: Sequelize.FLOAT,
      },
      pre_hd_weight: {
        defaultValue: 0,
        type: Sequelize.FLOAT,
      },
      inter_hd_wt_gain: {
        defaultValue: 0,
        type: Sequelize.FLOAT,
      },
      post_hd_weight: {
        defaultValue: 0,
        type: Sequelize.FLOAT,
      },
      net_fluid_removed: {
        defaultValue: 0,
        type: Sequelize.FLOAT,
      },
      dry_weight: {
        defaultValue: 0,
        type: Sequelize.FLOAT,
      },
      created_by: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      updated_by: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      deleted_by: {
        type: Sequelize.INTEGER,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      deleted_at: {
        type: Sequelize.DATE,
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('weights');
  }
};