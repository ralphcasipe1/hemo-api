'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('medications', {
      medication_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT,
      },
      patient_registry_id: {
        allowNull: false,
        onDelete: 'CASCADE',
        references: {
          model: 'patient_registries',
          key: 'patient_registry_id',
        },
        type: Sequelize.BIGINT,
      },
      generic: {
        type: Sequelize.STRING(25),
      },
      brand: {
        type: Sequelize.STRING(25),
      },
      preparation: {
        type: Sequelize.TEXT,
      },
      dosage: {
        type: Sequelize.STRING(20),
      },
      route: {
        type: Sequelize.STRING(20),
      },
      created_by: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      updated_by: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      deleted_by: {
        type: Sequelize.INTEGER,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      deleted_at: {
        type: Sequelize.DATE,
      },
  });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('medications');
  }
};
