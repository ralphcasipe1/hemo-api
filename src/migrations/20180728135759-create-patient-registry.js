'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('patient_registries', {
      patient_registry_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT
      },
      patient_id: {
        allowNull: false,
        onDelete: 'CASCADE',
        references: {
          model: 'patients',
          key: 'patient_id',
        },
        type: Sequelize.BIGINT,
      },
      registry_no: {
        allowNull: false,
        type: Sequelize.STRING(100)
      },
      type: {
        type: Sequelize.INTEGER
      },
      created_by: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      updated_by: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      deleted_by: {
        type: Sequelize.INTEGER
      },
      created_at: {
        allowNull: false,
        defaultValue: Sequelize.literal('NOW()'),
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        defaultValue: Sequelize.literal('NOW()'),
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('patient_registries');
  }
};