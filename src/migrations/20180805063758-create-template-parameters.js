'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('template_parameters', {
      template_parameter_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      patient_id: {
        allowNull: false,
        onDelete: 'CASCADE',
        references: {
          model: 'patients',
          key: 'patient_id',
        },
        type: Sequelize.INTEGER,
      },
      duration: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      blood_flow_rate: {
        allowNull: false,
        type: Sequelize.FLOAT,
      },
      dialysate_bath: {
        allowNull: false,
        type: Sequelize.STRING(50),
      },
      dialysate_additive: {
        type: Sequelize.FLOAT,
      },
      dialysate_flow_rate: {
        allowNull: false,
        type: Sequelize.FLOAT,
      },
      created_by: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      updated_by: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      deleted_by: {
        type: Sequelize.INTEGER,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      deleted_at: {
        type: Sequelize.DATE,
      },
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('template_parameters');
  }
};
