'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('vital_signs', {
      vital_sign_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      patient_registry_id: {
        allowNull: false,
        onDelete: 'CASCADE',
        references: {
          model: 'patient_registries',
          key: 'patient_registry_id',
        },
        type: Sequelize.INTEGER,
      },
      blood_pressure: {
        type: Sequelize.STRING(20),
      },
      cardiac_arrest: {
        type: Sequelize.STRING(20),
      },
      respiratory_rate: {
        type: Sequelize.STRING(20),
      },
      temperature: {
        type: Sequelize.STRING(20),
      },
      oxygen_saturation: {
        type: Sequelize.STRING(20),
      },
      arterial_pressure: {
        type: Sequelize.STRING(20),
      },
      venous_pressure: {
        type: Sequelize.STRING(20),
      },
      transmembrane_pressure: {
        type: Sequelize.STRING(20),
      },
      blood_flow_rate: {
        type: Sequelize.STRING(20),
      },
      blood_glucose: {
        type: Sequelize.STRING(20),
      },
      remarks: {
        type: Sequelize.TEXT,
      },
      created_by: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      updated_by: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      deleted_by: {
        type: Sequelize.INTEGER,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      deleted_at: {
        type: Sequelize.DATE,
      },
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('vital_signs');
  }
};
