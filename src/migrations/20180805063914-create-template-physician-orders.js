'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('template_physician_orders', {
      template_physician_order_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      patient_id: {
        allowNull: false,
        onDelete: 'CASCADE',
        references: {
          model: 'patients',
          key: 'patient_id',
        },
        type: Sequelize.INTEGER,
      },
      assessment: {
        type: Sequelize.TEXT,
      },
      medication: {
        type: Sequelize.TEXT,
      },
      procedure: {
        type: Sequelize.TEXT,
      },
      diagnostic_test: {
        type: Sequelize.TEXT,
      },
      other_remark: {
        type: Sequelize.TEXT,
      },
      created_by: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      updated_by: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      deleted_by: {
        type: Sequelize.INTEGER,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      deleted_at: {
        type: Sequelize.DATE,
      },
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('template_physician_orders');
  }
};
