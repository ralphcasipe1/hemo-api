'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('catheters', {
      catheter_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      patient_registry_id: {
        allowNull: false,
        onDelete: 'CASCADE',
        references: {
          model: 'patient_registries',
          key: 'patient_registry_id',
        },
        type: Sequelize.INTEGER,
      },
      physician: {
        type: Sequelize.STRING(60),
      },
      type: {
        type: Sequelize.INTEGER,
      },
      position: {
        type: Sequelize.INTEGER,
      },
      location: {
        type: Sequelize.INTEGER,
      },
      operation_date: {
        type: Sequelize.DATEONLY,
      },
      days_situ: {
        type: Sequelize.STRING(5),
      },
      is_red_port_inflow: {
        defaultValue: false,
        type: Sequelize.BOOLEAN,
      },
      is_red_port_outflow: {
        defaultValue: false,
        type: Sequelize.BOOLEAN,
      },
      is_blue_port_inflow: {
        defaultValue: false,
        type: Sequelize.BOOLEAN,
      },
      is_blue_port_outflow: {
        defaultValue: false,
        type: Sequelize.BOOLEAN,
      },
      created_by: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      updated_by: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      deleted_by: {
        type: Sequelize.INTEGER,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      deleted_at: {
        type: Sequelize.DATE,
      },
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('catheters');
  }
};
