'use strict'

module.exports = (sequelize, DataTypes) => {
  let SubjectiveAssessment = sequelize.define(
    'SubjectiveAssessment',
    {
      patient_registry_id: DataTypes.INTEGER,
      subjective_id: DataTypes.INTEGER,
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER
    },
    {
      timestamps: true,
      tableName: 'subjective_assessments',
      underscored: true
    }
  )

  return SubjectiveAssessment
}
