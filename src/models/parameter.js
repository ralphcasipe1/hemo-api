'use strict'

module.exports = (sequelize, DataTypes) => {
  var Parameter = sequelize.define(
    'Parameter',
    {
      parameter_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.BIGINT
      },
      duration: DataTypes.INTEGER,
      blood_flow_rate: DataTypes.FLOAT,
      dialysate_bath: DataTypes.STRING,
      dialysate_additive: DataTypes.FLOAT,
      dialysate_flow_rate: DataTypes.FLOAT,
      ufv_goal: DataTypes.FLOAT,
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER,
      deleted_by: DataTypes.INTEGER
    },
    {
      paranoid: true,
      tableName: 'parameters',
      underscored: true
    }
  )
  Parameter.associate = function (models) {
    Parameter.belongsTo(models.PatientRegistry, {
      foreignKey: 'patient_registry_id',
      as: 'patientRegistry',
      onDelete: 'CASCADE'
    })
  }
  return Parameter
}
