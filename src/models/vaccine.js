'use strict'

module.exports = (sequelize, DataTypes) => {
  var Vaccine = sequelize.define(
    'Vaccine',
    {
      vaccine_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      first_dose_date: DataTypes.DATEONLY,
      second_dose_date: DataTypes.DATEONLY,
      third_dose_date: DataTypes.DATEONLY,
      booster_date: DataTypes.DATEONLY,
      flu_date: DataTypes.DATEONLY,
      pneumonia_spec: {
        type: DataTypes.TEXT
      },
      pneumonia_date: {
        type: DataTypes.DATEONLY
      },
      remarks: {
        type: DataTypes.TEXT
      },
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER,
      deleted_by: DataTypes.INTEGER
    },
    {
      paranoid: true,
      tableName: 'vaccines',
      underscored: true
    }
  )
  Vaccine.associate = (models) => {
    Vaccine.belongsTo(models.PatientRegistry, {
      foreignKey: 'patient_registry_id',
      as: 'patient_registry',
      onDelete: 'CASCADE'
    })
  }
  return Vaccine
}
