'use strict'
module.exports = (sequelize, DataTypes) => {
  let VascularAbnormal = sequelize.define(
    'VascularAbnormal',
    {
      vascular_id: DataTypes.INTEGER,
      abnormal_id: DataTypes.INTEGER,
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER
    },
    {
      timestamps: true,
      tableName: 'vasculars_abnormals',
      underscored: true
    }
  )
  return VascularAbnormal
}
