'use strict'
module.exports = (sequelize, DataTypes) => {
  const DialyzerBrand = sequelize.define(
    'DialyzerBrand',
    {
      dialyzer_brand_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      brand_name: DataTypes.STRING,
      description: DataTypes.STRING,
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER,
      deleted_by: DataTypes.INTEGER
    },
    {
      paranoid: true,
      tableName: 'dialyzer_brands',
      underscored: true
    }
  )
  DialyzerBrand.associate = (models) => {
    DialyzerBrand.hasMany(models.Dialyzer, {
      foreignKey: 'dialyzer_brand_id'
    })
    as: 'dialyzers',
    DialyzerBrand.hasMany(models.TemplateDialyzer, {
      foreignKey: 'dialyzer_brand_id',
      as: 'templateDialyzers'
    })
  }
  return DialyzerBrand
}
