'use strict'

module.exports = (sequelize, DataTypes) => {
  let TemplateAntiCoagulant = sequelize.define(
    'TemplateAntiCoagulant',
    {
      template_anti_coagulant_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.BIGINT
      },
      nss_flushing: DataTypes.STRING,
      nss_flushing_every: DataTypes.STRING,
      lmwh_iv: DataTypes.STRING,
      lmwh_iv_iu: DataTypes.STRING,
      ufh_iv: DataTypes.STRING,
      ufh_iv_iu: DataTypes.STRING,
      ufh_iv_iu_every: DataTypes.STRING,
      bleeding_desc: DataTypes.TEXT,
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER,
      deleted_by: DataTypes.INTEGER
    },
    {
      paranoid: true,
      tableName: 'template_anti_coagulants',
      underscored: true
    }
  )
  TemplateAntiCoagulant.associate = (models) => {
    TemplateAntiCoagulant.belongsTo(models.Patient, {
      foreignKey: 'patient_id',
      as: 'patient',
      onDelete: 'CASCADE'
    })
  }
  return TemplateAntiCoagulant
}
