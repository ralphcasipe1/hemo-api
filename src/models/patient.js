'use strict'

module.exports = (sequelize, DataTypes) => {
  var Patient = sequelize.define('Patient', {
    patient_id: {
      allowNull    : false,
      autoIncrement: true,
      primaryKey   : true,
      type         : DataTypes.BIGINT
    },
    bizbox_patient_no  : DataTypes.BIGINT,
    first_name         : DataTypes.STRING,
    middle_name        : DataTypes.STRING,
    last_name          : DataTypes.STRING,
    age                : DataTypes.INTEGER,
    sex                : DataTypes.INTEGER,
    civil_status       : DataTypes.INTEGER,
    room_no            : DataTypes.STRING,
    hospital_no        : DataTypes.STRING,
    attending_physician: DataTypes.STRING,
    created_by         : DataTypes.INTEGER,
    updated_by         : DataTypes.INTEGER,
    deleted_by         : DataTypes.INTEGER
  }, {
    getterMethods: {
      fullName: function () {
        return this.first_name + ' ' + this.middle_name + ' ' + this.last_name
      }
    },
    paranoid   : true,
    underscored: true,
    tableName  : 'patients'
  })
  Patient.associate = function (models) {
    Patient.hasMany(models.PatientRegistry, {
      foreignKey: 'patient_id',
      as        : 'patientRegistries'
    })

    Patient.hasMany(models.TemplateAntiCoagulant, {
      foreignKey: 'patient_id',
      as        : 'templateAntiCoagulants'
    })

    Patient.hasMany(models.TemplateDialyzer, {
      foreignKey: 'patient_id',
      as        : 'templateDialyzers'
    })

    Patient.hasMany(models.TemplateMedication, {
      foreignKey: 'patient_id',
      as        : 'templateMedications'
    })

    Patient.hasMany(models.TemplateParameter, {
      foreignKey: 'patient_id',
      as        : 'templateParameters'
    })

    Patient.hasMany(models.TemplatePhysicianOrder, {
      foreignKey: 'patient_id',
      as        : 'templatePhysicianOrders'
    })

    Patient.hasMany(models.TemplateWeight, {
      foreignKey: 'patient_id',
      as        : 'templateWeights'
    })
  }
  return Patient
}
