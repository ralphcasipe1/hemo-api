'use strict'

module.exports = (sequelize, DataTypes) => {
  let ObjectiveAssessment = sequelize.define(
    'ObjectiveAssessment',
    {
      patient_registry_id: DataTypes.INTEGER,
      objective_id: DataTypes.INTEGER,
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER
    },
    {
      timestamps: true,
      tableName: 'objective_assessments',
      underscored: true
    }
  )

  return ObjectiveAssessment
}
