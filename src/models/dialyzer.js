'use strict'

module.exports = (sequelize, DataTypes) => {
  let Dialyzer = sequelize.define(
    'Dialyzer',
    {
      dialyzer_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      type: DataTypes.STRING(30),
      dialyzer_date: DataTypes.DATE,
      count: DataTypes.INTEGER,
      d_t: DataTypes.STRING,
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER,
      deleted_by: DataTypes.INTEGER
    },
    {
      paranoid: true,
      tableName: 'dialyzers',
      underscored: true
    }
  )
  Dialyzer.associate = (models) => {
    Dialyzer.belongsTo(models.PatientRegistry, {
      foreignKey: 'patient_registry_id',
      as: 'patient_registry',
      onDelete: 'CASCADE'
    })
    Dialyzer.belongsTo(models.DialyzerBrand, {
      foreignKey: 'dialyzer_brand_id',
      as: 'dialyzerBrand',
      onDelete: 'CASCADE'
    })
    Dialyzer.belongsTo(models.DialyzerSize, {
      foreignKey: 'dialyzer_size_id',
      as: 'dialyzerSize',
      onDelete: 'CASCADE'
    })
  }
  return Dialyzer
}
