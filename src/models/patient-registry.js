'use strict'

module.exports = (sequelize, DataTypes) => {
  var PatientRegistry = sequelize.define('PatientRegistry', {
    patient_registry_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.BIGINT
    },
    registry_no: DataTypes.STRING,
    type: DataTypes.INTEGER,
    created_by: DataTypes.INTEGER,
    updated_by: DataTypes.INTEGER,
    deleted_by: DataTypes.INTEGER
  }, {
    paranoid: true,
    tableName: 'patient_registries',
    underscored: true
  })
  PatientRegistry.associate = function (models) {
    PatientRegistry.belongsTo(models.Patient, {
      foreignKey: 'patient_id',
      as: 'patient',
      onDelete: 'CASCADE'
    })

    PatientRegistry.belongsToMany(models.Objective, {
      through: models.ObjectiveAssessment,
      foreignKey: 'patient_registry_id',
      as: 'objectives'
    })

    PatientRegistry.belongsToMany(models.Subjective, {
      through: models.SubjectiveAssessment,
      foreignKey: 'patient_registry_id',
      as: 'subjectives'
    })

    PatientRegistry.hasOne(models.AntiCoagulant, {
      foreignKey: 'patient_registry_id',
      as: 'antiCoagulant'
    })

    PatientRegistry.hasOne(models.Dialyzer, {
      foreignKey: 'patient_registry_id',
      as: 'dialyzer'
    })

    PatientRegistry.hasOne(models.Parameter, {
      foreignKey: 'patient_registry_id',
      as: 'parameter'
    })

    PatientRegistry.hasOne(models.Vaccine, {
      foreignKey: 'patient_registry_id',
      as: 'vaccine'
    })

    PatientRegistry.hasOne(models.Weight, {
      foreignKey: 'patient_registry_id',
      as: 'weight'
    })

    PatientRegistry.hasOne(models.Vascular, {
      foreignKey: 'patient_registry_id',
      as: 'vascular'
    })

    PatientRegistry.hasOne(models.Catheter, {
      foreignKey: 'patient_registry_id',
      as: 'catheter'
    })

    PatientRegistry.hasOne(models.PreAssessment, {
      foreignKey: 'patient_registry_id',
      as: 'preAssessment'
    })

    PatientRegistry.hasMany(models.Medication, {
      foreignKey: 'patient_registry_id',
      as: 'medications'
    })

    PatientRegistry.hasMany(models.NurseNote, {
      foreignKey: 'patient_registry_id',
      as: 'nurseNotes'
    })

    PatientRegistry.hasMany(models.VitalSign, {
      foreignKey: 'patient_registry_id',
      as: 'vital_signs'
    })
  }
  return PatientRegistry
}
