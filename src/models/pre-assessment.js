'use strict'

module.exports = (sequelize, DataTypes) => {
  let PreAssessment = sequelize.define(
    'PreAssessment',
    {
      pre_assessment_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      hbs_ag: {
        type: DataTypes.INTEGER
      },
      anti_hbs: {
        type: DataTypes.INTEGER
      },
      anti_hcv: {
        type: DataTypes.INTEGER
      },
      anti_hiv: {
        type: DataTypes.INTEGER
      },
      // antigen: DataTypes.INTEGER,
      antigen_date: DataTypes.DATEONLY,
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER,
      deleted_by: DataTypes.INTEGER
    },
    {
      timestamps: true,
      paranoid: true,
      tableName: 'pre_assessments',
      underscored: true
    }
  )
  PreAssessment.associate = (models) => {
    PreAssessment.belongsTo(models.PatientRegistry, {
      foreignKey: 'patient_registry_id',
      as: 'patient_registry',
      onDelete: 'CASCADE'
    })
    PreAssessment.belongsTo(models.Mobility, {
      foreignKey: 'mobility_id',
      as: 'mobility',
      onDelete: 'CASCADE'
    })
    PreAssessment.belongsTo(models.User, {
      foreignKey: 'created_by',
      as: 'createdBy'
    })
  }
  return PreAssessment
}
