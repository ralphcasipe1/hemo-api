'use strict'

module.exports = (sequelize, DataTypes) => {
  let NurseNote = sequelize.define(
    'NurseNote',
    {
      nurse_note_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      note_desc: DataTypes.TEXT,
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER,
      deleted_by: DataTypes.INTEGER
    },
    {
      timestamps: true,
      paranoid: true,
      tableName: 'nurse_notes',
      underscored: true
    }
  )
  NurseNote.associate = (models) => {
    NurseNote.belongsTo(models.PatientRegistry, {
      foreignKey: 'patient_registry_id',
      as: 'patient_registry',
      onDelete: 'CASCADE'
    })
    NurseNote.belongsTo(models.User, {
      foreignKey: 'updated_by',
      as: 'updatedBy'
    })
  }
  return NurseNote
}
