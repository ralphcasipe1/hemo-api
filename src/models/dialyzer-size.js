'use strict'
module.exports = (sequelize, DataTypes) => {
  const DialyzerSize = sequelize.define(
    'DialyzerSize',
    {
      dialyzer_size_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      size_name: DataTypes.STRING,
      description: DataTypes.STRING,
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER,
      deleted_by: DataTypes.INTEGER
    },
    {
      paranoid: true,
      tableName: 'dialyzer_sizes',
      underscored: true
    }
  )
  DialyzerSize.associate = (models) => {
    DialyzerSize.hasMany(models.Dialyzer, {
      foreignKey: 'dialyzer_size_id',
      as: 'dialyzers'
    })
    DialyzerSize.hasMany(models.DialyzerSize, {
      foreignKey: 'dialyzer_size_id',
      as: 'templateDialyzers'
    })
  }
  return DialyzerSize
}
