'use strict'

const fs = require('fs')
const path = require('path')
const Sequelize = require('sequelize')
const basename = path.basename(__filename)
/* let env = process.env.NODE_ENV || 'development'
let config = require(__dirname + '/../config/config.json')[env] */
let db = {}

let sequelize
/* if (config.use_env_variable) {
  sequelize = new Sequelize(process.env[config.use_env_variable], config)
} else {
  sequelize = new Sequelize(config.database, config.username, config.password, config)
} */

sequelize = new Sequelize('hemo_development', 'postgres', 'test098!?', {
  host   : '127.0.0.1',
  port   : 5432,
  dialect: 'postgres',
  logging: false
})

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.')
  })
  .catch((error: Error) => {
    console.error('Unable to connect to the database:', error)
  })

fs
  .readdirSync(__dirname)
  .filter((file: string): Array<string> => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js')
  })
  .forEach((file: string) => {
    let model = sequelize['import'](path.join(__dirname, file))
    db[model.name] = model
  })

Object.keys(db).forEach((modelName: string) => {
  if (db[modelName].associate) {
    db[modelName].associate(db)
  }
})

db.sequelize = sequelize
db.Sequelize = Sequelize

module.exports = db
