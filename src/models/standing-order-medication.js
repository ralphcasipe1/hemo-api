'use strict'

module.exports = (sequelize, DataTypes) => {
  let TemplateMedication = sequelize.define(
    'TemplateMedication',
    {
      template_medication_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      generic: DataTypes.STRING(100),
      brand: DataTypes.STRING(100),
      preparation: DataTypes.TEXT,
      dosage: DataTypes.STRING(20),
      route: DataTypes.STRING(20),
      timing: DataTypes.STRING(50),
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER,
      deleted_by: DataTypes.INTEGER
    },
    {
      paranoid: true,
      tableName: 'template_medications',
      underscored: true
    }
  )
  TemplateMedication.associate = (models) => {
    TemplateMedication.belongsTo(models.Patient, {
      foreignKey: 'patient_id',
      as: 'patient',
      onDelete: 'CASCADE'
    })
  }
  return TemplateMedication
}
