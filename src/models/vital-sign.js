'use strict'

module.exports = (sequelize, DataTypes) => {
  let VitalSign = sequelize.define(
    'VitalSign',
    {
      vital_sign_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      blood_pressure: DataTypes.STRING,
      cardiac_arrest: DataTypes.STRING,
      respiratory_rate: DataTypes.STRING,
      temperature: DataTypes.STRING,
      oxygen_saturation: DataTypes.STRING,
      arterial_pressure: DataTypes.STRING,
      venous_pressure: DataTypes.STRING,
      transmembrane_pressure: DataTypes.STRING,
      blood_flow_rate: DataTypes.STRING,
      blood_glucose: DataTypes.STRING,
      remarks: {
        type: DataTypes.TEXT
      },
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER,
      deleted_by: DataTypes.INTEGER
    },
    {
      timestamps: true,
      paranoid: true,
      tableName: 'vital_signs',
      underscored: true
    }
  )
  VitalSign.associate = (models) => {
    VitalSign.belongsTo(models.PatientRegistry, {
      foreignKey: 'patient_registry_id',
      as: 'patient_registry',
      onDelete: 'CASCADE'
    })
    VitalSign.belongsTo(models.User, {
      foreignKey: 'created_by'

    })
  }
  return VitalSign
}
