'use strict'

module.exports = (sequelize, DataTypes) => {
  var AntiCoagulant = sequelize.define(
    'AntiCoagulant',
    {
      anti_coagulant_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.BIGINT
      },
      nss_flushing: DataTypes.STRING(20),
      nss_flushing_every: DataTypes.STRING(20),
      lmwh_iv: DataTypes.STRING(20),
      lmwh_iv_iu: DataTypes.STRING(20),
      ufh_iv: DataTypes.STRING(20),
      ufh_iv_iu: DataTypes.STRING(20),
      ufh_iv_iu_every: DataTypes.STRING(20),
      bleeding_desc: DataTypes.TEXT,
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER,
      deleted_by: DataTypes.INTEGER
    },
    {
      timestamps: true,
      paranoid: true,
      tableName: 'anti_coagulants',
      underscored: true
    }
  )
  AntiCoagulant.associate = (models) => {
    AntiCoagulant.belongsTo(models.PatientRegistry, {
      foreignKey: 'patient_registry_id',
      as: 'patientRegistry',
      onDelete: 'CASCADE'
    })
  }
  return AntiCoagulant
}
