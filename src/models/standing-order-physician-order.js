'use strict'

module.exports = (sequelize, DataTypes) => {
  let TemplatePhysicianOrder = sequelize.define(
    'TemplatePhysicianOrder',
    {
      template_physician_order_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      assessment: DataTypes.TEXT,
      medication: DataTypes.TEXT,
      procedure: DataTypes.TEXT,
      diagnostic_test: DataTypes.TEXT,
      other_remark: DataTypes.TEXT,
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER,
      deleted_by: DataTypes.INTEGER
    },
    {
      paranoid: true,
      tableName: 'template_physician_orders',
      underscored: true
    }
  )
  TemplatePhysicianOrder.associate = (models) => {
    TemplatePhysicianOrder.belongsTo(models.Patient, {
      foreignKey: 'patient_id',
      as: 'patient',
      onDelete: 'CASCADE'
    })
  }
  return TemplatePhysicianOrder
}
