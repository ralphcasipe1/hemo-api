'use strict'

module.exports = (sequelize, DataTypes) => {
  let TemplateParameter = sequelize.define(
    'TemplateParameter',
    {
      template_parameter_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      duration: DataTypes.INTEGER,
      blood_flow_rate: DataTypes.FLOAT,
      dialysate_bath: DataTypes.STRING,
      dialysate_additive: DataTypes.FLOAT,
      dialysate_flow_rate: DataTypes.FLOAT,
      created_by: {
        allowNull: false,
        type: DataTypes.INTEGER
      },
      updated_by: DataTypes.INTEGER,
      deleted_by: DataTypes.INTEGER
    },
    {
      paranoid: true,
      tableName: 'template_parameters',
      underscored: true
    }
  )
  TemplateParameter.associate = (models) => {
    TemplateParameter.belongsTo(models.Patient, {
      foreignKey: 'patient_id',
      as: 'patient',
      onDelete: 'CASCADE'
    })
  }
  return TemplateParameter
}
