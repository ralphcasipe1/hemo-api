'use strict'
module.exports = (sequelize, DataTypes) => {
  let Subjective = sequelize.define(
    'Subjective',
    {
      subjective_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      name: DataTypes.STRING,
      description: DataTypes.STRING,
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER,
      deleted_by: DataTypes.INTEGER
    },
    {
      timestamps: true,
      paranoid: true,
      tableName: 'subjectives',
      underscored: true
    }
  )
  Subjective.associate = (models) => {
    Subjective.belongsToMany(models.PatientRegistry, {
      through: models.SubjectiveAssessment,
      foreignKey: 'subjective_id',
      as: 'patientRegistries'
    })
    Subjective.belongsToMany(models.PatientRegistry, {
      through: 'post_subjective_assessments',
      foreignKey: 'subjective_id',
      as: 'postPatientRegistries'
    })
  }
  return Subjective
}
