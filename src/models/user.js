'use strict'

let _ = require('lodash')

module.exports = (sequelize, DataTypes) => {
  var User = sequelize.define('User', {
    user_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.BIGINT
    },
    first_name: {
      allowNull: false,
      type: DataTypes.STRING(100),
      get () {
        return _.upperFirst(this.getDataValue('first_name'))
      },
      set (val) {
        return this.setDataValue('first_name', _.toLower(val))
      }
    },
    middle_name: {
      type: DataTypes.STRING(100),
      get () {
        return _.upperFirst(this.getDataValue('middle_name'))
      },
      set (val) {
        return this.setDataValue('middle_name', _.toLower(val))
      }
    },
    last_name: {
      allowNull: false,
      type: DataTypes.STRING(100),
      get () {
        return _.upperFirst(this.getDataValue('last_name'))
      },
      set (val) {
        return this.setDataValue('last_name', _.toLower(val))
      }
    },
    id_number: {
      allowNull: false,
      type: DataTypes.STRING(20)
    },
    password: {
      allowNull: false,
      type: DataTypes.STRING
    },
    is_admin: {
      defaultValue: false,
      type: DataTypes.BOOLEAN
    },
    is_doctor: {
      defaultValue: false,
      type: DataTypes.BOOLEAN
    },
    created_by: {
      type: DataTypes.BIGINT
    },
    updated_by: {
      type: DataTypes.BIGINT
    },
    deleted_by: {
      type: DataTypes.BIGINT
    }
  }, {
    paranoid: true,
    underscored: true,
    tableName: 'users'
  })
  User.associate = function (models) {
    User.hasMany(models.Medication, {
      foreignKey: 'created_by',
      as: 'createdBy'
    })

    User.hasMany(models.VitalSign, {
      foreignKey: 'created_by'
    })
  }
  return User
}
