'use strict';

let authenticationController =
  require('../../controllers').authenticationController;
let dialyzerBrandController = require('../../controllers').dialyzerBrandController;

module.exports = app => {
  app
    .route('/dialyzer_brands')
    .get(authenticationController.unauthorizedException, dialyzerBrandController.index)
    .post(
      authenticationController.unauthorizedException,
      dialyzerBrandController.create
    );

  app
    .route('/dialyzer_brands/:dialyzerBrandId')
    .get(authenticationController.unauthorizedException, dialyzerBrandController.select)
    .patch(
      authenticationController.unauthorizedException,
      dialyzerBrandController.update
    )
    .delete(
      authenticationController.unauthorizedException,
      dialyzerBrandController.destroy
    );
}