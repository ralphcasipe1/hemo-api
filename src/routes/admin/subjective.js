'use strict';

let authenticationController =
  require('../../controllers').authenticationController;
let subjectiveController = require('../../controllers').subjectiveController;

module.exports = app => {
  app
    .route('/subjectives')
    .get(authenticationController.unauthorizedException, subjectiveController.index)
    .post(
      authenticationController.unauthorizedException,
      subjectiveController.create
    );

  app
    .route('/subjectives/:subjectiveId')
    .get(authenticationController.unauthorizedException, subjectiveController.select)
    .patch(
      authenticationController.unauthorizedException,
      subjectiveController.update
    )
    .delete(
      authenticationController.unauthorizedException,
      subjectiveController.destroy
    );
}