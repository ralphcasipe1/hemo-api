'use strict';

let authenticationController =
  require('../../controllers').authenticationController;
let objectiveController = require('../../controllers').objectiveController;

module.exports = app => {
  app
    .route('/objectives')
    .get(authenticationController.unauthorizedException, objectiveController.index)
    .post(
      authenticationController.unauthorizedException,
      objectiveController.create
    );

  app
    .route('/objectives/:objectiveId')
    .get(authenticationController.unauthorizedException, objectiveController.select)
    .patch(
      authenticationController.unauthorizedException,
      objectiveController.update
    )
    .delete(
      authenticationController.unauthorizedException,
      objectiveController.destroy
    );
}