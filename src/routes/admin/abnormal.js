'use strict';

let authenticationController =
  require('../../controllers').authenticationController;
let abnormalController = require('../../controllers').abnormalController;

module.exports = app => {
  app
    .route('/abnormals')
    .get(authenticationController.unauthorizedException, abnormalController.index)
    .post(
      authenticationController.unauthorizedException,
      abnormalController.create
    );

  app
    .route('/abnormals/:abnormalId')
    .get(authenticationController.unauthorizedException, abnormalController.select)
    .patch(
      authenticationController.unauthorizedException,
      abnormalController.update
    )
    .delete(
      authenticationController.unauthorizedException,
      abnormalController.destroy
    );
}