'use strict';

let authenticationController =
  require('../../controllers').authenticationController;
let dialyzerSizeController = require('../../controllers').dialyzerSizeController;

module.exports = app => {
  app
    .route('/dialyzer_sizes')
    .get(authenticationController.unauthorizedException, dialyzerSizeController.index)
    .post(
      authenticationController.unauthorizedException,
      dialyzerSizeController.create
    );

  app
    .route('/dialyzer_sizes/:dialyzerSizeId')
    .get(authenticationController.unauthorizedException, dialyzerSizeController.select)
    .patch(
      authenticationController.unauthorizedException,
      dialyzerSizeController.update
    )
    .delete(
      authenticationController.unauthorizedException,
      dialyzerSizeController.destroy
    );
}