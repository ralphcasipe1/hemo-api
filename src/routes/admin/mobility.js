'use strict';

let authenticationController =
  require('../../controllers').authenticationController;
let mobilityController = require('../../controllers').mobilityController;

module.exports = app => {
  app
    .route('/mobilities')
    .get(authenticationController.unauthorizedException, mobilityController.index)
    .post(
      authenticationController.unauthorizedException,
      mobilityController.create
    );

  app
    .route('/mobilities/:mobilityId')
    .get(authenticationController.unauthorizedException, mobilityController.select)
    .patch(
      authenticationController.unauthorizedException,
      mobilityController.update
    )
    .delete(
      authenticationController.unauthorizedException,
      mobilityController.destroy
    );
}