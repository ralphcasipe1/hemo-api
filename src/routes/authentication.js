'use strict';

let authenticationController = require('../controllers').authenticationController;

module.exports = app => {
  app
    .route('/authentication')
    .post(authenticationController.authenticate);
}