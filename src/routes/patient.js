'use strict'

let authenticationController =
  require('../controllers').authenticationController
let patientController = require('../controllers').patientController

module.exports = app => {
  app
    .route('/patients')
    .get(authenticationController.unauthorizedException, patientController.index)
    .post(authenticationController.unauthorizedException, patientController.create)

  app
    .route('/patients/:patientId')
    .get(authenticationController.unauthorizedException, patientController.select)
}
