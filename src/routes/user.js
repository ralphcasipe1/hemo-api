'use strict'

let authenticationController =
  require('../controllers').authenticationController
let userController = require('../controllers').userController

module.exports = app => {
  app
    .route('/users')
    .get(userController.index)
    .post(
      // authenticationController.unauthorizedException,
      userController.create
    )

  app
    .route('/users/:userId')
    .get(userController.select)
    .patch(
      authenticationController.unauthorizedException,
      userController.update
    )
    .delete(
      authenticationController.unauthorizedException,
      userController.destroy
    )
}
