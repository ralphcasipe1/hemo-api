'use strict';

let parameterController = require('../../../controllers').parameterController;
let authenticationController = require('../../../controllers').authenticationController;

module.exports = app => {
  app
    .route('/patient_registries/:patientRegistryId/parameters')
    .get(authenticationController.unauthorizedException, parameterController.index)
    .post(authenticationController.unauthorizedException, parameterController.create);

  app
    .route('/patient_registries/:patientRegistryId/parameters/:parameterId')
    .get(authenticationController.unauthorizedException, parameterController.destroy)
    .patch(authenticationController.unauthorizedException, parameterController.update);
}
