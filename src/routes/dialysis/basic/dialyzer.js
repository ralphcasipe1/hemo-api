'use strict';

let dialyzerController = require('../../../controllers').dialyzerController;
let authenticationController = require('../../../controllers').authenticationController;

module.exports = app => {
  app
    .route('/patient_registries/:patientRegistryId/dialyzers')
    .get(authenticationController.unauthorizedException, dialyzerController.index)
    .post(authenticationController.unauthorizedException, dialyzerController.create);

  app
    .route('/patient_registries/:patientRegistryId/dialyzers/:dialyzerId')
    .get(authenticationController.unauthorizedException, dialyzerController.destroy)
    .patch(authenticationController.unauthorizedException, dialyzerController.update);
}
