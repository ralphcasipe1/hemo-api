'use strict';

let weightController = require('../../../controllers').weightController;
let authenticationController = require('../../../controllers').authenticationController;

module.exports = app => {
  app
    .route('/patient_registries/:patientRegistryId/weights')
    .get(authenticationController.unauthorizedException, weightController.index)
    .post(authenticationController.unauthorizedException, weightController.create);

  app
    .route('/patient_registries/:patientRegistryId/weights/:weightId')
    .get(authenticationController.unauthorizedException, weightController.destroy)
    .patch(authenticationController.unauthorizedException, weightController.update);
}
