'use strict';

let vaccineController = require('../../../controllers').vaccineController;
let authenticationController = require('../../../controllers').authenticationController;

module.exports = app => {
  app
    .route('/patient_registries/:patientRegistryId/vaccines')
    .get(authenticationController.unauthorizedException, vaccineController.index)
    .post(authenticationController.unauthorizedException, vaccineController.create);

  app
    .route('/patient_registries/:patientRegistryId/vaccines/:vaccineId')
    .get(authenticationController.unauthorizedException, vaccineController.destroy)
    .patch(authenticationController.unauthorizedException, vaccineController.update);
}
