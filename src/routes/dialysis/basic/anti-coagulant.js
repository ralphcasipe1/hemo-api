'use strict';

let antiCoagulantController = require('../../../controllers').antiCoagulantController;
let authenticationController = require('../../../controllers').authenticationController;

module.exports = app => {
  app
    .route('/patient_registries/:patientRegistryId/anti_coagulants')
    .get(authenticationController.unauthorizedException, antiCoagulantController.index)
    .post(authenticationController.unauthorizedException, antiCoagulantController.create);

  app
    .route('/patient_registries/:patientRegistryId/anti_coagulants/:antiCoagulantId')
    .get(authenticationController.unauthorizedException, antiCoagulantController.destroy)
    .patch(authenticationController.unauthorizedException, antiCoagulantController.update);
}
