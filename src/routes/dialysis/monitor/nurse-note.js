'use strict';

let nurseNoteController = require('../../../controllers').nurseNoteController;
let authenticationController = require('../../../controllers').authenticationController;

module.exports = app => {
  app
    .route('/patient_registries/:patientRegistryId/nurse_notes')
    .get(authenticationController.unauthorizedException, nurseNoteController.index)
    .post(authenticationController.unauthorizedException, nurseNoteController.create);

  app
    .route('/patient_registries/:patientRegistryId/nurse_notes/:nurseNoteId')
    .get(authenticationController.unauthorizedException, nurseNoteController.destroy)
    .patch(authenticationController.unauthorizedException, nurseNoteController.update);
}
