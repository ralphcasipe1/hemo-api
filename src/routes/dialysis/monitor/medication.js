'use strict';

let medicationController = require('../../../controllers').medicationController;
let authenticationController = require('../../../controllers').authenticationController;

module.exports = app => {
  app
    .route('/patient_registries/:patientRegistryId/medications')
    .get(authenticationController.unauthorizedException, medicationController.index)
    .post(authenticationController.unauthorizedException, medicationController.create);

  app
    .route('/patient_registries/:patientRegistryId/medications/:medicationId')
    .get(authenticationController.unauthorizedException, medicationController.destroy)
    .patch(authenticationController.unauthorizedException, medicationController.update);
}
