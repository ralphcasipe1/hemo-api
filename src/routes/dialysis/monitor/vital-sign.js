'use strict';

let vitalSignController = require('../../../controllers').vitalSignController;
let authenticationController = require('../../../controllers').authenticationController;

module.exports = app => {
  app
    .route('/patient_registries/:patientRegistryId/vital_signs')
    .get(authenticationController.unauthorizedException, vitalSignController.index)
    .post(authenticationController.unauthorizedException, vitalSignController.create);

  app
    .route('/patient_registries/:patientRegistryId/vital_signs/:nurseNoteId')
    .get(authenticationController.unauthorizedException, vitalSignController.destroy)
    .patch(authenticationController.unauthorizedException, vitalSignController.update);
}
