'use strict';

let authenticationController = require('../../../controllers').authenticationController;
let objectiveAssessmentController = require('../../../controllers').objectiveAssessmentController;

module.exports = app => {
  app
    .route('/patient_registries/:patientRegistryId/objective_assessments')
    .get(
      authenticationController.unauthorizedException,
      objectiveAssessmentController.index
    )
    .post(
      authenticationController.unauthorizedException,
      objectiveAssessmentController.create
    )
    .delete(
      authenticationController.unauthorizedException,
      objectiveAssessmentController.destroy
    );
  app
    .route('/patient_registries/:patientRegistryId/objective_assessments/:objectiveAssessmentId')
    .get(
      authenticationController.unauthorizedException,
      objectiveAssessmentController.select
    )
    .patch(
      authenticationController.unauthorizedException,
      objectiveAssessmentController.create
    );
}
