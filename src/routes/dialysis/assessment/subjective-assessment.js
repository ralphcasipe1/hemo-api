'use strict';

let authenticationController = require('../../../controllers').authenticationController;
let subjectiveAssessmentController = require('../../../controllers').subjectiveAssessmentController;

module.exports = app => {
  app
    .route('/patient_registries/:patientRegistryId/subjective_assessments')
    .get(
      authenticationController.unauthorizedException,
      subjectiveAssessmentController.index
    )
    .post(
      authenticationController.unauthorizedException,
      subjectiveAssessmentController.create
    )
    .delete(
      authenticationController.unauthorizedException,
      subjectiveAssessmentController.destroy
    );
  app
    .route('/patient_registries/:patientRegistryId/subjective_assessments/:subjectiveAssessmentId')
    .get(
      authenticationController.unauthorizedException,
      subjectiveAssessmentController.select
    )
    .patch(
      authenticationController.unauthorizedException,
      subjectiveAssessmentController.create
    );
}
