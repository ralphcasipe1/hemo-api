'use strict';

let vascularController = require('../../../controllers').vascularController;
let authenticationController = require('../../../controllers').authenticationController;

module.exports = app => {
  app
    .route('/patient_registries/:patientRegistryId/vasculars')
    .get(authenticationController.unauthorizedException, vascularController.index)
    .post(authenticationController.unauthorizedException, vascularController.create);

  app
    .route('/patient_registries/:patientRegistryId/vasculars/:vascularId')
    .get(authenticationController.unauthorizedException, vascularController.destroy)
    .patch(authenticationController.unauthorizedException, vascularController.update);
}
