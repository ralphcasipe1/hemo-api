'use strict';

let authenticationController = require('../../../controllers').authenticationController;
let preAssessmentController = require('../../../controllers').preAssessmentController;

module.exports = app => {
  app
    .route('/patient_registries/:patientRegistryId/pre_assessments')
    .get(authenticationController.unauthorizedException, preAssessmentController.index)
    .post(authenticationController.unauthorizedException, preAssessmentController.create);

  app
    .route('/patient_registries/:patientRegistryId/pre_assessments/:preAssessmentId')
    .get(authenticationController.unauthorizedException, preAssessmentController.destroy)
    .patch(authenticationController.unauthorizedException, preAssessmentController.update);
}
