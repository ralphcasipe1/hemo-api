'use strict';

let catheterController = require('../../../controllers').catheterController;
let authenticationController = require('../../../controllers').authenticationController;

module.exports = app => {
  app
    .route('/patient_registries/:patientRegistryId/catheters')
    .get(authenticationController.unauthorizedException, catheterController.index)
    .post(authenticationController.unauthorizedException, catheterController.create);

  app
    .route('/patient_registries/:patientRegistryId/catheters/:catheterId')
    .get(authenticationController.unauthorizedException, catheterController.destroy)
    .patch(authenticationController.unauthorizedException, catheterController.update);
}
