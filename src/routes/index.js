'use strict';

let abnormalRoute = require('./admin/abnormal');
let dialyzerBrandRoute = require('./admin/dialyzer-brand');
let dialyzerSizeRoute = require('./admin/dialyzer-size');
let mobilityRoute = require('./admin/mobility');
let objectiveRoute = require('./admin/objective');
let subjectiveRoute = require('./admin/subjective');

let authenticationRoute = require('./authentication');
let bizboxPatientRoute = require('./bizbox-patient');
let patientRoute = require('./patient');
let patientRegistryRoute = require('./patient-registry');
let userRoute = require('./user');

let antiCoagulantRoute = require('./dialysis/basic/anti-coagulant');
let dialyzerRoute = require('./dialysis/basic/dialyzer');
let parameterRoute = require('./dialysis/basic/parameter');
let vaccineRoute = require('./dialysis/basic/vaccine');
let weightRoute = require('./dialysis/basic/weight');
let medicationRoute = require('./dialysis/monitor/medication');
let nurseNoteRoute = require('./dialysis/monitor/nurse-note');
let vitalSignRoute = require('./dialysis/monitor/vital-sign');

let catheterRoute = require('./dialysis/assessment/catheter');
let objectiveAssessmentRoute = require('./dialysis/assessment/objective-assessment');
let preAssessmentRoute = require('./dialysis/assessment/pre-assessment');
let subjectiveAssessmentRoute = require('./dialysis/assessment/subjective-assessment');
let vascularRoute = require('./dialysis/assessment/vascular');

let standingOrderAntiCoagulantRoute = require('./standing-order/standing-order-anti-coagulant');
let standingOrderDialyzerRoute = require('./standing-order/standing-order-dialyzer');
let standingOrderMedicationRoute = require('./standing-order/standing-order-medication');
let standingOrderParameterRoute = require('./standing-order/standing-order-parameter');
let standingOrderPhysicianOrderRoute = require('./standing-order/standing-order-physician-order');
let standingOrderWeightRoute = require('./standing-order/standing-order-weight');

module.exports = (app, router) => {
  abnormalRoute(router);
  antiCoagulantRoute(router);
  authenticationRoute(router);
  bizboxPatientRoute(router);
  catheterRoute(router);
  dialyzerRoute(router);
  dialyzerBrandRoute(router)
  dialyzerSizeRoute(router);
  medicationRoute(router);
  mobilityRoute(router);
  objectiveAssessmentRoute(router)
  objectiveRoute(router);
  nurseNoteRoute(router);
  patientRoute(router);
  patientRegistryRoute(router);
  parameterRoute(router);
  preAssessmentRoute(router);
  standingOrderAntiCoagulantRoute(router);
  standingOrderDialyzerRoute(router);
  standingOrderMedicationRoute(router);
  standingOrderParameterRoute(router);
  standingOrderPhysicianOrderRoute(router);
  standingOrderWeightRoute(router);
  subjectiveAssessmentRoute(router);
  subjectiveRoute(router);
  userRoute(router);
  weightRoute(router);
  vaccineRoute(router);
  vascularRoute(router);
  vitalSignRoute(router);
  
  app.use('/api', router);
}