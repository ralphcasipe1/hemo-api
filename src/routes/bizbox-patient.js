'use strict';

let authenticationController = 
  require('../controllers').authenticationController;
let bizboxPatientController = require('../controllers').bizboxPatientController;

module.exports = app => {
  app
    .route('/bizbox/patients/date/:date')
    .get(authenticationController.unauthorizedException, bizboxPatientController.index)

  app
    .route('/bizbox/patients/:patientId')
    .get(authenticationController.unauthorizedException, bizboxPatientController.select);
}