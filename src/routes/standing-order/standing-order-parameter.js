'use strict'

let authenticationController =
  require('../../controllers').authenticationController
let standingOrderParameterController = require('../../controllers').standingOrderParameterController

module.exports = app => {
  app
    .route('/patients/:patientId/standing_order_parameters')
    .get(authenticationController.unauthorizedException, standingOrderParameterController.index)
    .post(
      authenticationController.unauthorizedException,
      standingOrderParameterController.create
    )

  app
    .route('/patients/:patientId/standing_order_parameters/:standingOrderParameterId')
    .get(standingOrderParameterController.select)
    .patch(
      authenticationController.unauthorizedException,
      standingOrderParameterController.update
    )
    .delete(
      authenticationController.unauthorizedException,
      standingOrderParameterController.destroy
    )
}
