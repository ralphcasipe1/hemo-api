'use strict'

let authenticationController =
  require('../../controllers').authenticationController
let standingOrderDialyzerController = require('../../controllers').standingOrderDialyzerController

module.exports = app => {
  app
    .route('/patients/:patientId/standing_order_dialyzers')
    .get(authenticationController.unauthorizedException, standingOrderDialyzerController.index)
    .post(
      authenticationController.unauthorizedException,
      standingOrderDialyzerController.create
    )

  app
    .route('/patients/:patientId/standing_order_dialyzers/:standingOrderDialyzerId')
    .get(standingOrderDialyzerController.select)
    .patch(
      authenticationController.unauthorizedException,
      standingOrderDialyzerController.update
    )
    .delete(
      authenticationController.unauthorizedException,
      standingOrderDialyzerController.destroy
    )
}
