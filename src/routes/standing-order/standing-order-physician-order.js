'use strict';

let authenticationController =
  require('../../controllers').authenticationController;
let standingOrderPhysicianOrderController = require('../../controllers').standingOrderPhysicianOrderController;

module.exports = app => {
  app
    .route('/patients/:patientId/standing_order_physician_orders')
    .get(authenticationController.unauthorizedException, standingOrderPhysicianOrderController.index)
    .post(
      authenticationController.unauthorizedException,
      standingOrderPhysicianOrderController.create
    );

  app
    .route('/patients/:patientId/standing_order_physician_orders')
    .get(standingOrderPhysicianOrderController.select)
    .patch(
      authenticationController.unauthorizedException,
      standingOrderPhysicianOrderController.update
    )
    .delete(
      authenticationController.unauthorizedException,
      standingOrderPhysicianOrderController.destroy
    );
};