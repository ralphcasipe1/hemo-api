'use strict'

let authenticationController =
  require('../../controllers').authenticationController
let standingOrderMedicationController = require('../../controllers').standingOrderMedicationController

module.exports = app => {
  app
    .route('/patients/:patientId/standing_order_medications')
    .get(authenticationController.unauthorizedException, standingOrderMedicationController.index)
    .post(
      authenticationController.unauthorizedException,
      standingOrderMedicationController.create
    )

  app
    .route('/patients/:patientId/standing_order_medications/:standingOrderMedicationId')
    .get(standingOrderMedicationController.select)
    .patch(
      authenticationController.unauthorizedException,
      standingOrderMedicationController.update
    )
    .delete(
      authenticationController.unauthorizedException,
      standingOrderMedicationController.destroy
    )
}
