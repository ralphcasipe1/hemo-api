'use strict'

let authenticationController =
  require('../../controllers').authenticationController
let standingOrderWeightController = require('../../controllers').standingOrderWeightController

module.exports = app => {
  app
    .route('/patients/:patientId/standing_order_weights')
    .get(authenticationController.unauthorizedException, standingOrderWeightController.index)
    .post(
      authenticationController.unauthorizedException,
      standingOrderWeightController.create
    )

  app
    .route('/patients/:patientId/standing_order_weights/:standingOrderWeightId')
    .get(standingOrderWeightController.select)
    .patch(
      authenticationController.unauthorizedException,
      standingOrderWeightController.update
    )
    .delete(
      authenticationController.unauthorizedException,
      standingOrderWeightController.destroy
    )
}
