const authenticationController =
  require('../../controllers').authenticationController
const standingOrderAntiCoagulantController = require('../../controllers/').standingOrderAntiCoagulantController

module.exports = app => {
  app
    .route('/patients/:patientId/standing_order_anti_coagulants')
    .get(authenticationController.unauthorizedException, standingOrderAntiCoagulantController.index)
    .post(
      authenticationController.unauthorizedException,
      standingOrderAntiCoagulantController.create
    )

  app
    .route('/patients/:patientId/standing_order_anti_coagulants/:standingOrderAntiCoagulantId')
    .get(standingOrderAntiCoagulantController.select)
    .patch(
      authenticationController.unauthorizedException,
      standingOrderAntiCoagulantController.update
    )
    .delete(
      authenticationController.unauthorizedException,
      standingOrderAntiCoagulantController.destroy
    )
}
