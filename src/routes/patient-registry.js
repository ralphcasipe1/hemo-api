'use strict';

let authenticationController = 
  require('../controllers').authenticationController;

let patientRegistryController = 
  require('../controllers').patientRegistryController;

module.exports = app => {
  app
    .route('/patients/:patientId/patient_registries')
    .get(
      authenticationController.unauthorizedException,
      patientRegistryController.index
    )
    .post(
      authenticationController.unauthorizedException,
      patientRegistryController.create
    );

  app
    .route('/patients/:patientId/patient_registries/:patientRegistryId')
    .get(patientRegistryController.select);
}