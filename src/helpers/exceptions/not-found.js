'use strict'

/**
 * Error exception for not found
 * @return {Error} throw Error for Not Found
 */
module.exports = () => {
  let error = new Error('Not Found')
  error.status = 404

  throw error
}
