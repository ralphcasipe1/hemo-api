import {
  templateDialyzer as templateDialyzerController,
  user as userController
} from '../controllers';

export default function (app) {
  app
    .route('/patients/:patientId/so_dialyzers')
    .get(userController.userRequired, templateDialyzerController.findAll)
    .post(userController.userRequired, templateDialyzerController.create);
  app
    .route('/patients/:patientId/so_dialyzers/:templateDialyzerId')
    .get(userController.userRequired, templateDialyzerController.findOne)
    .patch(userController.userRequired, templateDialyzerController.update)
    .delete(userController.userRequired, templateDialyzerController.destroy);
}
