import {
  dialyzerSize as dialyzerSizeController,
  user as userController
} from '../controllers';

export default function (app) {
  app
    .route('/dialyzer_sizes')
    .get(userController.userRequired, dialyzerSizeController.list)
    .post(userController.userRequired, dialyzerSizeController.create);
  app
    .route('/dialyzer_sizes/:dialyzerSizeId')
    .get(userController.userRequired, dialyzerSizeController.select)
    .patch(userController.userRequired, dialyzerSizeController.update)
    .delete(userController.userRequired, dialyzerSizeController.destroy);
}
