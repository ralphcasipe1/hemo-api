import {
  parameter as parameterController,
  user as userController
} from '../controllers';

export default function (app) {
  app
    .route('/patient_registries/:patientRegistryId/parameters')
    .get(userController.userRequired, parameterController.get)
    .post(userController.userRequired, parameterController.create);
  app
    .route('/patient_registries/:patientRegistryId/parameters/:parameterId')
    .get(userController.userRequired, parameterController.select)
    .patch(userController.userRequired, parameterController.update)
    .delete(userController.userRequired, parameterController.destroy);
}
