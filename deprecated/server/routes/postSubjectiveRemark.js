import {
  postSubjectiveRemark as postSubjectiveRemarkController,
  user as userController
} from '../controllers';

export default function (app) {
  app
    .route('/patient_registries/:patientRegistryId/post_subjective_remarks')
    .get(userController.userRequired, postSubjectiveRemarkController.get)
    .post(userController.userRequired, postSubjectiveRemarkController.create);
  app
    .route('/patient_registries/:patientRegistryId/post_subjective_remarks/:postSubjectiveRemarkId')
    .delete(
      userController.userRequired,
      postSubjectiveRemarkController.destroy
    );
}
