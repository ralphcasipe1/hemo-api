import {
  mobility as mobilityController,
  user as userController
} from '../controllers';

export default function (app) {
  app
    .route('/mobilities')
    .get(userController.userRequired, mobilityController.list)
    .post(userController.userRequired, mobilityController.create);
  app
    .route('/mobilities/:mobilityId')
    .get(userController.userRequired, mobilityController.select)
    .patch(userController.userRequired, mobilityController.update)
    .delete(userController.userRequired, mobilityController.destroy);
}
