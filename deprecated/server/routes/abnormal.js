import {
  user as userController,
  AbnormalController
} from '../controllers';

const abnormalController = new AbnormalController();

export default function (app) {
  app
    .route('/abnormals')
    .get(userController.userRequired, abnormalController.list)
    .post(userController.userRequired, abnormalController.create);
  app
    .route('/abnormals/:abnormalId')
    .get(userController.userRequired, abnormalController.select)
    .patch(userController.userRequired, abnormalController.update)
    .delete(userController.userRequired, abnormalController.destroy);
}
