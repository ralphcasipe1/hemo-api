import {
  templateAntiCoagulant as templateAntiCoagulantController,
  user as userController
} from '../controllers';

export default function (app) {
  app
    .route('/patients/:patientId/so_anti_coagulants')
    .get(
      userController.userRequired,
      templateAntiCoagulantController.findAll
    )
    .post(userController.userRequired, templateAntiCoagulantController.create);
  app
    .route('/patients/:patientId/so_anti_coagulants/:templateAntiCoagulantId')
    .get(userController.userRequired, templateAntiCoagulantController.findOne)
    .patch(userController.userRequired, templateAntiCoagulantController.update)
    .delete(
      userController.userRequired,
      templateAntiCoagulantController.destroy
    );
}
