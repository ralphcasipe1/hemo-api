import abnormalRoute from './abnormal';
import antiCoagulantRoute from './antiCoagulant';
import authRoute from './auth';
import bizboxHemoPatientRoute from './bizboxHemoPatient';
import bizboxPhysicianRoute from './bizboxPhysician';
import catheterRoute from './catheter';
import catheterAbnormalRoute from './catheterAbormal';
import dialyzerRoute from './dialyzer';
import dialyzerBrandRoute from './dialyzerBrand';
import dialyzerSizeRoute from './dialyzerSize';
import medicationRoute from './medication';
import mobilityRoute from './mobility';
import nurseNurseRoute from './nurseNote';
import objectiveRoute from './objective';
import objectiveAssessmentRoute from './objectiveAssessment';
import objectiveRemarkRoute from './objectiveRemark';
import parameterRoute from './parameter';
import patientRoute from './patient';
import patientRegistryRoute from './patientRegistry';
import physicianRoute from './physician';
import physicianOrderRoute from './physicianOrder';
import postObjectiveAssessmentRoute from './postObjectiveAssessment';
import postObjectiveRemarkRoute from './postObjectiveRemark';
import postSubjectiveAssessmentRoute from './postSubjectiveAssessment';
import postSubjectiveRemarkRoute from './postSubjectiveRemark';
import preAssessmentRoute from './preAssessment';
import subjectiveRoute from './subjective';
import subjectiveAssessmentRoute from './subjectiveAssessment';
import subjectiveRemarkRoute from './subjectiveRemark';
import sOAntiCoagulantRoute from './templateAntiCoagulant';
import sODialyzerRoute from './templateDialyzer';
import sOMedicationRoute from './templateMedication';
import sOParameterRoute from './templateParameter';
import sOPhysicianOrderRouter from './templatePhysicianOrder';
import sOWeightRoute from './templateWeight';
import userRoute from './user';
import vaccineRoute from './vaccine';
import vascularRoute from './vascular';
import vascularAbnormalRoute from './vascularAbnormal';
import vitalSignRoute from './vitalSign';
import weightRoute from './weight';

export default function (app, router) {
  abnormalRoute(router);
  antiCoagulantRoute(router);
  authRoute(router);
  bizboxHemoPatientRoute(router);
  bizboxPhysicianRoute(router);
  catheterRoute(router);
  catheterAbnormalRoute(router);
  dialyzerRoute(router);
  dialyzerBrandRoute(router);
  dialyzerSizeRoute(router);
  medicationRoute(router);
  mobilityRoute(router);
  nurseNurseRoute(router);
  objectiveRoute(router);
  objectiveAssessmentRoute(router);
  objectiveRemarkRoute(router);
  parameterRoute(router);
  patientRoute(router);
  patientRegistryRoute(router);
  physicianRoute(router);
  physicianOrderRoute(router);
  postObjectiveAssessmentRoute(router);
  postObjectiveRemarkRoute(router);
  postSubjectiveAssessmentRoute(router);
  postSubjectiveRemarkRoute(router);
  preAssessmentRoute(router);
  subjectiveRoute(router);
  subjectiveAssessmentRoute(router);
  subjectiveRemarkRoute(router);
  sOAntiCoagulantRoute(router);
  sODialyzerRoute(router);
  sOMedicationRoute(router);
  sOParameterRoute(router);
  sOPhysicianOrderRouter(router);
  sOWeightRoute(router);
  userRoute(router);
  vaccineRoute(router);
  vascularRoute(router);
  vascularAbnormalRoute(router);
  vitalSignRoute(router);
  weightRoute(router);
  app.use('/api', router);
}