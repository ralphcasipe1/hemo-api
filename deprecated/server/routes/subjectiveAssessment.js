import {
  subjectiveAssessment as subjectiveAssessmentController,
  user as userController
} from '../controllers';

export default function (app) {
  app
    .route('/patient_registries/:patientRegistryId/subjective_assessments')
    .get(
      userController.userRequired,
      subjectiveAssessmentController.listSubjectives
    )
    .post(
      userController.userRequired,
      subjectiveAssessmentController.setSubjectives
    )
    .delete(
      userController.userRequired,
      subjectiveAssessmentController.removeSubjectives
    );
  app
    .route('/patient_registries/:patientRegistryId/subjective_assessments/:subjectiveAssessmentId')
    .get(
      userController.userRequired,
      subjectiveAssessmentController.selectSubjective
    )
    .patch(
      userController.userRequired,
      subjectiveAssessmentController.setSubjectives
    );
}
