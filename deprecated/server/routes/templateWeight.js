import {
  templateWeight as templateWeightController,
  user as userController
} from '../controllers';

export default function (app) {
  app
    .route('/patients/:patientId/so_weights')
    .get(userController.userRequired, templateWeightController.findAll)
    .post(userController.userRequired, templateWeightController.create);
  app
    .route('/patients/:patientId/so_weights/:templateWeightId')
    .get(userController.userRequired, templateWeightController.findOne)
    .patch(userController.userRequired, templateWeightController.update)
    .delete(userController.userRequired, templateWeightController.destroy);
}
