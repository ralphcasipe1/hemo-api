import {
  physician as physicianController,
  user as userController
} from '../controllers';

export default function (app) {
  app
    .route('/physicians')
    .get(userController.userRequired, physicianController.list)
    .post(userController.userRequired, physicianController.create);
}
