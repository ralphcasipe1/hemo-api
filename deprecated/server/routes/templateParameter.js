import {
  templateParameter as templateParameterController,
  user as userController
} from '../controllers';

export default function (app) {
  app
    .route('/patients/:patientId/so_parameters')
    .get(userController.userRequired, templateParameterController.findAll)
    .post(userController.userRequired, templateParameterController.create);
  app
    .route('/patients/:patientId/so_parameters/:templateParameterId')
    .get(userController.userRequired, templateParameterController.findOne)
    .patch(userController.userRequired, templateParameterController.update)
    .delete(userController.userRequired, templateParameterController.destroy);
}
