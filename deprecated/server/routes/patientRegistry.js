import {
  patientRegistry as patientRegistryController,
  user as userController
} from '../controllers';

export default function (app) {
  app
    .route('/patients/:patientId/patient_registries')
    .get(userController.userRequired, patientRegistryController.list)
    .post(userController.userRequired, patientRegistryController.create);
  app
    .route('/patients/:patientId/patient_registries/:patientRegistryId')
    .get(userController.userRequired, patientRegistryController.select)
    .patch(userController.userRequired, patientRegistryController.update)
    .delete(userController.userRequired, patientRegistryController.destroy);
}
