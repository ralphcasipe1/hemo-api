import {
  postSubjectiveAssessment as postSubjectiveAssessmentController,
  user as userController
} from '../controllers';

export default function (app) {
  app
    .route('/patient_registries/:patientRegistryId/post_subjective_assessments')
    .get(
      userController.userRequired,
      postSubjectiveAssessmentController.listPostSubjectives
    )
    .post(
      userController.userRequired,
      postSubjectiveAssessmentController.setPostSubjectives
    )
    .delete(
      userController.userRequired,
      postSubjectiveAssessmentController.removePostSubjectives
    );
  app
    .route('/patient_registries/:patientRegistryId/post_subjective_assessments/:postSubjectiveAssessmentId')
    .get(
      userController.userRequired,
      postSubjectiveAssessmentController.selectPostSubjective
    )
    .patch(
      userController.userRequired,
      postSubjectiveAssessmentController.setPostSubjectives
    );
}
