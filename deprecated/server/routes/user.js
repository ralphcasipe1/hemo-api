import { user as userController } from '../controllers';

export default function (app) {
  app
    .route('/users')
    .get(userController.list)
    .post(userController.create);
  app
    .route('/users/:userId')
    .get(userController.select)
    .patch(userController.update)
    .delete(userController.destroy);
}
