import {
  user as userController,
  weight as weightController
} from '../controllers';

export default function (app) {
  app
    .route('/patient_registries/:patientRegistryId/weights')
    .get(userController.userRequired, weightController.get)
    .post(userController.userRequired, weightController.create);
  app
    .route('/patient_registries/:patientRegistryId/weights/:weightId')
    .get(userController.userRequired, weightController.select)
    .patch(userController.userRequired, weightController.update)
    .delete(userController.userRequired, weightController.destroy);
}
