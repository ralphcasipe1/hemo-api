import {
  patient as patientController,
  user as userController
} from '../controllers';

export default function (app) {
  app
    .route('/patients')
    .get(userController.userRequired, patientController.list)
    .post(userController.userRequired, patientController.create);
  app
    .route('/patients/:patientId')
    .get(userController.userRequired, patientController.select)
    .patch(userController.userRequired, patientController.update)
    .delete(userController.userRequired, patientController.destroy);
}
