import {
  user as userController,
  vascular as vascularController
} from '../controllers';

export default function (app) {
  app
    .route('/patient_registries/:patientRegistryId/vasculars')
    .get(userController.userRequired, vascularController.get)
    .post(userController.userRequired, vascularController.create);
  app
    .route('/patient_registries/:patientRegistryId/vasculars/:vascularId')
    .get(userController.userRequired, vascularController.select)
    .patch(userController.userRequired, vascularController.update)
    .delete(userController.userRequired, vascularController.destroy);
}
