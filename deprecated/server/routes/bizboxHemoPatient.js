import {
  user as userController,
  bizboxHemoPatient as bizboxHemoPatientController
} from '../controllers';

export default function (app) {
  app
    .route('/bizbox/patients/date/:date')
    .get(userController.userRequired, bizboxHemoPatientController.list);
  app
    .route('/bizbox/patients/:patientId')
    .get(userController.userRequired, bizboxHemoPatientController.select);
}
