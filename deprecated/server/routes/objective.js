import {
  objective as objectiveController,
  user as userController
} from '../controllers';

export default function (app) {
  app
    .route('/objectives')
    .get(userController.userRequired, objectiveController.list)
    .post(userController.userRequired, objectiveController.create);
  app
    .route('/objectives/:objectiveId')
    .get(userController.userRequired, objectiveController.select)
    .patch(userController.userRequired, objectiveController.update)
    .delete(userController.userRequired, objectiveController.destroy);
}
