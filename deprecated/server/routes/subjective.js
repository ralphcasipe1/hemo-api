import {
  subjective as subjectiveController,
  user as userController
} from '../controllers';

export default function (app) {
  app
    .route('/subjectives')
    .get(userController.userRequired, subjectiveController.list)
    .post(userController.userRequired, subjectiveController.create);
  app
    .route('/subjectives/:subjectiveId')
    .get(userController.userRequired, subjectiveController.select)
    .patch(userController.userRequired, subjectiveController.update)
    .delete(userController.userRequired, subjectiveController.destroy);
}
