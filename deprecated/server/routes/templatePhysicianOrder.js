import {
  templatePhysicianOrder as templatePhysicianOrderController,
  user as userController
} from '../controllers';

export default function (app) {
  app
    .route('/patients/:patientId/so_physician_orders')
    .get(userController.userRequired, templatePhysicianOrderController.get)
    .post(userController.userRequired, templatePhysicianOrderController.create);
  app
    .route('/patients/:patientId/so_physician_orders/:templatePhysicianOrderId')
    .get(userController.userRequired, templatePhysicianOrderController.select)
    .patch(userController.userRequired, templatePhysicianOrderController.update)
    .delete(
      userController.userRequired,
      templatePhysicianOrderController.destroy
    );
}
