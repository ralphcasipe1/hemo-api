import {
  dialyzerBrand as dialyzerBrandController,
  user as userController
} from '../controllers';

export default function (app) {
  app
    .route('/dialyzer_brands')
    .get(userController.userRequired, dialyzerBrandController.list)
    .post(userController.userRequired, dialyzerBrandController.create);
  app
    .route('/dialyzer_brands/:dialyzerBrandId')
    .get(userController.userRequired, dialyzerBrandController.select)
    .patch(userController.userRequired, dialyzerBrandController.update)
    .delete(userController.userRequired, dialyzerBrandController.destroy);
}
