import {
  user as userController,
  catheter as catheterController
} from '../controllers';

export default function (app) {
  app
    .route('/patient_registries/:patientRegistryId/catheters')
    .get(userController.userRequired, catheterController.get)
    .post(userController.userRequired, catheterController.create);
  app
    .route('/patient_registries/:patientRegistryId/catheters/:catheterId')
    .get(userController.userRequired, catheterController.select)
    .patch(userController.userRequired, catheterController.update)
    .delete(userController.userRequired, catheterController.destroy);
}
