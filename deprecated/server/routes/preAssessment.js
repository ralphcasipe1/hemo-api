import {
  preAssessment as preAssessmentController,
  user as userController
} from '../controllers';

export default function (app) {
  app
    .route('/patient_registries/:patientRegistryId/pre_assessments')
    .get(userController.userRequired, preAssessmentController.get)
    .post(userController.userRequired, preAssessmentController.create);
  app
    .route('/patient_registries/:patientRegistryId/pre_assessments/:preAssessmentId')
    .get(userController.userRequired, preAssessmentController.select)
    .patch(userController.userRequired, preAssessmentController.update)
    .delete(userController.userRequired, preAssessmentController.destroy);
}
