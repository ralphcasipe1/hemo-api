import {
  user as userController,
  bizboxPhysician as bizboxPhysicianController
} from '../controllers';

export default function (app) {
  app
    .route('/bizbox/physicians')
    .get(userController.userRequired, bizboxPhysicianController.list);
  app
    .route('/bizbox/physicians/:physicianId')
    .get(userController.userRequired, bizboxPhysicianController.select);
}
