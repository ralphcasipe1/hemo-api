import {
  user as userController,
  antiCoagulant as antiCoagulantController
} from '../controllers';

export default function (app) {
  app
    .route('/patient_registries/:patientRegistryId/anti_coagulants')
    .get(userController.userRequired, antiCoagulantController.get)
    .post(userController.userRequired, antiCoagulantController.create);
  app
    .route('/patient_registries/:patientRegistryId/anti_coagulants/:antiCoagulantId')
    .get(userController.userRequired, antiCoagulantController.select)
    .patch(userController.userRequired, antiCoagulantController.update)
    .delete(userController.userRequired, antiCoagulantController.destroy);
}
