import {
  user as userController,
  vitalSign as vitalSignController
} from '../controllers';

export default function (app) {
  app
    .route('/patient_registries/:patientRegistryId/vital_signs')
    .get(userController.userRequired, vitalSignController.findAll)
    .post(userController.userRequired, vitalSignController.create);
  app
    .route('/patient_registries/:patientRegistryId/vital_signs/:vitalSignId')
    .get(userController.userRequired, vitalSignController.findOne)
    .patch(userController.userRequired, vitalSignController.update)
    .delete(userController.userRequired, vitalSignController.destroy);
}
