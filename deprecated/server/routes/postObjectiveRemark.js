import {
  postObjectiveRemark as postObjectiveRemarkController,
  user as userController
} from '../controllers';

export default function (app) {
  app
    .route('/patient_registries/:patientRegistryId/post_objective_remarks')
    .get(userController.userRequired, postObjectiveRemarkController.get)
    .post(userController.userRequired, postObjectiveRemarkController.create);
  app
    .route('/patient_registries/:patientRegistryId/post_objective_remarks/:postObjectiveRemarkId')
    .delete(userController.userRequired, postObjectiveRemarkController.destroy);
}
