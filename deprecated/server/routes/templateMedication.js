import {
  templateMedication as templateMedicationController,
  user as userController
} from '../controllers';

export default function (app) {
  app
    .route('/patients/:patientId/so_medications')
    .get(userController.userRequired, templateMedicationController.list)
    .post(userController.userRequired, templateMedicationController.create);
  app
    .route('/patients/:patientId/so_medications/:templateMedicationId')
    .get(userController.userRequired, templateMedicationController.select)
    .patch(userController.userRequired, templateMedicationController.update)
    .delete(userController.userRequired, templateMedicationController.destroy);
}
