import {
  user as userController,
  vaccine as vaccineController
} from '../controllers';

export default function (app) {
  app
    .route('/patient_registries/:patientRegistryId/vaccines')
    .get(userController.userRequired, vaccineController.get)
    .post(userController.userRequired, vaccineController.create);
  app
    .route('/patient_registries/:patientRegistryId/vaccines/:vaccineId')
    .get(userController.userRequired, vaccineController.select)
    .patch(userController.userRequired, vaccineController.update)
    .delete(userController.userRequired, vaccineController.destroy);
}
