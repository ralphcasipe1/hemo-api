import {
  objectiveRemark as objectiveRemarkController,
  user as userController
} from '../controllers';

export default function (app) {
  app
    .route('/patient_registries/:patientRegistryId/objective_remarks')
    .get(userController.userRequired, objectiveRemarkController.get)
    .post(userController.userRequired, objectiveRemarkController.create);
  app
    .route('/patient_registries/:patientRegistryId/objective_remarks/:objectiveRemarkId')
    .delete(userController.userRequired, objectiveRemarkController.destroy);
}
