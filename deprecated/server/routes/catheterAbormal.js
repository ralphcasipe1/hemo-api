import {
  user as userController,
  catheterAbnormal as catheterAbnormalController
} from '../controllers';

export default function (app) {
  app
    .route('/catheters/:catheterId/abnormals')
    .get(
      userController.userRequired,
      catheterAbnormalController.listCatheterAbnormals
    )
    .post(
      userController.userRequired,
      catheterAbnormalController.setCatheterAbnormals
    );
  app
    .route('/catheters/:catheterId/abnormals/:abnormalId')
    .get(
      userController.userRequired,
      catheterAbnormalController.selectCatheterAbnormal
    )
    .patch(
      userController.userRequired,
      catheterAbnormalController.setCatheterAbnormals
    )
    .delete(
      userController.userRequired,
      catheterAbnormalController.removeCatheterAbnormal
    );
}
