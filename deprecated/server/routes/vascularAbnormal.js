import {
  user as userController,
  vascularAbnormal as vascularAbnormalController
} from '../controllers';

export default function (app) {
  app
    .route('/vasculars/:vascularId/abnormals')
    .get(
      userController.userRequired,
      vascularAbnormalController.listVascularAbnormals
    )
    .post(
      userController.userRequired,
      vascularAbnormalController.setVascularAbnormals
    );
  app
    .route('/vasculars/:vascularId/abnormals/:abnormalId')
    .get(
      userController.userRequired,
      vascularAbnormalController.selectVascularAbnormal
    )
    .patch(
      userController.userRequired,
      vascularAbnormalController.setVascularAbnormals
    )
    .delete(
      userController.userRequired,
      vascularAbnormalController.removeVascularAbnormal
    );
}
