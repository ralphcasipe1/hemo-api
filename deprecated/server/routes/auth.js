import { AuthenticationController } from '../controllers';

const authenticationController = new AuthenticationController();

export default function (app) {
  app.route('/authentication').post(authenticationController.authenticate);
}
