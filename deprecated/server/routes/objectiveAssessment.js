import {
  objectiveAssessment as objectiveAssessmentController,
  user as userController
} from '../controllers';

export default function (app) {
  app
    .route('/patient_registries/:patientRegistryId/objective_assessments')
    .get(
      userController.userRequired,
      objectiveAssessmentController.listObjectives
    )
    .post(
      userController.userRequired,
      objectiveAssessmentController.setObjectives
    )
    .delete(
      userController.userRequired,
      objectiveAssessmentController.removeObjectives
    );
  app
    .route('/patient_registries/:patientRegistryId/objective_assessments/:objectiveAssessmentId')
    .get(
      userController.userRequired,
      objectiveAssessmentController.selectObjective
    )
    .patch(
      userController.userRequired,
      objectiveAssessmentController.setObjectives
    );
}
