import {
  nurseNote as nurseNoteController,
  user as userController
} from '../controllers';

export default function (app) {
  app
    .route('/patient_registries/:patientRegistryId/nurse_notes')
    .get(userController.userRequired, nurseNoteController.list)
    .post(userController.userRequired, nurseNoteController.create);
  app
    .route('/patient_registries/:patientRegistryId/nurse_notes/:nurseNoteId')
    .get(userController.userRequired, nurseNoteController.select)
    .patch(userController.userRequired, nurseNoteController.update)
    .delete(userController.userRequired, nurseNoteController.destroy);
}
