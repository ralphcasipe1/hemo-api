import {
  physicianOrder as physicianOrderController,
  user as userController
} from '../controllers';

export default function (app) {
  app
    .route('/patient_registries/:patientRegistryId/physician_orders')
    .get(userController.userRequired, physicianOrderController.list)
    .post(userController.userRequired, physicianOrderController.create);
  app
    .route('/patient_registries/:patientRegistryId/physician_orders/:physicianOrderId')
    .get(userController.userRequired, physicianOrderController.select)
    .patch(userController.userRequired, physicianOrderController.update)
    .delete(userController.userRequired, physicianOrderController.destroy);
}
