import {
  user as userController,
  dialyzer as dialyzerController
} from '../controllers';

export default function (app) {
  app
    .route('/patient_registries/:patientRegistryId/dialyzers')
    .get(userController.userRequired, dialyzerController.get)
    .post(userController.userRequired, dialyzerController.create);
  app
    .route('/patient_registries/:patientRegistryId/dialyzers/:dialyzerId')
    .get(userController.userRequired, dialyzerController.select)
    .patch(userController.userRequired, dialyzerController.update)
    .delete(userController.userRequired, dialyzerController.destroy);
}
