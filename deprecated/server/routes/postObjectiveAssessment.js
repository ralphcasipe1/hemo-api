import {
  postObjectiveAssessment as postObjectiveAssessmentController,
  user as userController
} from '../controllers';

export default function (app) {
  app
    .route('/patient_registries/:patientRegistryId/post_objective_assessments')
    .get(
      userController.userRequired,
      postObjectiveAssessmentController.listPostObjectives
    )
    .post(
      userController.userRequired,
      postObjectiveAssessmentController.setPostObjectives
    )
    .delete(
      userController.userRequired,
      postObjectiveAssessmentController.removePostObjectives
    );
  app
    .route('/patient_registries/:patientRegistryId/post_objective_assessments/:postObjectiveAssessmentId')
    .get(
      userController.userRequired,
      postObjectiveAssessmentController.selectPostObjective
    )
    .patch(
      userController.userRequired,
      postObjectiveAssessmentController.setPostObjectives
    );
}
