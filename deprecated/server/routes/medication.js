import {
  user as userController,
  medication as medicationController
} from '../controllers';

export default function (app) {
  app
    .route('/patient_registries/:patientRegistryId/medications')
    .get(userController.userRequired, medicationController.findAll)
    .post(userController.userRequired, medicationController.create);
  app
    .route('/patient_registries/:patientRegistryId/medications/:medicationId')
    .get(userController.userRequired, medicationController.findOne)
    .patch(userController.userRequired, medicationController.update)
    .delete(userController.userRequired, medicationController.destroy);
}
