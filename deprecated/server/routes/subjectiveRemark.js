import {
  subjectiveRemark as subjectiveRemarkController,
  user as userController
} from '../controllers';

export default function (app) {
  app
    .route('/patient_registries/:patientRegistryId/subjective_remarks')
    .get(userController.userRequired, subjectiveRemarkController.get)
    .post(userController.userRequired, subjectiveRemarkController.create);
  app
    .route('/patient_registries/:patientRegistryId/subjective_remarks/:subjectiveRemarkId')
    .delete(userController.userRequired, subjectiveRemarkController.destroy);
}
