export default function (sequelize, DataTypes) {
  const TemplateWeightLog = sequelize.define(
    'TemplateWeightLog',
    {
      template_weight_log_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      description: DataTypes.TEXT,
    },
    {
      timestamps: true,
      tableName: 'template_weight_logs',
      underscored: true,
    }
  );
  TemplateWeightLog.associate = (models) => {
    TemplateWeightLog.belongsTo(models.TemplateWeight, {
      foreignKey: 'template_weight_id',
      as: 'templateWeight',
      onDelete: 'CASCADE',
    });
  };
  return TemplateWeightLog;
}
