export default function (sequelize, DataTypes) {
  const Weight = sequelize.define(
    'Weight',
    {
      weight_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      last_post_hd_wt: DataTypes.FLOAT,
      pre_hd_weight: DataTypes.FLOAT,
      inter_hd_wt_gain: DataTypes.FLOAT,
      post_hd_weight: DataTypes.FLOAT,
      net_fluid_removed: DataTypes.FLOAT,
      dry_weight: DataTypes.FLOAT,
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER,
      deleted_by: DataTypes.INTEGER,
    },
    {
      timestamps: true,
      paranoid: true,
      tableName: 'weights',
      underscored: true,
    }
  );
  Weight.associate = (models) => {
    Weight.belongsTo(models.PatientRegistry, {
      foreignKey: 'patient_registry_id',
      as: 'patient_registry',
      onDelete: 'CASCADE',
    });
  };
  return Weight;
}
