export default function (sequelize, DataTypes) {
  const Catheter = sequelize.define(
    'Catheter',
    {
      catheter_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      type: DataTypes.INTEGER,
      position: DataTypes.INTEGER,
      location: DataTypes.INTEGER,
      operation_date: DataTypes.DATE,
      physician: DataTypes.STRING,
      days_situ: DataTypes.STRING,
      is_red_port_inflow: DataTypes.BOOLEAN,
      is_red_port_outflow: DataTypes.BOOLEAN,
      is_blue_port_inflow: DataTypes.BOOLEAN,
      is_blue_port_outflow: DataTypes.BOOLEAN,
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER,
      deleted_by: DataTypes.INTEGER,
    },
    {
      timestamps: true,
      paranoid: true,
      tableName: 'catheters',
      underscored: true,
    }
  );
  Catheter.associate = (models) => {
    Catheter.belongsTo(models.PatientRegistry, {
      foreignKey: 'patient_registry_id',
      as: 'patient_registry',
      onDelete: 'CASCADE',
    });
    Catheter.belongsToMany(models.Abnormal, {
      through: models.CatheterAbnormal,
      foreignKey: 'catheter_id',
    });
  };
  return Catheter;
}
