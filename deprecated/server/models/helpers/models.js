import Abnormal from '../abnormal';
import Activity from '../activity';
import AntiCoagulant from '../anticoagulant';
import Catheter from '../catheter';
import CatheterAbnormal from '../catheterAbnormal';
import Dialyzer from '../dialyzer';
import DialyzerBrand from '../dialyzerBrand';
import DialyzerSize from '../dialyzerSize';
import Medication from '../medication';
import Mobility from '../mobility';
import NurseNote from '../nursenote';
import Objective from '../objective';
import ObjectiveAssessment from '../objectiveAssessment';
import ObjectiveRemark from '../objectiveRemark';
import Parameter from '../parameter';
import Patient from '../patient';
import PatientMeta from '../patientMeta';
import PatientRegistry from '../patientRegistry';
import Physician from '../physician';
import PhysicianOrder from '../physicianOrder';
import PostObjectiveAssessment from '../postObjectiveAssessment';
import PostObjectiveRemark from '../postObjectiveRemark';
import PostSubjectiveAssessment from '../postSubjectiveAssessment';
import PostSubjectiveRemark from '../postSubjectiveRemark';
import PreAssessment from '../preAssessment';
import Subjective from '../subjective';
import SubjectiveAssessment from '../subjectiveAssessment';
import SubjectiveRemark from '../subjectiveRemark';
import Task from '../task';
import TemplateAntiCoagulant from '../templateAntiCoagulant';
import TemplateAntiCoagulantLog from '../templateAntiCoagulantLog';
import TemplateDialyzer from '../templateDialyzer';
import TemplateDialyzerLog from '../templateDialyzerLog';
import TemplateMedication from '../templateMedication';
import TemplateMedicationLog from '../templateMedicationLog';
import TemplateParameter from '../templateParameter';
import TemplateParameterLog from '../templateParameterLog';
import TemplatePhysicianOrder from '../templatePhysicianOrder';
import TemplatePhysicianOrderLog from '../templatePhysicianOrderLog';
import TemplateWeight from '../templateWeight';
import TemplateWeightLog from '../templateWeightLog';
import Timline from '../timeline';
import User from '../user';
import Vaccine from '../vaccine';
import Vascular from '../vascular';
import VascularAbnormal from '../vascularAbnormal';
import VascularNormalAssessment from '../vascularNormalAssessment';
import VitalSign from '../vitalsign';
import Weight from '../weight';

const models = [
  {
    Abnormal,
    Activity,
    AntiCoagulant,
    Catheter,
    CatheterAbnormal,
    Dialyzer,
    DialyzerBrand,
    DialyzerSize,
    Medication,
    Mobility,
    NurseNote,
    Objective,
    ObjectiveAssessment,
    ObjectiveRemark,
    Parameter,
    Patient,
    PatientMeta,
    PatientRegistry,
    Physician,
    PhysicianOrder,
    PostObjectiveAssessment,
    PostObjectiveRemark,
    PostSubjectiveAssessment,
    PostSubjectiveRemark,
    PreAssessment,
    Subjective,
    SubjectiveAssessment,
    SubjectiveRemark,
    Task,
    TemplateAntiCoagulant,
    TemplateAntiCoagulantLog,
    TemplateDialyzer,
    TemplateDialyzerLog,
    TemplateMedication,
    TemplateMedicationLog,
    TemplateParameter,
    TemplateParameterLog,
    TemplatePhysicianOrder,
    TemplatePhysicianOrderLog,
    TemplateWeight,
    TemplateWeightLog,
    Timline,
    User,
    Vaccine,
    Vascular,
    VascularAbnormal,
    VascularNormalAssessment,
    VitalSign,
    Weight,
  },
];

export default models;
