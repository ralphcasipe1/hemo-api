export default function (sequelize, DataTypes) {
  const PostSubjectiveAssessment = sequelize.define(
    'PostSubjectiveAssessment',
    {
      patient_registry_id: DataTypes.INTEGER,
      subjective_id: DataTypes.INTEGER,
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER,
    },
    {
      timestamps: true,
      tableName: 'post_subjective_assessments',
      underscored: true,
    }
  );
  return PostSubjectiveAssessment;
}
