export default function (sequelize, DataTypes) {
  const Patient = sequelize.define(
    'Patient',
    {
      patient_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      bizbox_patient_no: DataTypes.BIGINT,
      fname: DataTypes.STRING,
      mname: DataTypes.STRING,
      lname: DataTypes.STRING,
      age: DataTypes.INTEGER,
      sex: DataTypes.INTEGER,
      civil_status: DataTypes.INTEGER,
      room_no: DataTypes.STRING,
      hospital_no: DataTypes.STRING,
      attending_physician: DataTypes.INTEGER,
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER,
      deleted_by: DataTypes.INTEGER,
    },
    {
      paranoid: true,
      tableName: 'patients',
    }
  );
  Patient.associate = (models) => {
    Patient.hasMany(models.PatientRegistry, {
      foreignKey: 'patient_id',
      as: 'patientRegistries',
    });
    Patient.hasMany(models.TemplateAntiCoagulant, {
      foreignKey: 'patient_id',
      as: 'sOAntiCoagulants',
    });
    Patient.hasMany(models.TemplateDialyzer, {
      foreignKey: 'patient_id',
      as: 'sODialyzers',
    });
    Patient.hasMany(models.TemplateMedication, {
      foreignKey: 'patient_id',
      as: 'sOMedications',
    });
    Patient.hasMany(models.TemplateParameter, {
      foreignKey: 'patient_id',
      as: 'sOParameters',
    });
    Patient.hasMany(models.TemplatePhysicianOrder, {
      foreignKey: 'patient_id',
      as: 'sOPhysicianOrders',
    });
    Patient.hasMany(models.TemplateWeight, {
      foreignKey: 'patient_id',
      as: 'sOWeights',
    });
  };
  return Patient;
}
