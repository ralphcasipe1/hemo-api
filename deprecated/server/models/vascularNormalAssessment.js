export default function (sequelize, DataTypes) {
  const VascularNormalAssessment = sequelize.define(
    'VascularNormalAssessment',
    {
      vascular_normal_assessment_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      normal_assess: DataTypes.INTEGER,
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER,
      deleted_by: DataTypes.INTEGER,
    },
    {
      timestamps: true,
      paranoid: true,
      tableName: 'vascular_normal_assessments',
      underscored: true,
    }
  );
  VascularNormalAssessment.associate = (models) => {
    VascularNormalAssessment.belongsTo(models.Vascular, {
      foreignKey: 'vascular_id',
      as: 'vascular',
      onDelete: 'CASCADE',
    });
  };
  return VascularNormalAssessment;
}
