export default function (sequelize, DataTypes) {
  const Timeline = sequelize.define(
    'Timeline',
    {
      timeline_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      description: {
        type: DataTypes.TEXT,
      },
      timeline_status: {
        type: DataTypes.ENUM(
          'pending',
          'added',
          'updated',
          'deleted',
          'completed'
        ),
        defaultValue: 'added',
      },
    },
    {
      timestamps: true,
      tableName: 'timelines',
      underscored: true,
    }
  );
  Timeline.associate = (models) => {
    Timeline.belongsTo(models.User, {
      foreignKey: 'user_id',
      as: 'user',
      onDelete: 'CASCADE',
    });
    Timeline.belongsTo(models.Activity, {
      foreignKey: 'activity_id',
      as: 'activity',
      onDelete: 'CASCADE',
    });
  };
  return Timeline;
}
