export default function (sequelize, DataTypes) {
  const Activity = sequelize.define(
    'Activity',
    {
      activity_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      name: DataTypes.STRING,
    },
    {
      timestamps: true,
      tableName: 'activities',
      underscored: true,
    }
  );
  Activity.associate = (models) => {
    Activity.hasMany(models.Timeline, {
      foreignKey: 'activity_id',
      as: 'timelines',
    });
  };
  return Activity;
}
