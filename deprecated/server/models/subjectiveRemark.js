export default function (sequelize, DataTypes) {
  const SubjectiveRemark = sequelize.define(
    'SubjectiveRemark',
    {
      subjective_remark_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      remark: DataTypes.TEXT,
    },
    {
      timestamps: true,
      tableName: 'subjective_remarks',
      underscored: true,
    }
  );
  SubjectiveRemark.associate = (models) => {
    SubjectiveRemark.belongsTo(models.PatientRegistry, {
      foreignKey: 'patient_registry_id',
      as: 'patient_registry',
      onDelete: 'CASCADE',
    });
  };
  return SubjectiveRemark;
}
