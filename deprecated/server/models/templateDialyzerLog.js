export default function (sequelize, DataTypes) {
  const TemplateDialyzerLog = sequelize.define(
    'TemplateDialyzerLog',
    {
      template_dialyzer_log_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      description: DataTypes.TEXT,
    },
    {
      timestamps: true,
      tableName: 'template_dialyzer_logs',
      underscored: true,
    }
  );
  TemplateDialyzerLog.associate = (models) => {
    TemplateDialyzerLog.belongsTo(models.TemplateDialyzer, {
      foreignKey: 'template_dialyzer_id',
      as: 'templateDialyzer',
      onDelete: 'CASCADE',
    });
  };
  return TemplateDialyzerLog;
}
