export default function (sequelize, DataTypes) {
  const PostObjectiveRemark = sequelize.define(
    'PostObjectiveRemark',
    {
      post_objective_remark_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      remark: DataTypes.TEXT,
    },
    {
      timestamps: true,
      tableName: 'post_objective_remarks',
      underscored: true,
    }
  );
  PostObjectiveRemark.associate = (models) => {
    PostObjectiveRemark.belongsTo(models.PatientRegistry, {
      foreignKey: 'patient_registry_id',
      as: 'patient_registry',
      onDelete: 'CASCADE',
    });
  };
  return PostObjectiveRemark;
}
