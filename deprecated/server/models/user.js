import _ from 'lodash';
import moment from 'moment';

export default function (sequelize, DataTypes) {
  const User = sequelize.define(
    'User',
    {
      user_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      fname: {
        allowNull: false,
        type: DataTypes.STRING(100),
        get() {
          return _.upperFirst(this.getDataValue('fname'));
        },
        set(val) {
          return this.setDataValue('fname', _.toLower(val));
        },
      },
      mname: {
        type: DataTypes.STRING(100),
        get() {
          return _.upperFirst(this.getDataValue('mname'));
        },
        set(val) {
          return this.setDataValue('mname', _.toLower(val));
        },
      },
      lname: {
        allowNull: false,
        type: DataTypes.STRING(100),
        get() {
          return _.upperFirst(this.getDataValue('lname'));
        },
        set(val) {
          return this.setDataValue('lname', _.toLower(val));
        },
      },
      id_number: {
        allowNull: false,
        type: DataTypes.STRING(20),
      },
      password: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      is_admin: DataTypes.BOOLEAN,
      is_doctor: DataTypes.BOOLEAN,
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER,
      deleted_by: DataTypes.INTEGER,
      created_at: {
        type: DataTypes.DATE,
        get() {
          return moment(this.getDataValue('created_at')).format('YYYY-MM-DD');
        },
      },
      updated_at: {
        type: DataTypes.DATE,
        get() {
          return moment(this.getDataValue('updated_at')).format('YYYY-MM-DD');
        },
      },
    },
    {
      tableName: 'users',
      paranoid: true,
    }
  );
  User.associate = (models) => {
    User.hasMany(models.Timeline, {
      foreignKey: 'user_id',
      as: 'timelines',
    });
    User.hasMany(models.Task, {
      foreignKey: 'user_id',
      as: 'tasks',
    });
    User.hasMany(models.NurseNote, {
      foreignKey: 'updated_by',
    });
    User.hasMany(models.PhysicianOrder, {
      foreignKey: 'created_by',
    });
    User.hasMany(models.Medication, {
      foreignKey: 'created_by',
    });
    User.hasMany(models.VitalSign, {
      foreignKey: 'created_by',
    });
  };
  return User;
}
