export default function (sequelize, DataTypes) {
  const PatientRegistry = sequelize.define(
    'PatientRegistry',
    {
      patient_registry_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      registry_no: DataTypes.STRING,
      type: DataTypes.INTEGER,
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER,
      deleted_by: DataTypes.INTEGER,
    },
    {
      timestamps: true,
      paranoid: true,
      tableName: 'patient_registries',
      underscored: true,
    }
  );
  PatientRegistry.associate = (models) => {
    PatientRegistry.belongsTo(models.Patient, {
      foreignKey: 'patient_id',
      as: 'patient',
      onDelete: 'CASCADE',
    });
    PatientRegistry.hasOne(models.PatientMeta, {
      foreignKey: 'patient_registry_id',
      as: 'patientMeta',
    });
    PatientRegistry.hasOne(models.AntiCoagulant, {
      foreignKey: 'patient_registry_id',
      as: 'antiCoagulant',
    });
    PatientRegistry.hasOne(models.Dialyzer, {
      foreignKey: 'patient_registry_id',
      as: 'dialyzer',
    });
    PatientRegistry.hasOne(models.Parameter, {
      foreignKey: 'patient_registry_id',
      as: 'parameter',
    });
    PatientRegistry.hasOne(models.Vaccine, {
      foreignKey: 'patient_registry_id',
      as: 'vaccine',
    });
    PatientRegistry.hasOne(models.Weight, {
      foreignKey: 'patient_registry_id',
      as: 'weight',
    });
    PatientRegistry.hasOne(models.PreAssessment, {
      foreignKey: 'patient_registry_id',
      as: 'preAssessment',
    });
    PatientRegistry.hasMany(models.NurseNote, {
      foreignKey: 'patient_registry_id',
      as: 'nurseNotes',
    });
    PatientRegistry.hasMany(models.Medication, {
      foreignKey: 'patient_registry_id',
      as: 'medications',
    });
    PatientRegistry.belongsToMany(models.Objective, {
      through: models.ObjectiveAssessment,
      foreignKey: 'patient_registry_id',
      as: 'objectives',
    });
    PatientRegistry.hasOne(models.ObjectiveRemark, {
      foreignKey: 'patient_registry_id',
      as: 'objectiveRemark',
    });
    PatientRegistry.belongsToMany(models.Objective, {
      through: models.PostObjectiveAssessment,
      foreignKey: 'patient_registry_id',
      as: 'postObjectives',
    });
    PatientRegistry.hasOne(models.PostObjectiveRemark, {
      foreignKey: 'patient_registry_id',
      as: 'postObjectiveRemark',
    });
    PatientRegistry.belongsToMany(models.Subjective, {
      through: models.SubjectiveAssessment,
      foreignKey: 'patient_registry_id',
      as: 'subjectives',
    });
    PatientRegistry.hasOne(models.SubjectiveRemark, {
      foreignKey: 'patient_registry_id',
      as: 'subjectiveRemark',
    });
    PatientRegistry.belongsToMany(models.Subjective, {
      through: models.PostSubjectiveAssessment,
      foreignKey: 'patient_registry_id',
      as: 'postSubjectives',
    });
    PatientRegistry.hasOne(models.PostSubjectiveRemark, {
      foreignKey: 'patient_registry_id',
      as: 'postSubjectiveRemark',
    });
    PatientRegistry.hasOne(models.Vascular, {
      foreignKey: 'patient_registry_id',
      as: 'vascular',
    });
    PatientRegistry.hasOne(models.Catheter, {
      foreignKey: 'patient_registry_id',
      as: 'catheter',
    });
    PatientRegistry.hasMany(models.VitalSign, {
      foreignKey: 'patient_registry_id',
      as: 'vital_signs',
    });
    PatientRegistry.hasMany(models.PhysicianOrder, {
      foreignKey: 'patient_registry_id',
      as: 'physicianOrders',
    });
  };
  return PatientRegistry;
}
