export default function (sequelize, DataTypes) {
  const TemplateMedicationLog = sequelize.define(
    'TemplateMedicationLog',
    {
      template_medication_log_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      description: DataTypes.TEXT,
    },
    {
      timestamps: true,
      tableName: 'template_medication_logs',
      underscored: true,
    }
  );
  TemplateMedicationLog.associate = (models) => {
    TemplateMedicationLog.belongsTo(models.TemplateMedication, {
      foreignKey: 'template_medication_id',
      as: 'template_medication',
      onDelete: 'CASCADE',
    });
  };
  return TemplateMedicationLog;
}
