export default function (sequelize, DataTypes) {
  const Physician = sequelize.define(
    'Physician',
    {
      physician_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      bizbox_physician_no: DataTypes.BIGINT,
      fname: {
        type: DataTypes.STRING,
      },
      lname: {
        type: DataTypes.STRING,
      },
      date_of_birth: {
        type: DataTypes.DATE,
      },
      specialization: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER,
      deleted_by: DataTypes.INTEGER,
    },
    {
      timestamps: true,
      paranoid: true,
      tableName: 'physicians',
      underscored: true,
    }
  );
  Physician.associate = (models) => {
    Physician.hasMany(models.PhysicianOrder, {
      foreignKey: 'physician_id',
      as: 'physicianOrders',
    });
  };
  return Physician;
}
