export default function (sequelize, DataTypes) {
  const Parameter = sequelize.define(
    'Parameter',
    {
      parameter_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      duration: DataTypes.INTEGER,
      blood_flow_rate: DataTypes.FLOAT,
      dialysate_bath: DataTypes.STRING,
      dialysate_additive: DataTypes.FLOAT,
      dialysate_flow_rate: DataTypes.FLOAT,
      ufv_goal: DataTypes.FLOAT,
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER,
      deleted_by: DataTypes.INTEGER,
    },
    {
      timestamps: true,
      paranoid: true,
      tableName: 'parameters',
      underscored: true,
    }
  );
  Parameter.associate = (models) => {
    Parameter.belongsTo(models.PatientRegistry, {
      foreignKey: 'patient_registry_id',
      as: 'patient_registry',
      onDelete: 'CASCADE',
    });
  };
  return Parameter;
}
