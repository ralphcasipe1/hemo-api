export default function (sequelize, DataTypes) {
  const TemplatePhysicianOrderLog = sequelize.define(
    'TemplatePhysicianOrderLog',
    {
      template_physician_order_log_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      description: DataTypes.TEXT,
    },
    {
      timestamps: true,
      tableName: 'template_physician_order_logs',
      underscored: true,
    }
  );
  TemplatePhysicianOrderLog.associate = (models) => {
    TemplatePhysicianOrderLog.belongsTo(models.TemplatePhysicianOrder, {
      foreignKey: 'template_physician_order_id',
      as: 'templatePhysicianOrder',
      onDelete: 'CASCADE',
    });
  };
  return TemplatePhysicianOrderLog;
}
