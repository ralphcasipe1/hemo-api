export default function (sequelize, DataTypes) {
  const ObjectiveRemark = sequelize.define(
    'ObjectiveRemark',
    {
      objective_remark_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      remark: DataTypes.TEXT,
    },
    {
      timestamps: true,
      tableName: 'objective_remarks',
      underscored: true,
    }
  );
  ObjectiveRemark.associate = (models) => {
    ObjectiveRemark.belongsTo(models.PatientRegistry, {
      foreignKey: 'patient_registry_id',
      as: 'patient_registry',
      onDelete: 'CASCADE',
    });
  };
  return ObjectiveRemark;
}
