export default function (sequelize, DataTypes) {
  const PostObjectiveAssessment = sequelize.define(
    'PostObjectiveAssessment',
    {
      patient_registry_id: DataTypes.INTEGER,
      objective_id: DataTypes.INTEGER,
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER,
    },
    {
      timestamps: true,
      tableName: 'post_objective_assessments',
      underscored: true,
    }
  );
  return PostObjectiveAssessment;
}
