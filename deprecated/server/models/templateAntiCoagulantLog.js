export default function (sequelize, DataTypes) {
  const TemplateAntiCoagulantLog = sequelize.define(
    'TemplateAntiCoagulantLog',
    {
      template_anti_coagulant_log_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      description: DataTypes.TEXT,
    },
    {
      timestamps: true,
      tableName: 'template_anti_coagulant_logs',
      underscored: true,
    }
  );
  TemplateAntiCoagulantLog.associate = (models) => {
    TemplateAntiCoagulantLog.belongsTo(models.TemplateAntiCoagulant, {
      foreignKey: 'template_anti_coagulant_id',
      as: 'templateAntiCoagulant',
      onDelete: 'CASCADE',
    });
  };
  return TemplateAntiCoagulantLog;
}
