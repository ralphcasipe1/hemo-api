export default function (sequelize, DataTypes) {
  const Mobility = sequelize.define(
    'Mobility',
    {
      mobility_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      name: DataTypes.STRING,
      description: DataTypes.STRING,
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER,
      deleted_by: DataTypes.INTEGER,
    },
    {
      timestamps: true,
      paranoid: true,
      tableName: 'mobilities',
      underscored: true,
    }
  );
  Mobility.associate = (models) => {
    Mobility.hasMany(models.PreAssessment, {
      foreignKey: 'mobility_id',
      as: 'preAssessments',
    });
  };
  return Mobility;
}
