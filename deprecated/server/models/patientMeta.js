export default function (sequelize, DataTypes) {
  const PatientMeta = sequelize.define(
    'PatientMeta',
    {
      machine_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      tx_date: DataTypes.DATEONLY,
      rn_on_duty: DataTypes.INTEGER,
      machine_no: DataTypes.STRING,
      initiation: DataTypes.INTEGER,
      mrod: DataTypes.STRING,
      termination: DataTypes.INTEGER,
      allergy: DataTypes.TEXT,
      technician: DataTypes.INTEGER,
      blood_type: DataTypes.STRING,
      date_taken: DataTypes.DATEONLY,
    },
    {
      timestamps: true,
      tableName: 'patient_metas',
      underscored: true,
    }
  );
  PatientMeta.associate = (models) => {
    PatientMeta.belongsTo(models.PatientRegistry, {
      foreignKey: 'patient_registry_id',
      as: 'patient_registry',
      onDelete: 'CASCADE',
    });
  };
  return PatientMeta;
}
