import fs from 'fs';
import path from 'path';
import Sequelize from 'sequelize';

const basename = path.basename(__filename);

const db = {};
let sequelize;

const defineOption = {
  underscored: true,
  timestamps: true,
  charset: 'utf8',
};

const config = (database, host) => (
  {
    database,
    host,
    username: 'postgres',
    password: 'test098!?',
    port: 5432,
    dialect: 'postgres',
    define: defineOption,
  }
);

// const sequelize = new Sequelize(config('hemo_development', '127.0.0.1'));

if (process.env.NODE_ENV === 'development') {
  sequelize = new Sequelize(config('hemo_development', '127.0.0.1'));
} else {
  sequelize = new Sequelize(config('hemo_production', '127.0.0.1'));
}

fs
  .readdirSync(__dirname)
  .filter((file) =>
    file.indexOf('.') !== 0
    && file !== basename
    && file.slice(-3) === '.js')
  .map((file) => {
    const model = sequelize.import(path.resolve(__dirname, file));
    db[model.name] = model;
    return db;
  });

Object.keys(db).map((modelName) => {
  if (db[modelName].associate) {
    return db[modelName].associate(db);
  }
  return {};
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

export default db;
