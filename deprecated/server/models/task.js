export default function (sequelize, DataTypes) {
  const Task = sequelize.define(
    'Task',
    {
      task_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      task_desc: DataTypes.TEXT,
      task_status: {
        type: DataTypes.ENUM('pending', 'completed'),
        defaultValue: 'pending',
      },
    },
    {
      timestamps: true,
      tableName: 'tasks',
      underscored: true,
    }
  );
  Task.associate = (models) => {
    Task.belongsTo(models.User, {
      foreignKey: 'user_id',
      as: 'user',
      onDelete: 'CASCADE',
    });
  };
  return Task;
}
