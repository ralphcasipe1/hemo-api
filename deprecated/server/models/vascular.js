export default function (sequelize, DataTypes) {
  const Vascular = sequelize.define(
    'Vascular',
    {
      vascular_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      type: DataTypes.INTEGER,
      operation_date: DataTypes.DATE,
      surgeon: DataTypes.STRING(50),
      location: DataTypes.INTEGER,
      arterial_location: DataTypes.INTEGER,
      arterial_needle: DataTypes.INTEGER,
      venous_needle: DataTypes.INTEGER,
      is_bruit: {
        type: DataTypes.BOOLEAN,
      },
      is_thrill: {
        type: DataTypes.BOOLEAN,
      },
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER,
      deleted_by: DataTypes.INTEGER,
    },
    {
      timestamps: true,
      paranoid: true,
      tableName: 'vasculars',
      underscored: true,
    }
  );
  Vascular.associate = (models) => {
    Vascular.hasMany(models.VascularNormalAssessment, {
      foreignKey: 'vascular_id',
      as: 'normalAssessments',
    });
    Vascular.belongsTo(models.PatientRegistry, {
      foreignKey: 'patient_registry_id',
      as: 'patient_registry',
      onDelete: 'CASCADE',
    });
    Vascular.belongsToMany(models.Abnormal, {
      through: models.VascularAbnormal,
      foreignKey: 'vascular_id',
    });
  };
  return Vascular;
}
