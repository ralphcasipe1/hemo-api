export default function (sequelize, DataTypes) {
  const TemplateDialyzer = sequelize.define(
    'TemplateDialyzer',
    {
      template_dialyzer_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      dialyzer_brand_id: DataTypes.INTEGER,
      type: DataTypes.INTEGER,
      dialyzer_date: DataTypes.DATE,
      count: DataTypes.INTEGER,
      d_t: DataTypes.DATEONLY,
      dialyzer_size_id: DataTypes.INTEGER,
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER,
      deleted_by: DataTypes.INTEGER,
    },
    {
      timestamps: true,
      paranoid: true,
      tableName: 'template_dialyzers',
      underscored: true,
    }
  );
  TemplateDialyzer.associate = (models) => {
    TemplateDialyzer.hasMany(models.TemplateDialyzerLog, {
      foreignKey: 'template_dialyzer_id',
      as: 'templateDialyzerLogs',
    });
    TemplateDialyzer.belongsTo(models.Patient, {
      foreignKey: 'patient_id',
      as: 'patient',
      onDelete: 'CASCADE',
    });
    TemplateDialyzer.belongsTo(models.DialyzerBrand, {
      foreignKey: 'dialyzer_brand_id',
      as: 'dialyzerBrand',
      onDelete: 'CASCADE',
    });
    TemplateDialyzer.belongsTo(models.DialyzerSize, {
      foreignKey: 'dialyzer_size_id',
      as: 'dialyzerSize',
      onDelete: 'CASCADE',
    });
  };
  return TemplateDialyzer;
}
