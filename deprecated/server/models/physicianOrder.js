export default function (sequelize, DataTypes) {
  const PhysicianOrder = sequelize.define(
    'PhysicianOrder',
    {
      physician_order_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      assessment: DataTypes.TEXT,
      medication: DataTypes.TEXT,
      procedure: DataTypes.TEXT,
      diagnostic_test: DataTypes.TEXT,
      other_remark: DataTypes.TEXT,
      physician_order_status: {
        type: DataTypes.ENUM('pending', 'approved', 'completed'),
        defaultValue: 'pending',
      },
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER,
      deleted_by: DataTypes.INTEGER,
    },
    {
      timestamps: true,
      paranoid: true,
      tableName: 'physician_orders',
      underscored: true,
    }
  );
  PhysicianOrder.associate = (models) => {
    PhysicianOrder.belongsTo(models.PatientRegistry, {
      foreignKey: 'patient_registry_id',
      as: 'patient_registry',
      onDelete: 'CASCADE',
    });
    PhysicianOrder.belongsTo(models.Physician, {
      foreignKey: 'physician_id',
      as: 'physician',
      onDelete: 'CASCADE',
    });
    PhysicianOrder.belongsTo(models.User, {
      foreignKey: 'created_by',
      as: 'createdBy',
    });
  };
  return PhysicianOrder;
}
