export default function (sequelize, DataTypes) {
  const TemplateWeight = sequelize.define(
    'TemplateWeight',
    {
      template_weight_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      dry_weight: DataTypes.FLOAT,
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER,
      deleted_by: DataTypes.INTEGER,
    },
    {
      timestamps: true,
      paranoid: true,
      tableName: 'template_weights',
      underscored: true,
    }
  );
  TemplateWeight.associate = (models) => {
    TemplateWeight.hasMany(models.TemplateWeightLog, {
      foreignKey: 'template_weight_id',
      as: 'templateWeightLogs',
    });
    TemplateWeight.belongsTo(models.Patient, {
      foreignKey: 'patient_id',
      as: 'patient',
      onDelete: 'CASCADE',
    });
  };
  return TemplateWeight;
}
