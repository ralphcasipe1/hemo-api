export default function (sequelize, DataTypes) {
  const PostSubjectiveRemark = sequelize.define(
    'PostSubjectiveRemark',
    {
      post_subjective_remark_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      remark: DataTypes.TEXT,
    },
    {
      timestamps: true,
      tableName: 'post_subjective_remarks',
      underscored: true,
    }
  );
  PostSubjectiveRemark.associate = (models) => {
    PostSubjectiveRemark.belongsTo(models.PatientRegistry, {
      foreignKey: 'patient_registry_id',
      as: 'patient_registry',
      onDelete: 'CASCADE',
    });
  };
  return PostSubjectiveRemark;
}
