export default function (sequelize, DataTypes) {
  const Objective = sequelize.define(
    'Objective',
    {
      objective_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      name: DataTypes.STRING,
      description: DataTypes.STRING,
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER,
      deleted_by: DataTypes.INTEGER,
    },
    {
      timestamps: true,
      paranoid: true,
      tableName: 'objectives',
      underscored: true,
    }
  );
  Objective.associate = (models) => {
    Objective.belongsToMany(models.PatientRegistry, {
      // through: 'objective_assessments',
      through: models.ObjectiveAssessment,
      foreignKey: 'objective_id',
      as: 'patientRegistries',
    });
    Objective.belongsToMany(models.PatientRegistry, {
      // through: 'post_objective_assessments',
      through: models.PostObjectiveAssessment,
      foreignKey: 'objective_id',
      as: 'postPatientRegistries',
    });
  };
  return Objective;
}
