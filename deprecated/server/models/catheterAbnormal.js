export default function (sequelize, DataTypes) {
  const CatheterAbnormal = sequelize.define(
    'CatheterAbnormal',
    {
      catheter_id: DataTypes.INTEGER,
      abnormal_id: DataTypes.INTEGER,
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER,
      deleted_by: DataTypes.INTEGER,
    },
    {
      timestamps: true,
      paranoid: true,
      tableName: 'catheters_abnormals',
      underscored: true,
    }
  );
  return CatheterAbnormal;
}
