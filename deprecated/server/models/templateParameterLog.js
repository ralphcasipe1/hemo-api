export default function (sequelize, DataTypes) {
  const TemplateParameterLog = sequelize.define(
    'TemplateParameterLog',
    {
      template_parameter_log_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      description: DataTypes.TEXT,
    },
    {
      timestamps: true,
      tableName: 'template_parameter_logs',
      underscored: true,
    }
  );
  TemplateParameterLog.associate = (models) => {
    TemplateParameterLog.belongsTo(models.TemplateParameter, {
      foreignKey: 'template_parameter_id',
      as: 'templateParameter',
      onDelete: 'CASCADE',
    });
  };
  return TemplateParameterLog;
}
