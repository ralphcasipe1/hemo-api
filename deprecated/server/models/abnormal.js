export default function (sequelize, DataTypes) {
  const Abnormal = sequelize.define(
    'Abnormal',
    {
      abnormal_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      name: {
        type: DataTypes.STRING,
        unique: true,
      },
      description: DataTypes.STRING,
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER,
      deleted_by: DataTypes.INTEGER,
    },
    {
      timestamps: true,
      paranoid: true,
      tableName: 'abnormals',
      underscored: true,
    }
  );
  Abnormal.associate = (models) => {
    Abnormal.belongsToMany(models.Vascular, {
      through: models.VascularAbnormal,
      foreignKey: 'abnormal_id',
    });
    Abnormal.belongsToMany(models.Catheter, {
      through: models.CatheterAbnormal,
      foreignKey: 'abnormal_id',
    });
  };
  return Abnormal;
}
