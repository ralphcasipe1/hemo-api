export default function (sequelize, DataTypes) {
  const Medication = sequelize.define(
    'Medication',
    {
      medication_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      generic: DataTypes.STRING(25),
      brand: DataTypes.STRING(25),
      preparation: DataTypes.TEXT,
      dosage: DataTypes.STRING(20),
      route: DataTypes.STRING(20),
      created_by: DataTypes.INTEGER,
      updated_by: DataTypes.INTEGER,
      deleted_by: DataTypes.INTEGER,
    },
    {
      timestamps: true,
      paranoid: true,
      tableName: 'medications',
      underscored: true,
    }
  );
  Medication.associate = (models) => {
    Medication.belongsTo(models.PatientRegistry, {
      foreignKey: 'patient_registry_id',
      as: 'patient_registry',
      onDelete: 'CASCADE',
    });
    Medication.belongsTo(models.User, {
      foreignKey: 'created_by',
      as: 'createdBy',
    });
  };
  return Medication;
}
