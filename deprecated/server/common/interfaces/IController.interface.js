// @flow

import { IUserRequest } from './IUserRequest.interface';

export interface IController<T> {
  findAll(request, response): Array<T>;
  create(request, response): T;
  findOne(request, response): T;
  update(request, response): T;
  delete(request, response): T;
}
