// @flow

import { IController } from '../interfaces/IController.interface';

export class BaseController implements IController {
  data = [];

  constructor(data?: Array<mixed>) {
    this.data = data;
  }

  findAll() {
    return null;
  }

  create() {
    return null
  }

  findOne() {
    return null
  }
}
