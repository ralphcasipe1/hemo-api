export function up(queryInterface, Sequelize) {
  return queryInterface.createTable('patients', {
    patient_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    bizbox_patient_no: {
      allowNull: false,
      type: Sequelize.BIGINT,
      unique: true,
    },
    fname: {
      allowNull: false,
      type: Sequelize.STRING(100),
    },
    mname: {
      type: Sequelize.STRING(100),
    },
    lname: {
      allowNull: false,
      type: Sequelize.STRING(100),
    },
    age: {
      type: Sequelize.INTEGER,
    },
    sex: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    civil_status: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    room_no: {
      type: Sequelize.STRING,
    },
    hospital_no: {
      type: Sequelize.STRING,
    },
    attending_physician: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    created_by: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    updated_by: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    deleted_by: {
      type: Sequelize.INTEGER,
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    deleted_at: {
      type: Sequelize.DATE,
    },
  });
}
export function down(queryInterface) {
  return queryInterface.dropTable('patients');
}
