export function up(queryInterface, Sequelize) {
  return queryInterface.createTable('template_weights', {
    template_weight_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    patient_id: {
      allowNull: false,
      onDelete: 'CASCADE',
      references: {
        model: 'patients',
        key: 'patient_id',
      },
      type: Sequelize.INTEGER,
    },
    dry_weight: {
      allowNull: false,
      type: Sequelize.FLOAT,
    },
    created_by: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    updated_by: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    deleted_by: {
      type: Sequelize.INTEGER,
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    deleted_at: {
      type: Sequelize.DATE,
    },
  });
}
export function down(queryInterface) {
  return queryInterface.dropTable('template_weights');
}
