module.exports = {
  up(queryInterface, Sequelize) {
    return queryInterface.createTable('patient_metas', {
      machine_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      patient_registry_id: {
        allowNull: false,
        onDelete: 'CASCADE',
        references: {
          model: 'patient_registries',
          key: 'patient_registry_id',
        },
        type: Sequelize.INTEGER,
      },
      tx_date: {
        type: Sequelize.DATEONLY,
        allowNull: false,
      },
      rn_on_duty: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      machine_no: {
        type: Sequelize.STRING(50),
        allowNull: false,
      },
      initiation: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      mrod: {
        type: Sequelize.STRING,
      },
      termination: {
        type: Sequelize.INTEGER,
      },
      allergy: {
        type: Sequelize.TEXT,
      },
      technician: {
        type: Sequelize.INTEGER,
      },
      blood_type: {
        type: Sequelize.STRING,
      },
      date_taken: {
        type: Sequelize.DATEONLY,
        allowNull: false,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down(queryInterface) {
    return queryInterface.dropTable('patient_metas');
  },
};
