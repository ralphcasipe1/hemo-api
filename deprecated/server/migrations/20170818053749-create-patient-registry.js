export function up(queryInterface, Sequelize) {
  return queryInterface.createTable('patient_registries', {
    patient_registry_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    patient_id: {
      allowNull: false,
      onDelete: 'CASCADE',
      references: {
        model: 'patients',
        key: 'patient_id',
      },
      type: Sequelize.INTEGER,
    },
    registry_no: {
      allowNull: false,
      unique: true,
      type: Sequelize.STRING(25),
    },
    type: {
      allowNull: false,
      defaultValue: 1,
      type: Sequelize.INTEGER,
    },
    created_by: {
      type: Sequelize.INTEGER,
    },
    updated_by: {
      type: Sequelize.INTEGER,
    },
    deleted_by: {
      type: Sequelize.INTEGER,
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    deleted_at: {
      type: Sequelize.DATE,
    },
  });
}
export function down(queryInterface) {
  return queryInterface.dropTable('patient_registries');
}
