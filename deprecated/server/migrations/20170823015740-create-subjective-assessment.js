export function up(queryInterface, Sequelize) {
  return queryInterface.createTable('subjective_assessments', {
    patient_registry_id: {
      allowNull: false,
      references: {
        model: 'patient_registries',
        key: 'patient_registry_id',
      },
      type: Sequelize.INTEGER,
    },
    subjective_id: {
      allowNull: false,
      references: {
        model: 'subjectives',
        key: 'subjective_id',
      },
      type: Sequelize.INTEGER,
    },
    created_by: {
      type: Sequelize.INTEGER,
    },
    updated_by: {
      type: Sequelize.INTEGER,
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
  });
}
export function down(queryInterface) {
  return queryInterface.dropTable('subjective_assessments');
}
