export function up(queryInterface, Sequelize) {
  return queryInterface.createTable('physician_orders', {
    physician_order_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    patient_registry_id: {
      allowNull: false,
      onDelete: 'CASCADE',
      references: {
        model: 'patient_registries',
        key: 'patient_registry_id',
      },
      type: Sequelize.INTEGER,
    },
    physician_id: {
      onDelete: 'CASCADE',
      references: {
        model: 'physicians',
        key: 'physician_id',
      },
      type: Sequelize.INTEGER,
    },
    assessment: {
      type: Sequelize.TEXT,
    },
    medication: {
      type: Sequelize.TEXT,
    },
    procedure: {
      type: Sequelize.TEXT,
    },
    diagnostic_test: {
      type: Sequelize.TEXT,
    },
    other_remark: {
      type: Sequelize.TEXT,
    },
    physician_order_status: {
      type: Sequelize.ENUM('pending', 'approved', 'completed'),
      defaultValue: 'pending',
    },
    created_by: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    updated_by: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    deleted_by: {
      type: Sequelize.INTEGER,
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    deleted_at: {
      type: Sequelize.DATE,
    },
  });
}
export function down(queryInterface) {
  return queryInterface.dropTable('physician_orders');
}
