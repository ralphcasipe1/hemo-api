export function up(queryInterface, Sequelize) {
  return queryInterface.createTable('template_anti_coagulants', {
    template_anti_coagulant_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    patient_id: {
      allowNull: false,
      onDelete: 'CASCADE',
      references: {
        model: 'patients',
        key: 'patient_id',
      },
      type: Sequelize.INTEGER,
    },
    nss_flushing: {
      type: Sequelize.STRING(50),
    },
    nss_flushing_every: {
      type: Sequelize.STRING(50),
    },
    lmwh_iv: {
      type: Sequelize.STRING(50),
    },
    lmwh_iv_iu: {
      type: Sequelize.STRING(50),
    },
    ufh_iv: {
      type: Sequelize.STRING(50),
    },
    ufh_iv_iu: {
      type: Sequelize.STRING(50),
    },
    ufh_iv_iu_every: {
      type: Sequelize.STRING(50),
    },
    bleeding_desc: {
      type: Sequelize.TEXT,
    },
    created_by: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    updated_by: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    deleted_by: {
      type: Sequelize.INTEGER,
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    deleted_at: {
      type: Sequelize.DATE,
    },
  });
}
export function down(queryInterface) {
  return queryInterface.dropTable('template_anti_coagulants');
}
