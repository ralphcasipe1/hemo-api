export function up(queryInterface, Sequelize) {
  return queryInterface.createTable('pre_assessments', {
    pre_assessment_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    patient_registry_id: {
      allowNull: false,
      onDelete: 'CASCADE',
      references: {
        model: 'patient_registries',
        key: 'patient_registry_id',
      },
      type: Sequelize.INTEGER,
    },
    mobility_id: {
      allowNull: false,
      onDelete: 'CASCADE',
      references: {
        model: 'mobilities',
        key: 'mobility_id',
      },
      type: Sequelize.INTEGER,
    },
    hbs_ag: {
      type: Sequelize.INTEGER,
    },
    anti_hbs: {
      type: Sequelize.INTEGER,
    },
    anti_hcv: {
      type: Sequelize.INTEGER,
    },
    anti_hiv: {
      type: Sequelize.INTEGER,
    },
    antigen_date: {
      type: Sequelize.DATEONLY,
    },
    created_by: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    updated_by: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    deleted_by: {
      type: Sequelize.INTEGER,
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    deleted_at: {
      type: Sequelize.DATE,
    },
  });
}
export function down(queryInterface) {
  return queryInterface.dropTable('pre_assessments');
}
