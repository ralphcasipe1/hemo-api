export function up(queryInterface, Sequelize) {
  return queryInterface.createTable('post_objective_remarks', {
    post_objective_remark_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    patient_registry_id: {
      allowNull: false,
      onDelete: 'CASCADE',
      reference: {
        model: 'patient_registries',
        key: 'patient_registry_id',
      },
      type: Sequelize.INTEGER,
    },
    remark: {
      type: Sequelize.TEXT,
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
  });
}
export function down(queryInterface) {
  return queryInterface.dropTable('post_objective_remarks');
}
