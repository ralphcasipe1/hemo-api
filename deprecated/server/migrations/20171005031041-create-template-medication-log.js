export function up(queryInterface, Sequelize) {
  return queryInterface.createTable('template_medication_logs', {
    template_medication_log_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    template_medication_id: {
      allowNull: false,
      onDelete: 'CASCADE',
      references: {
        model: 'template_medications',
        key: 'template_medication_id',
      },
      type: Sequelize.INTEGER,
    },
    description: {
      type: Sequelize.TEXT,
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
  });
}
export function down(queryInterface) {
  return queryInterface.dropTable('template_medication_logs');
}
