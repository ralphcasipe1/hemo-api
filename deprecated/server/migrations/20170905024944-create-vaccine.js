export function up(queryInterface, Sequelize) {
  return queryInterface.createTable('vaccines', {
    vaccine_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    patient_registry_id: {
      allowNull: false,
      onDelete: 'CASCADE',
      references: {
        model: 'patient_registries',
        key: 'patient_registry_id',
      },
      type: Sequelize.INTEGER,
    },
    first_dose_date: {
      type: Sequelize.DATEONLY,
    },
    second_dose_date: {
      type: Sequelize.DATEONLY,
    },
    third_dose_date: {
      type: Sequelize.DATEONLY,
    },
    booster_date: {
      type: Sequelize.DATEONLY,
    },
    flu_date: {
      type: Sequelize.DATEONLY,
    },
    pneumonia_spec: {
      type: Sequelize.TEXT,
    },
    pneumonia_date: {
      type: Sequelize.DATEONLY,
    },
    remarks: {
      type: Sequelize.TEXT,
    },
    created_by: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    updated_by: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    deleted_by: {
      type: Sequelize.INTEGER,
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    deleted_at: {
      type: Sequelize.DATE,
    },
  });
}
export function down(queryInterface) {
  return queryInterface.dropTable('vaccines');
}
