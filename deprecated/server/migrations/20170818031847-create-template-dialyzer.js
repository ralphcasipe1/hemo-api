export function up(queryInterface, Sequelize) {
  return queryInterface.createTable('template_dialyzers', {
    template_dialyzer_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    patient_id: {
      allowNull: false,
      onDelete: 'CASCADE',
      references: {
        model: 'patients',
        key: 'patient_id',
      },
      type: Sequelize.INTEGER,
    },
    dialyzer_brand_id: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    type: {
      allowNull: false,
      type: Sequelize.STRING(30),
    },
    dialyzer_size_id: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    dialyzer_date: {
      type: Sequelize.DATEONLY,
    },
    count: {
      defaultValue: 1,
      type: Sequelize.INTEGER,
    },
    d_t: {
      type: Sequelize.DATEONLY,
    },
    created_by: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    updated_by: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    deleted_by: {
      type: Sequelize.INTEGER,
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    deleted_at: {
      type: Sequelize.DATE,
    },
  });
}
export function down(queryInterface) {
  return queryInterface.dropTable('template_dialyzers');
}
