export function up(queryInterface, Sequelize) {
  return queryInterface.createTable('physicians', {
    physician_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    bizbox_physician_no: {
      allowNull: false,
      type: Sequelize.BIGINT,
      unique: true,
    },
    fname: {
      allowNull: false,
      type: Sequelize.STRING(100),
    },
    lname: {
      allowNull: false,
      type: Sequelize.STRING(100),
    },
    date_of_birth: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    specialization: {
      allowNull: false,
      type: Sequelize.STRING(100),
    },
    created_by: {
      type: Sequelize.INTEGER,
    },
    updated_by: {
      type: Sequelize.INTEGER,
    },
    deleted_by: {
      type: Sequelize.INTEGER,
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    deleted_at: {
      type: Sequelize.DATE,
    },
  });
}
export function down(queryInterface) {
  return queryInterface.dropTable('physicians');
}
