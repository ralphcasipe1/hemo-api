export function up(queryInterface, Sequelize) {
  return queryInterface.createTable('dialyzer_sizes', {
    dialyzer_size_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    size_name: {
      allowNull: false,
      unique: true,
      type: Sequelize.STRING(20),
    },
    description: {
      type: Sequelize.STRING(150),
    },
    created_by: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    updated_by: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    deleted_by: {
      type: Sequelize.INTEGER,
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    deleted_at: {
      type: Sequelize.DATE,
    },
  });
}
export function down(queryInterface) {
  return queryInterface.dropTable('dialyzer_sizes');
}
