export function up(queryInterface, Sequelize) {
  return queryInterface.createTable('vascular_normal_assessments', {
    vascular_normal_assessment_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    vascular_id: {
      allowNull: false,
      onDelete: 'CASCADE',
      references: {
        model: 'vasculars',
        key: 'vascular_id',
      },
      type: Sequelize.INTEGER,
    },
    normal_assess: {
      type: Sequelize.INTEGER,
    },
    created_by: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    updated_by: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    deleted_by: {
      type: Sequelize.INTEGER,
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    deleted_at: {
      type: Sequelize.DATE,
    },
  });
}
export function down(queryInterface) {
  return queryInterface.dropTable('vascular_normal_assessments');
}
