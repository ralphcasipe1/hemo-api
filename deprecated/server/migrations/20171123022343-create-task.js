export function up(queryInterface, Sequelize) {
  return queryInterface.createTable('tasks', {
    task_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    user_id: {
      allowNull: false,
      onDelete: 'CASCADE',
      references: {
        model: 'users',
        key: 'user_id',
      },
      type: Sequelize.INTEGER,
    },
    task_desc: {
      allowNull: false,
      type: Sequelize.TEXT,
    },
    task_status: {
      type: Sequelize.ENUM('pending', 'completed'),
      defaultValue: 'pending',
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
  });
}
export function down(queryInterface) {
  return queryInterface.dropTable('tasks');
}
