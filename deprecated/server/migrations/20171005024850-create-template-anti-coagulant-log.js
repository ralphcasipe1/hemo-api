export function up(queryInterface, Sequelize) {
  return queryInterface.createTable('template_anti_coagulant_logs', {
    template_anti_coagulant_log_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    template_anti_coagulant_id: {
      allowNull: false,
      onDelete: 'CASCADE',
      references: {
        model: 'template_anti_coagulants',
        key: 'template_anti_coagulant_id',
      },
      type: Sequelize.INTEGER,
    },
    description: {
      type: Sequelize.TEXT,
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
  });
}
export function down(queryInterface) {
  return queryInterface.dropTable('template_anti_coagulant_logs');
}
