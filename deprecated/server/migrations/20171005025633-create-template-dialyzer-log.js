export function up(queryInterface, Sequelize) {
  return queryInterface.createTable('template_dialyzer_logs', {
    template_dialyzer_log_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    template_dialyzer_id: {
      allowNull: false,
      onDelete: 'CASCADE',
      references: {
        model: 'template_dialyzers',
        key: 'template_dialyzer_id',
      },
      type: Sequelize.INTEGER,
    },
    description: {
      type: Sequelize.TEXT,
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
  });
}
export function down(queryInterface) {
  return queryInterface.dropTable('template_dialyzer_logs');
}
