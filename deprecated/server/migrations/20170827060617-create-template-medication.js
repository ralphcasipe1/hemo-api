export function up(queryInterface, Sequelize) {
  return queryInterface.createTable('template_medications', {
    template_medication_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    patient_id: {
      allowNull: false,
      onDelete: 'CASCADE',
      references: {
        model: 'patients',
        key: 'patient_id',
      },
      type: Sequelize.INTEGER,
    },
    generic: {
      type: Sequelize.STRING(100),
    },
    brand: {
      type: Sequelize.STRING(100),
    },
    preparation: {
      type: Sequelize.TEXT,
    },
    dosage: {
      type: Sequelize.STRING(20),
    },
    route: {
      type: Sequelize.STRING(20),
    },
    timing: {
      type: Sequelize.STRING(50),
    },
    created_by: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    updated_by: {
      allowNull: false,
      type: Sequelize.INTEGER,
    },
    deleted_by: {
      type: Sequelize.INTEGER,
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    deleted_at: {
      type: Sequelize.DATE,
    },
  });
}
export function down(queryInterface) {
  return queryInterface.dropTable('template_medications');
}
