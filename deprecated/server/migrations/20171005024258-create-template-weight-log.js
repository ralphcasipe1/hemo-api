export function up(queryInterface, Sequelize) {
  return queryInterface.createTable('template_weight_logs', {
    template_weight_log_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    template_weight_id: {
      allowNull: false,
      onDelete: 'CASCADE',
      references: {
        model: 'template_weights',
        key: 'template_weight_id',
      },
      type: Sequelize.INTEGER,
    },
    description: {
      type: Sequelize.STRING,
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
  });
}
export function down(queryInterface) {
  return queryInterface.dropTable('template_weight_logs');
}
