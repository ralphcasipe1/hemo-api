export function up(queryInterface, Sequelize) {
  return queryInterface.createTable('template_parameter_logs', {
    template_parameter_log_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    template_parameter_id: {
      allowNull: false,
      onDelete: 'CASCADE',
      references: {
        model: 'template_parameters',
        key: 'template_parameter_id',
      },
      type: Sequelize.INTEGER,
    },
    description: {
      type: Sequelize.TEXT,
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
  });
}
export function down(queryInterface) {
  return queryInterface.dropTable('template_parameter_logs');
}
