export function up(queryInterface, Sequelize) {
  return queryInterface.createTable('objective_assessments', {
    patient_registry_id: {
      allowNull: false,
      references: {
        model: 'patient_registries',
        key: 'patient_registry_id',
      },
      type: Sequelize.INTEGER,
    },
    objective_id: {
      allowNull: false,
      references: {
        model: 'objectives',
        key: 'objective_id',
      },
      type: Sequelize.INTEGER,
    },
    created_by: {
      type: Sequelize.INTEGER,
    },
    updated_by: {
      type: Sequelize.INTEGER,
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
  });
}
export function down(queryInterface) {
  return queryInterface.dropTable('objective_assessments');
}
