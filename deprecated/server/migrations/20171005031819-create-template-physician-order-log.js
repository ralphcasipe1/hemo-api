export function up(queryInterface, Sequelize) {
  return queryInterface.createTable('template_physician_order_logs', {
    template_physician_order_log_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    template_physician_order_id: {
      allowNull: false,
      onDelete: 'CASCADE',
      references: {
        model: 'template_physician_orders',
        key: 'template_physician_order_id',
      },
      type: Sequelize.INTEGER,
    },
    description: {
      type: Sequelize.TEXT,
    },
    createad_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
  });
}
export function down(queryInterface) {
  return queryInterface.dropTable('template_physician_order_logs');
}
