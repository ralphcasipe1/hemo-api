export function up(queryInterface, Sequelize) {
  return queryInterface.createTable('users', {
    user_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    fname: {
      allowNull: false,
      type: Sequelize.STRING(100),
    },
    mname: {
      type: Sequelize.STRING(100),
    },
    lname: {
      allowNull: false,
      type: Sequelize.STRING(100),
    },
    id_number: {
      allowNull: false,
      unique: true,
      type: Sequelize.STRING(30),
    },
    password: {
      allowNull: false,
      type: Sequelize.STRING,
    },
    is_admin: {
      defaultValue: false,
      type: Sequelize.BOOLEAN,
    },
    is_doctor: {
      defaultValue: false,
      type: Sequelize.BOOLEAN,
    },
    created_by: {
      type: Sequelize.INTEGER,
    },
    updated_by: {
      type: Sequelize.INTEGER,
    },
    deleted_by: {
      type: Sequelize.INTEGER,
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    deleted_at: {
      type: Sequelize.DATE,
    },
  });
}
export function down(queryInterface) {
  return queryInterface.dropTable('users');
}
