export function up(queryInterface, Sequelize) {
  return queryInterface.createTable('timelines', {
    timeline_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    activity_id: {
      allowNull: false,
      onDelete: 'CASCADE',
      references: {
        model: 'activities',
        key: 'activity_id',
      },
      type: Sequelize.INTEGER,
    },
    user_id: {
      allowNull: false,
      onDelete: 'CASCADE',
      references: {
        model: 'users',
        key: 'user_id',
      },
      type: Sequelize.INTEGER,
    },
    description: {
      type: Sequelize.TEXT,
    },
    timeline_status: {
      type: Sequelize.ENUM(
        'pending',
        'added',
        'updated',
        'deleted',
        'completed'
      ),
      defaultValue: 'added',
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
    },
  });
}
export function down(queryInterface) {
  return queryInterface.dropTable('timelines');
}
