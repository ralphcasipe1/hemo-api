import db from '../models';

const { Task, } = db.sequelize.models;

const TaskController = {
  create(req, res) {
    return Task.create({
      user_id: req.user.user_id,
      task_desc: req.body.task_desc,
    }).then((data) => res.status(200).send({
      data,
    }), (err) => res.status(400).send({
      message: err.message,
    }));
  },

  list(req, res) {
    return Task.findAll({
      where: {
        user_id: req.user.user_id,
      },
    }).then((data) => res.status(200).send({
      data,
    }), (err) => res.status(400).send({
      message: err.message,
    }));
  },

  update(req, res) {
    return Task.findOne({
      task_id: req.params.taskId,
      user_id: req.user.user_id,
    }).then((task) => {
      if (!task) {
        return res.status(404).send({
          message: 'No task found',
        });
      }
      return task
        .update(
          req.body,
          { fields: Object.keys(req.body), }
        )
        .then(
          () => res.status(200).send({ data: task, }),
          (err) => res.status(400).send({
            message: err,
          })
        );
    }, (err) => res.status(400).send({
      message: err,
    }));
  },
};

export default TaskController;

