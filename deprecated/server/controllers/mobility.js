import db from '../models';

const { Mobility, } = db.sequelize.models;

const MobilityController = {
  create(req, res) {
    return Mobility.create({
      name: req.body.name,
      description: req.body.description,
      created_by: req.user.user_id,
      updated_by: req.user.user_id,
    }).then((mobility) => res.status(201).send({
      data: mobility,
    }), (err) => res.status(400).send({
      status: err.status,
      message: err.message,
    }));
  },

  destroy(req, res) {
    return Mobility.findById(req.params.mobilityId).then((mobility) => {
      if (!mobility) {
        return res.status(400).send({
          message: 'No mobility found',
        });
      }
      return mobility.destroy().then(() => res.status(200).send({
        data: mobility,
        message: 'Mobility deleted successfully',
      }), (err) => res.status(400).send({
        message: err.message,
      }));
    });
  },

  list(req, res) {
    return Mobility.all({
      limit: 15,
      order: [
        [
          'created_at',
          'ASC',
        ],
      ],
    }).then((mobilitys) => res.status(200).send({
      data: mobilitys,
    }), (err) => res.status(400).send({
      message: err.message,
    }));
  },

  select(req, res) {
    return Mobility.findById(req.params.mobilityId).then((mobility) => res.status(200).send({
      data: mobility,
    }), (err) => res.status(400).send({
      message: err.message,
    }));
  },

  update(req, res) {
    return Mobility.findById(req.params.mobilityId).then((mobility) => {
      if (!mobility) {
        return res.status(404).send({
          message: 'No mobility found',
        });
      }
      return mobility
        .update(req.body, { fields: Object.keys(req.body), })
        .then(() => res.status(200).send({
          data: mobility,
        }), (err) => res.status(400).send({
          message: err.message,
        }));
    }, (err) => res.status(400).send({
      message: err.message,
    }));
  },
};

export default MobilityController;

