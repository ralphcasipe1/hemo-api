import db from '../models';

const { DialyzerBrand, } = db.sequelize.models;

const DialyzerBrandController = {
  create(req, res) {
    return DialyzerBrand.create({
      brand_name: req.body.brand_name,
      description: req.body.description,
      created_by: req.user.user_id,
      updated_by: req.user.user_id,
    }).then((dialyzerBrand) => res.status(201).send({
      data: dialyzerBrand,
    }), (err) => res.status(400).send({
      status: err.status,
      message: err.message,
    }));
  },

  destroy(req, res) {
    return DialyzerBrand.findById(req.params.dialyzerBrandId).then((dialyzerBrand) => {
      if (!dialyzerBrand) {
        return res.status(400).send({
          message: 'No dialyzer brand found',
        });
      }
      return dialyzerBrand.destroy().then(() => res.status(200).send({
        data: dialyzerBrand,
        message: 'Dialyzer brand deleted successfully',
      }), (err) => res.status(400).send({
        message: err.message,
      }));
    });
  },

  list(req, res) {
    return DialyzerBrand.all({
      limit: 15,
      order: [
        [
          'created_at',
          'ASC',
        ],
      ],
    }).then((dialyzerBrands) => res.status(200).send({
      data: dialyzerBrands,
    }), (err) => res.status(400).send({
      message: err.message,
    }));
  },

  select(req, res) {
    return DialyzerBrand
      .findById(req.params.dialyzerBrandId)
      .then(
        (dialyzerBrand) => res.status(200).send({
          data: dialyzerBrand,
        }),
        (err) => res.status(400).send({
          message: err.message,
        })
      );
  },

  update(req, res) {
    return DialyzerBrand.findById(req.params.dialyzerBrandId).then((dialyzerBrand) => {
      if (!dialyzerBrand) {
        return res.status(404).send({
          message: 'No dialyzer brand found',
        });
      }
      return dialyzerBrand
        .update(req.body, { fields: Object.keys(req.body), })
        .then(() => res.status(200).send({
          data: dialyzerBrand,
        }), (err) => res.status(400).send({
          message: err.message,
        }));
    }, (err) => res.status(400).send({
      message: err.message,
    }));
  },
};

export default DialyzerBrandController;
