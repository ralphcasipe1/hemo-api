import db from '../models';

const { Abnormal, Catheter, } = db.sequelize.models;

const CatheterAbnormalController = {
  setCatheterAbnormals(req, res) {
    return Catheter.findById(req.params.catheterId).then((catheter) => {
      if (!catheter) {
        return res.status(404).send({
          message: 'No catheter found',
        });
      }
      return catheter
        .setAbnormals(req.body.abnormal_id, {
          through: {
            created_by: req.user.user_id,
            updated_by: req.user.user_id,
          },
        })
        .then(() => Catheter.findById(req.params.catheterId, {
          include: [
            {
              model: Abnormal,
            },
          ],
        }).then(() => res.status(201).send({ data: catheter, }), (err) => res.status(400).send({
          message: err.message,
        })), (err) => res.status(400).send({
          message: err.message,
        }));
    }, (err) => res.status(400).send({
      message: err.message,
    }));
  },

  listCatheterAbnormals(req, res) {
    return Catheter.findById(req.params.catheterId).then((catheter) => {
      if (!catheter) {
        return res.status(404).send({
          message: 'No catheter found',
        });
      }
      return catheter.getAbnormals().then((data) => res.status(201).send({
        data,
      }), (err) => res.status(400).send(err));
    });
  },

  selectCatheterAbnormal(req, res) {
    return Catheter.findById(req.params.catheterId).then((catheter) => {
      if (!catheter) {
        return res.status(404).send({
          message: 'No catheter found',
        });
      }
      return catheter
        .getAbnormals({
          where: {
            abnormal_id: req.params.abnormalId,
          },
        })
        .then((data) => res.status(201).send({
          data,
        }), (err) => res.status(400).send(err));
    });
  },

  removeCatheterAbnormal(req, res) {
    return Catheter.findById(req.params.catheterId).then((catheter) => {
      if (!catheter) {
        return res.status(404).send({
          message: 'No catheter found',
        });
      }
      return catheter.setAbnormals([]).then((data) => res.status(201).send({
        data,
      }), (err) => res.status(400).send(err));
    });
  },
};

export default CatheterAbnormalController;
