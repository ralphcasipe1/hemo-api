import db from '../models';

const { PatientRegistry, } = db.sequelize.models;

const PostObjectiveAssessmentController = {
  setPostObjectives(req, res) {
    return PatientRegistry.findById(req.params.patientRegistryId).then(
      (patientRegistry) => {
        if (!patientRegistry) {
          return res.status(404).send({
            message: 'No patient registry found',
          });
        }
        return patientRegistry
          .setPostObjectives(req.body.objective_id, {
            through: {
              created_by: req.user.user_id,
              updated_by: req.user.user_id,
            },
          })
          .then(
            () =>
              PatientRegistry.findById(req.params.patientRegistryId).then(
                () => {
                  if (!patientRegistry) {
                    return res.status(404).send({
                      message: 'No patient registry found',
                    });
                  }
                  return patientRegistry.getPostObjectives().then(
                    (data) =>
                      res.status(201).send({
                        data,
                      }),
                    (err) =>
                      res.status(400).send({
                        message: err.message,
                      })
                  );
                },
                (err) =>
                  res.status(400).send({
                    message: err.message,
                  })
              ),
            (err) => res.status(400).send(err)
          );
      },
      (err) => res.status(400).send(err)
    );
  },

  listPostObjectives(req, res) {
    return PatientRegistry.findById(req.params.patientRegistryId).then((patientRegistry) => {
      if (!patientRegistry) {
        return res.status(404).send({
          message: 'No patient registry found',
        });
      }
      return patientRegistry.getPostObjectives().then(
        (data) =>
          res.status(201).send({
            data,
          }),
        (err) =>
          res.status(400).send({
            message: err.message,
          })
      );
    });
  },

  selectPostObjective(req, res) {
    return PatientRegistry.findById(req.params.patientRegistryId).then((patientRegistry) => {
      if (!patientRegistry) {
        return res.status(404).send({
          message: 'No patient registry found',
        });
      }
      return patientRegistry
        .getPostObjectives({
          where: {
            objective_id: req.params.objectiveId,
          },
        })
        .then(
          (data) =>
            res.status(201).send({
              data,
            }),
          (err) => res.status(400).send(err)
        );
    });
  },

  removePostObjectives(req, res) {
    return PatientRegistry.findById(req.params.patientRegistryId).then((patientRegistry) => {
      if (!patientRegistry) {
        return res.status(404).send({
          message: 'No patient registry found',
        });
      }
      return patientRegistry.setPostObjectives([]).then(
        (data) =>
          res.status(201).send({
            data: data.reduce((nest) => nest.concat(data)),
          }),
        (err) => res.status(400).send(err)
      );
    });
  },
};

export default PostObjectiveAssessmentController;
