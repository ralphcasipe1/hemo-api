import db from '../models';

const { Activity, } = db.sequelize.models;

export class ActivityController {

  create(request, response) {
    return Activity.create({ name: request.body.name })
      .then(
        data => response.status(200).send({ data }),
        error => response.status(400).send({ message: error.message })
      );
  }

  list(request, response) {
    return Activity.findAll()
      .then(
        data => response.status(200).send({ data }),
        error => response.status(400).send({ message: error.message })
      )
  }
}