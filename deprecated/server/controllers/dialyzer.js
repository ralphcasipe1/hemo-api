import db from '../models';

const {
  Dialyzer,
  DialyzerBrand,
  DialyzerSize,
  PatientRegistry,
} = db.sequelize.models;

const DialyzerController = {
  create(req, res) {
    return PatientRegistry.findById(req.params.patientRegistryId).then((patientRegistry) => {
      if (!patientRegistry) {
        return res.status(404).send({
          message: 'No patient registry found',
        });
      }
      return patientRegistry
        .createDialyzer({
          patient_registry_id: req.params.patientRegistryId,
          dialyzer_brand_id: req.body.dialyzer_brand_id,
          type: req.body.type,
          dialyzer_size_id: req.body.dialyzer_size_id,
          dialyzer_date: req.body.dialyzer_date,
          count: req.body.count,
          d_t: req.body.d_t,
          created_by: req.user.user_id,
          updated_by: req.user.user_id,
        })
        .then((dialyzer) => Dialyzer.findById(dialyzer.dialyzer_id, {
          include: [
            {
              model: DialyzerBrand,
              as: 'dialyzerBrand',
              attributes: [
                'dialyzer_brand_id',
                'brand_name',
                'description',
              ],
            },
            {
              model: DialyzerSize,
              as: 'dialyzerSize',
              attributes: [
                'dialyzer_size_id',
                'size_name',
                'description',
              ],
            },
          ],
          attributes: [
            'dialyzer_id',
            'patient_registry_id',
            'type',
            'dialyzer_date',
            'count',
            'd_t',
          ],
        }).then(() => res.status(201).send({
          data: dialyzer,
        }), (err) => res.status(400).send({
          name: err.name,
          message: err.message,
          detail: err.parent.detail,
        })), (err) => res.status(400).send({
          name: err.name,
          message: err.message,
          detail: err.parent.detail,
        }));
    });
  },

  destroy(req, res) {
    return Dialyzer.findById(req.params.dialyzerId).then((dialyzer) => {
      if (!dialyzer) {
        return res.status(400).send({
          message: 'No dialyzer found',
        });
      }
      return dialyzer.destroy().then(() => res.status(200).send({
        message: 'Dialyzer deleted successfully',
      }), (err) => res.status(400).send({
        message: err.message,
      }));
    });
  },

  get(req, res) {
    return PatientRegistry.findById(req.params.patientRegistryId).then((patientRegistry) => {
      if (!patientRegistry) {
        return res.status(404).send({
          message: 'No patient registry found',
        });
      }
      return patientRegistry
        .getDialyzer({
          include: [
            {
              model: DialyzerBrand,
              as: 'dialyzerBrand',
              attributes: [
                'dialyzer_brand_id',
                'brand_name',
                'description',
              ],
            },
            {
              model: DialyzerSize,
              as: 'dialyzerSize',
              attributes: [
                'dialyzer_size_id',
                'size_name',
                'description',
              ],
            },
          ],
          attributes: [
            'dialyzer_id',
            'patient_registry_id',
            'type',
            'dialyzer_date',
            'count',
            'd_t',
            'created_at',
            'updated_at',
          ],
        })
        .then((dialyzer) => res.status(200).send({
          data: dialyzer,
        }), (err) => res.status(400).send({
          message: err.message,
        }));
    });
  },

  select(req, res) {
    return Dialyzer.findById(req.params.dialyzerId, {
      include: [
        {
          model: DialyzerBrand,
          as: 'dialyzerBrand',
          attributes: [
            'dialyzer_brand_id',
            'brand_name',
            'description',
          ],
        },
        {
          model: DialyzerSize,
          as: 'dialyzerSize',
          attributes: [
            'dialyzer_size_id',
            'size_name',
            'description',
          ],
        },
      ],
      attributes: [
        'dialyzer_id',
        'patient_registry_id',
        'type',
        'dialyzer_date',
        'count',
        'd_t',
      ],
    }).then((dialyzer) => res.status(200).send({
      data: dialyzer,
    }), (err) => res.status(400).send({
      message: err.message,
    }));
  },

  update(req, res) {
    return Dialyzer.findById(req.params.dialyzerId).then((dialyzer) => {
      if (!dialyzer) {
        return res.status(404).send({
          message: 'No dialyzer found',
        });
      }
      return dialyzer
        .update(req.body, { fields: Object.keys(req.body), })
        .then(() => {
          Dialyzer.findById(dialyzer.dialyzer_id, {
            include: [
              {
                model: DialyzerBrand,
                as: 'dialyzerBrand',
                attributes: [
                  'dialyzer_brand_id',
                  'brand_name',
                  'description',
                ],
              },
              {
                model: DialyzerSize,
                as: 'dialyzerSize',
                attributes: [
                  'dialyzer_size_id',
                  'size_name',
                  'description',
                ],
              },
            ],
            attributes: [
              'dialyzer_id',
              'patient_registry_id',
              'type',
              'dialyzer_date',
              'count',
              'd_t',
            ],
          }).then(() => res.status(201).send({
            data: dialyzer,
          }), (err) => res.status(400).send({
            name: err.name,
            message: err.message,
            detail: err.parent.detail,
          }));
        }, (err) => res.status(400).send({
          message: err,
        }));
    }, (err) => res.status(400).send({
      message: err,
    }));
  },
};

export default DialyzerController;
