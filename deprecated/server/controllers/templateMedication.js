import db from '../models';

const { Patient, TemplateMedication, } = db.sequelize.models;

const TemplateMedicationController = {
  create(req, res) {
    return TemplateMedication.create({
      patient_id: req.params.patientId,
      generic: req.body.generic,
      brand: req.body.brand,
      preparation: req.body.preparation,
      timing: req.body.timing,
      dosage: req.body.dosage,
      route: req.body.route,
      created_by: req.user.user_id,
      updated_by: req.user.user_id,
    }).then((templateMedication) => res.status(201).send({
      data: templateMedication,
    }), (err) => res.status(400).send({
      name: err.name,
      message: err.message,
    }));
  },

  destroy(req, res) {
    return TemplateMedication
      .findById(req.params.templateMedicationId)
      .then((templateMedication) => {
        if (!templateMedication) {
          return res.status(400).send({
            message: 'No standing order medication found',
          });
        }
        return templateMedication.destroy().then(() => res.status(200).send({
          data: templateMedication,
          message: 'Standing order medication successfully deleted',
        }), (err) => res.status(400).send({
          message: err.message,
        }));
      });
  },

  list(req, res) {
    return Patient.findById(req.params.patientId).then((patient) => {
      if (!patient) {
        return res.status(404).send({
          message: 'No patient found',
        });
      }
      return patient.getTemplateMedication().then((templateMedication) => res.status(200).send({
        data: templateMedication,
      }), (err) => res.status(400).send({
        message: err.message,
      }));
    }, (err) => res.status(404).send({
      message: err.message,
    }));
  },

  select(req, res) {
    return TemplateMedication
      .findById(req.params.templateMedicationId)
      .then(
        (templateMedication) => res.status(200).send({
          data: templateMedication,
        }),
        (err) => res.status(400).send({
          message: err.message,
        })
      );
  },

  update(req, res) {
    return TemplateMedication
      .findById(req.params.templateMedicationId)
      .then((templateMedication) => {
        if (!templateMedication) {
          return res.status(404).send({
            message: 'No standing order medication found',
          });
        }
        return templateMedication
          .update(req.body, { fields: Object.keys(req.body), })
          .then(
            () => res.status(200).send({ data: templateMedication, }),
            (err) => res.status(400).ssend({
              name: err.name,
              message: err.message,
            })
          );
      }, (err) => res.status(400).send({
        name: err.name,
        message: err.message,
      }));
  },
};

export default TemplateMedicationController;
