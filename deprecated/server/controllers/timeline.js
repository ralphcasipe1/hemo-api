import db from '../models';

const { Activity, Timeline, } = db.sequelize.models;

const TimelineController = {
  create(req, res) {
    return Timeline.create({
      user_id: req.user.user_id,
      activity_id: req.body.activity_id,
      timeline_status: req.body.timeline_status,
      description: req.body.description,
    }).then((data) => res.status(200).send({
      data,
    }), (err) => res.status(400).send({
      message: err.message,
    }));
  },

  list(req, res) {
    return Timeline.findAll({
      where: {
        user_id: req.user.user_id,
      },
      include: [
        {
          model: Activity,
          as: 'activity',
        },
      ],
    }).then((data) => res.status(200).send({
      data,
    }), (err) => res.status(400).send({
      message: err.message,
    }));
  },
};

export default TimelineController;
