import db from '../models';

const { Physician, PhysicianOrder, User, } = db.sequelize.models;

const PhysicianOrderController = {
  create(req, res) {
    return PhysicianOrder.create({
      patient_registry_id: req.params.patientRegistryId,
      assessment: req.body.assessment,
      medication: req.body.medication,
      procedure: req.body.procedure,
      diagnostic_test: req.body.diagnostic_test,
      other_remark: req.body.other_remark,
      physician_id: req.body.physician_id,
      physician_order_status: req.user.is_doctor ? 'approved' : 'pending',
      created_by: req.user.user_id,
      updated_by: req.user.user_id,
    }).then(
      (physicianOrder) => {
        PhysicianOrder.findById(physicianOrder.physician_order_id, {
          include: [
            {
              model: User,
              as: 'createdBy',
              attributes: [
                'fname',
                'mname',
                'lname',
                'id_number',
              ],
            },
            {
              model: Physician,
              as: 'physician',
              attributes: [
                'fname',
                'lname',
                'bizbox_physician_no',
                'physician_id',
              ],
            },
          ],
          attributes: [
            'physician_order_id',
            'patient_registry_id',
            'physician_id',
            'assessment',
            'medication',
            'procedure',
            'diagnostic_test',
            'other_remark',
            'physician_order_status',
            'created_at',
            'updated_at',
            'deleted_at',
          ],
        }).then(
          () =>
            res.status(200).send({
              data: physicianOrder,
            }),
          (err) =>
            res.status(400).send({
              name: err.name,
              message: err.message,
              detail: err.parent.detail,
            })
        );
      },
      (err) =>
        res.status(400).send({
          name: err.name,
          message: err.message,
          detail: err.parent.detail,
        })
    );
  },

  destroy(req, res) {
    return PhysicianOrder.findById(req.params.physicianOrderId).then((physicianOrder) => {
      if (!physicianOrder) {
        return res.status(400).send({
          message: 'No physician order found',
        });
      }
      return physicianOrder.destroy().then(
        () =>
          res.status(200).send({
            data: physicianOrder,
            message: 'Physician order deleted successfully',
          }),
        (err) =>
          res.status(400).send({
            message: err.message,
          })
      );
    });
  },

  list(req, res) {
    return PhysicianOrder.all({
      limit: 15,
      order: [
        [
          'created_at',
          'ASC',
        ],
      ],
      where: {
        patient_registry_id: req.params.patientRegistryId,
      },
      include: [
        {
          model: User,
          as: 'createdBy',
          attributes: [
            'fname',
            'mname',
            'lname',
            'id_number',
          ],
        },
        {
          model: Physician,
          as: 'physician',
          attributes: [
            'fname',
            'lname',
            'bizbox_physician_no',
            'physician_id',
            'date_of_birth',
          ],
        },
      ],
      attributes: [
        'physician_order_id',
        'patient_registry_id',
        'physician_id',
        'assessment',
        'medication',
        'procedure',
        'diagnostic_test',
        'other_remark',
        'physician_order_status',
        'created_at',
        'updated_at',
        'deleted_at',
      ],
    }).then(
      (physicianOrders) =>
        res.status(200).send({
          data: physicianOrders,
        }),
      (err) =>
        res.status(400).send({
          message: err.message,
        })
    );
  },

  select(req, res) {
    return PhysicianOrder.findById(req.params.physicianOrderId).then(
      (physicianOrder) =>
        res.status(200).send({
          data: physicianOrder,
        }),
      (err) =>
        res.status(400).send({
          message: err.message,
        })
    );
  },

  update(req, res) {
    return PhysicianOrder.findById(req.params.physicianOrderId).then(
      (physcianOrder) => {
        if (!physcianOrder) {
          return res.status(404).send({
            message: 'No physician order found',
          });
        }
        return physcianOrder
          .update(req.body, { fields: Object.keys(req.body), })
          .then(
            () => res.status(200).send({ data: physcianOrder, }),
            (err) =>
              res.status(400).send({
                message: err.message,
              })
          );
      },
      (err) =>
        res.status(400).send({
          message: err.message,
        })
    );
  },
};

export default PhysicianOrderController;
