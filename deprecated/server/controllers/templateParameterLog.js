import db from '../models';

const { TemplateParameter, } = db.sequelize.models;

const TemplateParameterLogController = {
  create(req, res) {
    return TemplateParameter
      .findById(req.params.templateParameterId)
      .then((templateParameter) => {
        if (templateParameter.template_parameter_id) {
          return res.status(400)
            .send({
              message: 'No standing order parameter found',
            });
        }
        return templateParameter
          .createTemplateParameterLogs({
            template_parameter_id: templateParameter.template_parameter_id,
          })
          .then((data) => res.status(200)
            .send({
              data,
            }), (err) => res.status(400)
            .send({
              message: err.message,
            }));
      }, (err) => res.status(400)
        .send({
          message: err.message,
        }));
  },

  list(req, res) {
    return TemplateParameter
      .findById(req.params.templateParameterId)
      .then((templateParameter) => {
        if (templateParameter.template_parameter_id) {
          return res.status(400)
            .send({
              message: 'No standing order parameter found',
            });
        }
        return templateParameter
          .getTemplateParameterLogs()
          .then((data) => res.status(200)
            .send({
              data,
            }), (err) => res.status(400)
            .send({
              message: err.message,
            }));
      }, (err) => res.status(400)
        .send({
          message: err.message,
        }));
  },
};

export default TemplateParameterLogController;
