import db from '../models';

const {
  DialyzerBrand,
  DialyzerSize,
  TemplateDialyzer,
} = db.sequelize.models;

const TemplateDialyzerController = {
  create(req, res) {
    return TemplateDialyzer
      .create({
        patient_id: req.params.patientId,
        dialyzer_brand_id: req.body.dialyzer_brand_id,
        type: req.body.type,
        dialyzer_size_id: req.body.dialyzer_size_id,
        dialyzer_date: req.body.dialyzer_date,
        count: req.body.count,
        d_t: req.body.d_t,
        created_by: req.user.user_id,
        updated_by: req.user.user_id,
      })
      .then((templateDialyzer) => TemplateDialyzer
        .findById(templateDialyzer.template_dialyzer_id, {
          include: [
            {
              model: DialyzerBrand,
              as: 'dialyzerBrand',
              attributes: [
                'dialyzer_brand_id',
                'brand_name',
                'description',
              ],
            },
            {
              model: DialyzerSize,
              as: 'dialyzerSize',
              attributes: [
                'dialyzer_size_id',
                'size_name',
                'description',
              ],
            },
          ],
        })
        .then(() => res.status(200)
          .send({
            data: templateDialyzer,
          }), (err) => res.status(400).send({
          message: err.message,
        })), (err) => res.status(400).send({
        name: err.name,
        message: err.message,
        detail: err.parent.detail,
      }));
  },

  destroy(req, res) {
    return TemplateDialyzer
      .findById(req.params.templateDialyzerId)
      .then((templateDialyzer) => {
        if (!templateDialyzer) {
          return res.status(400).send({
            message: 'No standing order dialyzer found',
          });
        }
        return templateDialyzer
          .destroy()
          .then(() => res.status(200).send({
            message: 'Standing order dialyzer deleted successfully',
          }), (err) => res.status(400).send({
            message: err.message,
          }));
      });
  },

  findAll(req, res) {
    return TemplateDialyzer
      .findAll({
        where: {
          patient_id: req.params.patientId,
        },
        include: [
          {
            model: DialyzerBrand,
            as: 'dialyzerBrand',
            attributes: [
              'dialyzer_brand_id',
              'brand_name',
              'description',
            ],
          },
          {
            model: DialyzerSize,
            as: 'dialyzerSize',
            attributes: [
              'dialyzer_size_id',
              'size_name',
              'description',
            ],
          },
        ],
      })
      .then(
        (soDialyzers) => res
          .status(200)
          .send({
            data: soDialyzers,
          }),
        (err) => res
          .status(200)
          .send({
            message: err.message,
          })
      );
  },

  findOne(req, res) {
    return TemplateDialyzer
      .findById(req.params.templateDialyzerId)
      .then((templateDialyzer) => res.status(200)
        .send({
          data: templateDialyzer,
        }), (err) => res.status(400).send({
        message: err.message,
      }));
  },

  update(req, res) {
    return TemplateDialyzer
      .findById(req.params.templateDialyzerId)
      .then(
        (templateDialyzer) => {
          if (!templateDialyzer) {
            return res.status(404).send({
              message: 'No standing order dialyzer found',
            });
          }
          return templateDialyzer
            .update(req.body, { fields: Object.keys(req.body), })
            .then(
              () => TemplateDialyzer
                .findById(templateDialyzer.template_dialyzer_id, {
                  include: [
                    {
                      model: DialyzerBrand,
                      as: 'dialyzerBrand',
                      attributes: [
                        'dialyzer_brand_id',
                        'brand_name',
                        'description',
                      ],
                    },
                    {
                      model: DialyzerSize,
                      as: 'dialyzerSize',
                      attributes: [
                        'dialyzer_size_id',
                        'size_name',
                        'description',
                      ],
                    },
                  ],
                })
                .then(
                  () => res.status(200).send({ data: templateDialyzer, }),
                  (err) => res.status(400).send({
                    message: err,
                  })
                ),
              (err) => res.status(400).send({
                message: err,
              })
            );
        },
        (err) => res.status(400).send({
          message: err,
        })
      );
  },
};

export default TemplateDialyzerController;
