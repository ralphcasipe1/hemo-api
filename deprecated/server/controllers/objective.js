import db from '../models';

const { Objective, } = db.sequelize.models;

const ObjectiveController = {
  create(req, res) {
    return Objective.create({
      name: req.body.name,
      description: req.body.description,
      created_by: req.user.user_id,
      updated_by: req.user.user_id,
    }).then((objective) => res.status(201).send({
      data: objective,
    }), (err) => res.status(400).send({
      status: err.status,
      message: err.message,
    }));
  },

  destroy(req, res) {
    return Objective.findById(req.params.objectiveId).then((objective) => {
      if (!objective) {
        return res.status(400).send({
          message: 'No objective assessment found',
        });
      }
      return objective.destroy().then(() => res.status(200).send({
        data: objective,
        message: 'Objective assessment deleted successfully',
      }), (err) => res.status(400).send({
        message: err.message,
      }));
    });
  },

  list(req, res) {
    return Objective.all({
      limit: 15,
    }).then((objectives) => res.status(200).send({
      data: objectives,
    }), (err) => res.status(400).send({
      message: err.message,
    }));
  },

  select(req, res) {
    return Objective.findById(req.params.objectiveId).then((objective) => res.status(200).send({
      data: objective,
    }), (err) => res.status(400).send({
      message: err.message,
    }));
  },

  update(req, res) {
    return Objective.findById(req.params.objectiveId).then((objective) => {
      if (!objective) {
        return res.status(404).send({
          message: 'No objective assessment found',
        });
      }
      return objective
        .update(req.body, { fields: Object.keys(req.body), })
        .then(() => res.status(200).send({
          data: objective,
        }), (err) => res.status(400).send({
          message: err.message,
        }));
    }, (err) => res.status(400).send({
      message: err.message,
    }));
  },
};

export default ObjectiveController;

