import db from '../models';

const { Patient, TemplateWeight, } = db.sequelize.models;

const TemplateWeightController = {
  create(req, res) {
    return TemplateWeight
      .create({
        patient_id: req.params.patientId,
        dry_weight: req.body.dry_weight,
        created_by: req.user.user_id,
        updated_by: req.user.user_id,
      })
      .then((templateWeight) => res.status(201)
        .send({
          data: templateWeight,
        }), (err) => res.status(400).send({
        name: err.name,
        message: err.message,
        detail: err.parent.detail,
      }));
  },

  destroy(req, res) {
    return TemplateWeight
      .findById(req.params.templateWeightId)
      .then((templateWeight) => {
        if (!templateWeight) {
          return res.status(400).send({
            message: 'No standing order weight found',
          });
        }
        return templateWeight
          .destroy()
          .then(() => res.status(200).send({
            message: 'Standing order weight deleted successfully',
          }), (err) => res.status(400).send({
            message: err.message,
          }));
      });
  },

  findAll(req, res) {
    return TemplateWeight
      .findAll({
        where: {
          patient_id: req.params.patientId,
        }
      })
      .then(
        (sOWeights) => res
          .status(200)
          .send({
            data: sOWeights,
          }),
        (err) => res
          .status(400)
          .send({
            message: err.message,
          })
      );
  },

  findOne(req, res) {
    return TemplateWeight
      .findById(req.params.templateWeightId)
      .then((templateWeight) => res.status(200)
        .send({
          data: templateWeight,
        }), (err) => res.status(400).send({
        message: err.message,
      }));
  },

  update(req, res) {
    return TemplateWeight
      .findById(req.params.templateWeightId)
      .then((templateWeight) => {
        if (!templateWeight) {
          return res.status(404).send({
            message: 'No standing order weight found',
          });
        }
        return templateWeight
          .update(req.body, { fields: Object.keys(req.body), })
          .then(
            () => res.status(200).send({ data: templateWeight, }),
            (err) => res.status(400).send({
              message: err,
            })
          );
      }, (err) => res.status(400).send({
        message: err,
      }));
  },
};

export default TemplateWeightController;
