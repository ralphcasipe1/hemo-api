import db from '../models';

const { Abnormal, } = db.sequelize.models;

export class AbnormalController {

  create(request, response) {
    return Abnormal.create({
      name: request.body.name,
      description: request.body.description,
      created_by: request.user.user_id,
      updated_by: request.user.user_id,
    })
      .then(
        abnormal => response.status(201).send({ data: abnormal }),
        error => response.status(400).send({
          status: error.status,
          message: error.message,
        })
      );
  }

  destroy(request, response) {
    return Abnormal.findById(request.params.abnormalId)
      .then(abnormal => {
        if (!abnormal) {
          return response.status(400).send({
            message: 'No abnormal assessment found'
          })
        }
      });
  }

  list(request, response) {
    return Abnormal.all({
      limit: 15,
      order: [
        [
          'created_at',
          'ASC',
        ],
      ],
    })
      .then(
        abnormals => response.status(200).send({
          data: abnormals,
        }),
        err => response.status(400).send({
          message: err.message,
        })
      );
  }

  select(request, response) {
    return Abnormal.findById(request.params.abnormalId)
      .then(
        abnormal => response.status(200).send({ data: abnormal }),
        error => response.status(400).send({ message: error.message })
      );
  }

  update(request, response) {
    return Abnormal.findById(request.params.abnormalId)
      .then(
        abnormal => {
          if (!abnormal) {
            return response.status(404).send({
              message: 'No abnormal assessment found',
            });
          }
          return abnormal
            .update(request.body, { fields: Object.keys(request.body), })
            .then(
              () => response.status(200).send({
                data: abnormal,
              }),
              err => response.status(400).send({
                message: err.message,
              })
            );
        },
        err => response.status(400).send({
          message: err.message,
        })
    );
  }
}