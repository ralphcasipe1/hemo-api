import _ from 'lodash';
import db from '../models';

const { Abnormal, Catheter, PatientRegistry, } = db.sequelize.models;

const CatheterController = {
  create(req, res) {
    return PatientRegistry.findById(req.params.patientRegistryId).then((patientRegistry) => {
      if (!patientRegistry) {
        return res.status(404).send({
          message: 'No patient registry found',
        });
      }
      return patientRegistry
        .createCatheter({
          patient_registry_id: req.params.patientRegistryId,
          type: req.body.type,
          position: req.body.position,
          location: req.body.location,
          operation_date: req.body.operation_date,
          physician: req.body.physician,
          days_situ: req.body.days_situ,
          is_red_port_inflow: req.body.is_red_port_inflow,
          is_red_port_outflow: req.body.is_red_port_outflow,
          is_blue_port_inflow: req.body.is_blue_port_inflow,
          is_blue_port_outflow: req.body.is_blue_port_outflow,
          created_by: req.user.user_id,
          updated_by: req.user.user_id,
        })
        .then((catheter) => {
          if (!_.isEmpty(req.body.abnormal_id)) {
            return Catheter.findOne({
              where: {
                catheter_id: catheter.catheter_id,
                patient_registry_id: req.params.patientRegistryId,
              },
            }).then(() => {
              if (!catheter) {
                return res.status(404).send({
                  message: 'No catheter found',
                });
              }
              return catheter
                .setAbnormals(req.body.abnormal_id, {
                  through: {
                    created_by: req.user.user_id,
                    updated_by: req.user.user_id,
                  },
                })
                .then(() => Catheter.findOne({
                  where: {
                    patient_registry_id: req.params.patientRegistryId,
                  },
                  include: [
                    {
                      model: Abnormal,
                    },
                  ],
                }).then(() => res.status(201).send({
                  data: catheter,
                }), (err) => res.status(400).send({
                  message: err.message,
                })), (err) => res.status(400).send({
                  message: err.message,
                }));
            }, (err) => res.status(400).send({
              message: err.message,
            }));
          }
          return res.status(201).send({
            data: catheter,
          });
        }, (err) => res.status(400).send({
          name: err.name,
          message: err.message,
          detail: err.parent.detail,
        }));
    });
  },

  destroy(req, res) {
    return Catheter.findById(req.params.catheterId).then((catheter) => {
      if (!catheter) {
        return res.status(400).send({
          message: 'No catheter data found',
        });
      }
      return catheter.destroy().then(() => res.status(200).send({
        message: 'Catheter data deleted successfully',
      }), (err) => res.status(400).send({
        message: err.message,
      }));
    });
  },

  get(req, res) {
    return PatientRegistry.findById(req.params.patientRegistryId).then((patientRegistry) => {
      if (!patientRegistry) {
        return res.status(404).send({
          message: 'No patient registry found',
        });
      }
      return patientRegistry
        .getCatheter({
          include: [
            {
              model: Abnormal,
            },
          ],
        })
        .then((catheter) => res.status(200).send({
          data: catheter,
        }), (err) => res.status(400).send({
          message: err.message,
        }));
    });
  },

  select(req, res) {
    return Catheter.findById(req.params.catheterId).then((catheter) => res.status(200).send({
      data: catheter,
    }), (err) => res.status(400).send({
      message: err.message,
    }));
  },

  update(req, res) {
    return Catheter.findById(req.params.catheterId).then((catheter) => {
      if (!catheter) {
        return res.status(404).send({
          message: 'No catheter data found',
        });
      }
      return catheter
        .update(req.body, { fields: Object.keys(req.body), })
        .then(() => PatientRegistry
          .findById(req.params.patientRegistryId)
          .then((patientRegistry) => {
            if (!patientRegistry) {
              return res.status(404).send({
                message: 'No patient registry found',
              });
            }
            return patientRegistry
              .getCatheter({
                include: [
                  {
                    model: Abnormal,
                  },
                ],
              })
              .then(() => res.status(200).send({
                data: catheter,
              }), (err) => res.status(400).send({
                message: err.message,
              }));
          }), (err) => res.status(400).send({
          message: err,
        }));
    }, (err) => res.status(400).send({
      message: err,
    }));
  },
};

export default CatheterController;
