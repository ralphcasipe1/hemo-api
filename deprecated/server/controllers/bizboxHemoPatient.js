import db from '../models';
import sequelize from './bizboxConn';

const { Patient, } = db.sequelize.models;

const BizboxHemoPatientController = {
  list(req, res) {
    return sequelize
      .query(
        `SELECT  PK_psPatRegisters,
          a.FK_emdPatients,
          a.registrydate,
          registrystatus,
          dischdate,    
          f.lastname,
          f.firstname,
          f.middlename,
          dbo.udf_GetPatID(a.FK_emdPatients) HospitalNo,
          b.FK_psRooms as RoomNo,
          c.description as NrsStation,
          d.gender,
          d.civilstatus,
          dbo.udf_GetPrimaryAttendDoctor(a.PK_psPatRegisters) AS AttendingDoctor, 
          dbo.udf_GetPrimaryAdmitDoctor(a.PK_psPatRegisters) AS AdmittingDoctor,
          (select top 1 cc.PK_psDatacenter
              from emddoctors aa 
              inner join psdctrledgers bb 
              on aa.pk_emddoctors = bb.fk_emddoctors        
              inner join psdatacenter cc 
              on aa.pk_emddoctors = cc.pk_psdatacenter     
              where bb.fk_pspatregisters = a.PK_psPatRegisters and bb.fk_emdconsultanttypes = 1002
          ) as dr_id
          FROM psPatRegisters a
          LEFT JOIN psAdmissions b
          on a.PK_psPatRegisters = b.FK_psPatRegisters
          LEFT JOIN mscNrstation c 
          on b.FK_mscNrstation = c.PK_mscNrstation
          left join psPersonaldata d
          on d.PK_psPersonalData = a.FK_emdPatients
          inner join (
          SELECT fk_pspatregisters
          FROM pspatitem t2
          WHERE FK_mscItemCategory = 1009
          and  (renqty-retqty) > 0
          ) e
          on a.PK_psPatRegisters = e.FK_psPatRegisters
          left join psPersonaldata f
          on a.FK_emdPatients = f.PK_psPersonalData
          WHERE a.pattrantype = 'O' 
          AND   (registrystatus <> 'X')
          AND   registrydate Between '${req.params.date} 00:00:00' and '${req.params.date} 23:59:59'
          AND   a.FK_emdPatients <> 1002
          ORDER BY f.lastname`,
        {
          type: sequelize.QueryTypes.SELECT,
        }
      )
      .then((hemoPatients) => Patient.all().then((patients) => res.status(200).send({
        meta: {
          count: hemoPatients.length,
          currentPage: null,
          nextPage: null,
          limit: null,
          offset: null,
          date: req.params.date,
        },
        data: hemoPatients.filter((hemo) => patients
          .map((patient) => +patient.bizbox_patient_no)
          .indexOf(hemo.FK_emdPatients) === -1),
      }), (err) => res.status(400).send({
        message: err.message,
      })), (err) => res.status(400).send({
        message: err.message,
      }));
  },

  select(req, res) {
    sequelize.query(
      `
        SELECT top 1 PK_psPatRegisters
        , a.FK_emdPatients
        , a.registrydate
        , registrystatus
        , dischdate
        , f.lastname
        , f.firstname
        , f.middlename
        , dbo.udf_GetPatID(a.FK_emdPatients) HospitalNo
        , b.FK_psRooms as RoomNo
        , c.description as NrsStation
        , d.gender
        , d.civilstatus
        , dbo.udf_GetPrimaryAttendDoctor(a.PK_psPatRegisters) AS AttendingDoctor
        , dbo.udf_GetPrimaryAdmitDoctor(a.PK_psPatRegisters) AS AdmittingDoctor
        , (select top 1 cc.PK_psDatacenter
            from emddoctors aa
            inner join psdctrledgers bb
            on aa.pk_emddoctors = bb.fk_emddoctors
            inner join psdatacenter cc
            on aa.pk_emddoctors = cc.pk_psdatacenter
            where bb.fk_pspatregisters = a.PK_psPatRegisters and bb.fk_emdconsultanttypes = 1002
          ) as dr_id

        FROM psPatRegisters a
        
        LEFT JOIN psAdmissions b
          ON a.PK_psPatRegisters = b.FK_psPatRegisters
        LEFT JOIN mscNrstation c
          ON b.FK_mscNrstation = c.PK_mscNrstation
        LEFT JOIN psPersonaldata d
          ON d.PK_psPersonalData = a.FK_emdPatients
        INNER JOIN (
            SELECT fk_pspatregisters
            FROM pspatitem t2
            WHERE FK_mscItemCategory = 1009
            and  (renqty-retqty) > 0
        ) e
          ON a.PK_psPatRegisters = e.FK_psPatRegisters
        LEFT JOIN psPersonaldata f
          ON a.FK_emdPatients = f.PK_psPersonalData
        
        WHERE a.pattrantype = 'O'
          AND   (registrystatus <> 'X')
          AND   a.FK_emdPatients <> 1002
          AND   a.FK_emdPatients= :patientId
      `,
      {
        replacements: {
          patientId: req.params.patientId,
        },
        type: sequelize.QueryTypes.SELECT,
      }
    )
      .then(
        (hemoPatient) => res.status(200).send({
          data: hemoPatient.length === 0 ?
            {} :
            hemoPatient.reduce((patient) => patient),
        }),
        (err) => res.status(400).send({
          message: err.message,
        })
      );
  },
};

export default BizboxHemoPatientController;
