import db from '../models';

const { NurseNote, User, } = db.sequelize.models;

const NurseNoteController = {
  create(req, res) {
    return NurseNote.create({
      patient_registry_id: req.params.patientRegistryId,
      note_desc: req.body.note_desc,
      created_by: req.user.user_id,
      updated_by: req.user.user_id,
    }).then((nurseNote) => {
      NurseNote.findById(nurseNote.nurse_note_id, {
        attributes: [
          'nurse_note_id',
          'patient_registry_id',
          'note_desc',
          'created_at',
          'updated_at',
        ],
      }).then(() => res.status(200).send({
        data: nurseNote,
      }), (err) => res.status(400).send({
        name: err.name,
        message: err.message,
        detail: err.parent.detail,
      }));
    }, (err) => res.status(400).send({
      name: err.name,
      message: err.message,
      detail: err.parent.detail,
    }));
  },

  destroy(req, res) {
    return NurseNote.findById(req.params.nurseNoteId).then((nurseNote) => {
      if (!nurseNote) {
        return res.status(400).send({
          message: 'No nurse note found',
        });
      }
      return nurseNote.destroy().then(() => res.status(202).send({
        data: nurseNote,
        message: 'Nurse note successfully deleted',
      }), (err) => res.status(400).send({
        message: err.message,
      }));
    });
  },

  list(req, res) {
    return NurseNote.findAll({
      where: {
        patient_registry_id: req.params.patientRegistryId,
      },
      order: [
        [
          'updated_at',
          'ASC',
        ],
      ],
      include: [
        {
          model: User,
          as: 'updatedBy',
        },
      ],
    })
      .then(
        (nurseNotes) => res
          .status(200)
          .send({
            data: nurseNotes,
          }),
        (err) => res
          .status(400)
          .send({
            message: err.message,
          })
      );
    /* return User.all({
      order: [
        [
          'created_at',
          'ASC',
        ],
      ],
      include: [
        {
          model: NurseNote,
          as: 'nurseNotes',
          where: {
            patient_registry_id: req.params.patientRegistryId,
          },
          attributes: [
            'nurse_note_id',
            'patient_registry_id',
            'note_desc',
            'created_at',
            'updated_at',
          ],
        },
      ],
      attributes: [
        'user_id',
        'fname',
        'mname',
        'lname',
        'id_number',
      ],
    }).then((users) => res.status(200).send({
      data: users,
    }), (err) => res.status(400).send({
      message: err.message,
    })); */
  },

  select(req, res) {
    return NurseNote.findById(req.params.nurseNoteId).then((nurseNote) => res.status(200).send({
      data: nurseNote,
    }), (err) => res.status(400).send({
      message: err.message,
    }));
  },

  update(req, res) {
    return NurseNote.findById(req.params.nurseNoteId).then((nurseNote) => {
      if (!nurseNote) {
        return res.status(404).send({
          message: 'No nurse note found',
        });
      }
      return nurseNote
        .update(req.body, { fields: Object.keys(req.body), })
        .then(() => res.status(200).send({ data: nurseNote, }), (err) => res.status(400).send({
          message: err,
        }));
    }, (err) => res.status(400).send({
      message: err,
    }));
  },
};

export default NurseNoteController;
