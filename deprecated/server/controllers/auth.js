import jwt from 'jsonwebtoken';
import db from '../models';

const { User, } = db.sequelize.models;

export class AuthenticationController {

  authenticate(request, response) {

    return User.findOne({
      where: {
        id_number: request.body.id_number,
        password: request.body.password,
      },
    })
      .then(
        user => {
          if (!user) {
            return response.status(401).json({
              message: 'Authentication failed. Invalid username or password',
            });
          }

          return response.status(200).json({
            accessToken: jwt.sign(
              {
                id_number: user.id_number,
                user_id: user.user_id,
                fname: user.fname,
                mname: user.mname,
                lname: user.lname,
                is_admin: user.is_admin,
                is_doctor: user.is_doctor,
              },
              'unaKonsulta888!?'
            ),
          })
        },
        error => response.status(400).send({ message: error.message })
      )
  }
}
