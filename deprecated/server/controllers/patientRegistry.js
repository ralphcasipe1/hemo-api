import db from '../models';

const { PatientRegistry, } = db.sequelize.models;

const PatientRegistryController = {
  create(req, res) {
    return PatientRegistry.findOrCreate({
      where: {
        registry_no: req.body.registry_no.toString(),
      },
      defaults: {
        patient_id: req.params.patientId,
        created_by: req.user.user_id,
        updated_by: req.user.user_id,
      },
    }).spread((patientRegistry, isCreate) => {
      if (isCreate) {
        return res.status(200).send({
          exist: false,
          data: patientRegistry,
        });
      }
      return res.status(201).send({
        message: 'Patient registry already exists',
        exist: true,
        data: patientRegistry,
      });
    }, (err) => res.status(400).send({
      message: err.message,
    }));
  },

  destroy(req, res) {
    return PatientRegistry.findById(req.params.patientRegistryId).then((patientRegistry) => {
      if (!patientRegistry) {
        return res.status(400).send({
          message: 'No patient registry found',
        });
      }
      return patientRegistry.destroy().then(() => res.status(200).send({
        message: 'Patient registry deleted successfully',
      }), (err) => res.status(400).send({
        message: err.message,
      }));
    });
  },

  list(req, res) {
    return PatientRegistry.all({
      limit: 15,
      where: {
        patient_id: req.params.patientId,
      },
    }).then((patientRegistry) => res.status(200).send({
      data: patientRegistry,
    }), (err) => res.status(400).send({
      message: err.message,
    }));
  },

  select(req, res) {
    return PatientRegistry
      .findById(req.params.patientRegistryId)
      .then((patientRegistry) => res.status(200).send({
        data: patientRegistry,
      }), (err) => res.status(400).send({
        message: err.message,
      }));
  },

  update(req, res) {
    return PatientRegistry.findById(req.params.patientRegistryId).then((patientRegistry) => {
      if (!patientRegistry) {
        return res.status(404).send({
          message: 'No patient registry found',
        });
      }
      return patientRegistry
        .update(req.body, { fields: Object.keys(req.body), })
        .then(
          () => res.status(200).send({ data: patientRegistry, }),
          (err) => res.status(400).send({
            message: err,
          })
        );
    }, (err) => res.status(400).send({
      message: err.message,
    }));
  },
};

export default PatientRegistryController;
