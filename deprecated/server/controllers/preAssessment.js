import db from '../models';

const { Mobility, PreAssessment, PatientRegistry, } = db.sequelize.models;

const PreAssessmentController = {
  create(req, res) {
    return PreAssessment
      .create({
        patient_registry_id: req.params.patientRegistryId,
        mobility_id: req.body.mobility_id,
        hbs_ag: req.body.hbs_ag,
        anti_hbs: req.body.anti_hbs,
        anti_hcv: req.body.anti_hcv,
        anti_hiv: req.body.anti_hiv,
        antigen_date: req.body.antigen_date,
        created_by: req.user.user_id,
        updated_by: req.user.user_id,
      })
      .then((preAssessment) => PreAssessment.findById(preAssessment.pre_assessment_id, {
        include: [
          {
            model: Mobility,
            as: 'mobility',
            attributes: [
              'name',
              'description',
              'mobility_id',
            ],
          },
        ],
        attributes: [
          'pre_assessment_id',
          'patient_registry_id',
          'mobility_id',
          'hbs_ag',
          'anti_hbs',
          'anti_hcv',
          'anti_hiv',
          'antigen_date',
          'created_at',
          'updated_at',
        ],
      })
        .then((data) => res.status(200).send({
          data,
        }), (err) => res.status(400).send({
          name: err.name,
          message: err.message,
        })), (err) => res.status(400).send({
        name: err.name,
        message: err.message,
        detail: err.parent.detail,
      }));
  },

  destroy(req, res) {
    return PreAssessment
      .findById(req.params.preAssessmentId)
      .then((preAssessment) => {
        if (!preAssessment) {
          return res.status(400).send({
            message: 'No pre-assessment found',
          });
        }
        return preAssessment
          .destroy()
          .then(() => res.status(202).send({
            message: 'Pre-assessment deleted successfully',
          }), (err) => res.status(400).send({
            message: err.message,
          }));
      });
  },

  get(req, res) {
    return PatientRegistry
      .findById(req.params.patientRegistryId)
      .then((patientRegistry) => {
        if (!patientRegistry) {
          return res.status(404).send({
            message: 'No patient registry found',
          });
        }
        return patientRegistry
          .getPreAssessment({
            include: [
              {
                model: Mobility,
                as: 'mobility',
                attributes: [
                  'name',
                  'description',
                  'mobility_id',
                ],
              },
            ],
          })
          .then((preAssessment) => res.status(200)
            .send({
              data: preAssessment,
            }), (err) => res.status(400).send({
            message: err.message,
          }));
      });
  },

  select(req, res) {
    return PreAssessment
      .findById(req.params.preAssessmentId)
      .then((preAssessment) => res.status(200)
        .send({
          data: preAssessment,
        }), (err) => res.status(400).send({
        message: err.message,
      }));
  },

  update(req, res) {
    return PreAssessment
      .findById(req.params.preAssessmentId)
      .then((preAssessment) => {
        if (!preAssessment) {
          return res.status(404).send({
            message: 'No pre-assessment found',
          });
        }
        return preAssessment
          .update(req.body, { fields: Object.keys(req.body), })
          .then(
            () => res.status(200).send({ data: preAssessment, }),
            (err) => res.status(400).send({
              message: err,
            })
          );
      }, (err) => res.status(400).send({
        message: err,
      }));
  },
};

export default PreAssessmentController;
