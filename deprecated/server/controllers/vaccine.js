import db from '../models';

const { PatientRegistry, Vaccine, } = db.sequelize.models;

const VaccineController = {
  create(req, res) {
    return PatientRegistry.findById(req.params.patientRegistryId).then((patientRegistry) => {
      if (!patientRegistry) {
        return res.status(404).send({
          message: 'No patient registry found',
        });
      }
      return patientRegistry
        .createVaccine({
          patient_registry_id: req.params.patientRegistryId,
          first_dose_date: req.body.first_dose_date,
          second_dose_date: req.body.second_dose_date,
          third_dose_date: req.body.third_dose_date,
          booster_date: req.body.booster_date,
          flu_date: req.body.flu_date,
          pneumonia_spec: req.body.pneumonia_spec,
          pneumonia_date: req.body.pneumonia_date,
          remarks: req.body.remarks,
          created_by: req.user.user_id,
          updated_by: req.user.user_id,
        })
        .then((vaccine) => res.status(201).send({
          data: vaccine,
        }), (err) => res.status(400).send({
          name: err.name,
          message: err.message,
          detail: err.parent.detail,
        }));
    });
  },

  destroy(req, res) {
    return Vaccine.findById(req.params.vaccineId).then((vaccine) => {
      if (!vaccine) {
        return res.status(400).send({
          message: 'No vaccine found',
        });
      }
      return vaccine.destroy().then(() => res.status(200).send({
        message: 'Vaccine deleted successfully',
      }), (err) => res.status(400).send({
        message: err.message,
      }));
    });
  },

  get(req, res) {
    return PatientRegistry.findById(req.params.patientRegistryId).then((patientRegistry) => {
      if (!patientRegistry) {
        return res.status(404).send({
          message: 'No patient registry found',
        });
      }
      return patientRegistry.getVaccine().then((vaccine) => res.status(200).send({
        data: vaccine,
      }), (err) => res.status(400).send({
        message: err.message,
      }));
    });
  },

  select(req, res) {
    return Vaccine.findById(req.params.vaccineId).then((vaccine) => res.status(200).send({
      data: vaccine,
    }), (err) => res.status(400).send({
      message: err.message,
    }));
  },

  update(req, res) {
    return Vaccine.findById(req.params.vaccineId).then((vaccine) => {
      if (!vaccine) {
        return res.status(404).send({
          message: 'No vaccine found',
        });
      }
      return vaccine
        .update(
          req.body,
          { fields: Object.keys(req.body), }
        )
        .then(
          () => res.status(200).send({ data: vaccine, }),
          (err) => res.status(400).send({
            message: err,
          })
        );
    }, (err) => res.status(400).send({
      message: err,
    }));
  },
};


export default VaccineController;
