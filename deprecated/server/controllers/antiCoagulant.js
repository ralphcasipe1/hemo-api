import db from '../models';

const { AntiCoagulant, PatientRegistry, } = db.sequelize.models;

const AntiCoagulantController = {
  create(req, res) {
    return PatientRegistry.findById(req.params.patientRegistryId).then((patientRegistry) => {
      if (!patientRegistry) {
        return res.status(404).send({
          message: 'No patient registry found',
        });
      }
      return patientRegistry
        .createAntiCoagulant({
          patient_registry_id: req.params.patientRegistryId,
          nss_flushing: req.body.nss_flushing,
          nss_flushing_every: req.body.nss_flushing_every,
          lmwh_iv: req.body.lmwh_iv,
          lmwh_iv_iu: req.body.lmwh_iv_iu,
          ufh_iv: req.body.ufh_iv,
          ufh_iv_iu: req.body.ufh_iv_iu,
          ufh_iv_iu_every: req.body.ufh_iv_iu_every,
          bleeding_desc: req.body.bleeding_desc,
          created_by: req.user.user_id,
          updated_by: req.user.user_id,
        })
        .then(
          (antiCoagulant) =>
            res.status(201).send({
              data: antiCoagulant,
            }),
          (err) =>
            res.status(400).send({
              name: err.name,
              message: err.message,
            })
        );
    });
  },

  destroy(req, res) {
    return AntiCoagulant.findById(req.params.antiCoagulantId).then((antiCoagulant) => {
      if (!antiCoagulant) {
        return res.status(400).send({
          message: 'No anti-coagulant found',
        });
      }
      return antiCoagulant.destroy().then(
        () =>
          res.status(200).send({
            message: 'Anti-coagulant deleted successfully',
          }),
        (err) =>
          res.status(400).send({
            message: err.message,
          })
      );
    });
  },

  get(req, res) {
    return PatientRegistry.findById(req.params.patientRegistryId).then((patientRegistry) => {
      if (!patientRegistry) {
        return res.status(404).send({
          message: 'No patient registry found',
        });
      }
      return patientRegistry.getAntiCoagulant().then(
        (antiCoagulant) =>
          res.status(200).send({
            data: antiCoagulant,
          }),
        (err) =>
          res.status(400).send({
            message: err.message,
          })
      );
    });
  },

  select(req, res) {
    return AntiCoagulant.findById(req.params.antiCoagulantId).then(
      (antiCoagulant) =>
        res.status(200).send({
          data: antiCoagulant,
        }),
      (err) =>
        res.status(400).send({
          message: err.message,
        })
    );
  },

  update(req, res) {
    return AntiCoagulant.findById(req.params.antiCoagulantId).then(
      (antiCoagulant) => {
        if (!antiCoagulant) {
          return res.status(404).send({
            message: 'No anti-coagulant found',
          });
        }
        return antiCoagulant
          .update(req.body, { fields: Object.keys(req.body), })
          .then(
            () => res.status(200).send({ data: antiCoagulant, }),
            (err) =>
              res.status(400).send({
                message: err,
              })
          );
      },
      (err) =>
        res.status(400).send({
          message: err,
        })
    );
  },
};

export default AntiCoagulantController;
