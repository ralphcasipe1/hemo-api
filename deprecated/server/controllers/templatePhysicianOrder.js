import db from '../models';

const { Patient, TemplatePhysicianOrder, } = db.sequelize.models;

const TemplatePhysicianOrderController = {
  create(req, res) {
    return TemplatePhysicianOrder
      .create({
        patient_id: req.params.patientId,
        assessment: req.body.assessment,
        medication: req.body.medication,
        procedure: req.body.procedure,
        diagnostic_test: req.body.diagnostic_test,
        other_remark: req.body.other_remark,
        created_by: req.user.user_id,
        updated_by: req.user.user_id,
      })
      .then((templatePhysicianOrder) => res.status(201)
        .send({
          data: templatePhysicianOrder,
        }), (err) => res.status(400).send({
        name: err.name,
        message: err.message,
      }));
  },

  destroy(req, res) {
    return TemplatePhysicianOrder
      .findById(req.params.templatePhysicianOrderId)
      .then((templatePhysicianOrder) => {
        if (!templatePhysicianOrder) {
          return res.status(400).send({
            message: 'No standing order physician order found',
          });
        }
        return templatePhysicianOrder
          .destroy()
          .then(() => res.status(200).send({
            message: 'Standing order physician order deleted successfully',
          }), (err) => res.status(400).send({
            message: err.message,
          }));
      });
  },

  get(req, res) {
    return Patient
      .findById(req.params.patientId)
      .then((patient) => {
        if (!patient) {
          return res
            .status(404)
            .send({
              message: 'No patient found',
            });
        }
        return patient
          .getTemplatePhysicianOrder()
          .then((templatePhysicianOrder) => res
            .status(200)
            .send({
              data: templatePhysicianOrder,
            }), (err) => res
            .status(400)
            .send({
              message: err.message,
            }));
      }, (err) => res
        .status(404)
        .send({
          message: err.message,
        }));
  },

  select(req, res) {
    return TemplatePhysicianOrder
      .findById(req.params.templatePhysicianOrderId)
      .then((templatePhysicianOrder) => res.status(200)
        .send({
          data: templatePhysicianOrder,
        }), (err) => res.status(400).send({
        message: err.message,
      }));
  },

  update(req, res) {
    return TemplatePhysicianOrder
      .findById(req.params.templatePhysicianOrderId)
      .then((templatePhysicianOrder) => {
        console.log(req.params.templatePhysicianOrderId);
        if (!templatePhysicianOrder) {
          return res.status(404).send({
            message: 'No standing order physician order found',
          });
        }
        return templatePhysicianOrder
          .update(req.body, { fields: Object.keys(req.body), })
          .then(
            () => res.status(200).send({ data: templatePhysicianOrder, }),
            (err) => res.status(400).send({
              name: err.name,
              message: err.message,
            })
          );
      }, (err) => res.status(400).send({
        name: err.name,
        message: err.message,
      }));
  },
};

export default TemplatePhysicianOrderController;
