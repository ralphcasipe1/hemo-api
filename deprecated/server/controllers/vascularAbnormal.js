import db from '../models';

const { Vascular, } = db.sequelize.models;

const VascularAbnormalController = {
  setVascularAbnormals(req, res) {
    return Vascular
      .findById(req.params.vascularId)
      .then((vascular) => {
        if (!vascular) {
          return res.status(404).send({
            message: 'No vascular found',
          });
        }
        return vascular
          .setAbnormals(req.body.abnormal_id, {
            through: {
              created_by: req.user.user_id,
              updated_by: req.user.user_id,
            },
          })
          .then(() => Vascular
            .findById(req.params.vascularId)
            .then(() => vascular.getAbnormals()
              .then(() => res.status(201).send({
                data: vascular,
              }), (err) => res.status(400).send({
                message: err.message,
              })), (err) => res.status(400).send({
              message: err.message,
            })), (err) => res.status(400).send({
            message: err.message,
          }));
      }, (err) => res.status(400).send({
        message: err.message,
      }));
  },

  listVascularAbnormals(req, res) {
    return Vascular
      .findById(req.params.vascularId)
      .then((vascular) => {
        if (!vascular) {
          return res.status(404).send({
            message: 'No vascular found',
          });
        }
        return vascular
          .getAbnormals()
          .then((data) => res.status(201).send({
            data,
          }), (err) => res.status(400).send(err));
      });
  },

  selectVascularAbnormal(req, res) {
    return Vascular
      .findById(req.params.vascularId)
      .then((vascular) => {
        if (!vascular) {
          return res.status(404).send({
            message: 'No vascular found',
          });
        }
        return vascular
          .getAbnormals({
            where: {
              abnormal_id: req.params.abnormalId,
            },
          })
          .then((data) => res.status(201).send({
            data,
          }), (err) => res.status(400).send(err));
      });
  },

  removeVascularAbnormal(req, res) {
    return Vascular
      .findById(req.params.vascularId)
      .then((vascular) => {
        if (!vascular) {
          return res.status(404).send({
            message: 'No vascular found',
          });
        }
        return vascular
          .setAbnormals([])
          .then((data) => res.status(201).send({
            data,
          }), (err) => res.status(400).send(err));
      });
  },
};

export default VascularAbnormalController;

