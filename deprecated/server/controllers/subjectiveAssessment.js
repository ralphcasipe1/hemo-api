import db from '../models';

const { PatientRegistry, } = db.sequelize.models;

const SubjectiveAssessmentController = {
  setSubjectives(req, res) {
    return PatientRegistry
      .findById(req.params.patientRegistryId)
      .then((patientRegistry) => {
        if (!patientRegistry) {
          return res.status(404).send({
            message: 'No patient registry found',
          });
        }
        return patientRegistry
          .setSubjectives(req.body.subjective_id, {
            through: {
              created_by: req.user.user_id,
              updated_by: req.user.user_id,
            },
          })
          .then(
            () => PatientRegistry
              .findById(req.params.patientRegistryId)
              .then(() => {
                if (!patientRegistry) {
                  return res.status(404).send({
                    message: 'No patient registry found',
                  });
                }
                return patientRegistry
                  .getSubjectives()
                  .then((data) => res.status(201).send({
                    data,
                  }), (err) => res.status(400).send({
                    message: err.message,
                  }));
              }, (err) => res.status(400).send({
                message: err.message,
              })),
            /* data => res.status(201).send({
                              data: data.reduce((nest, data) => nest.concat(data))
                          }), */
            (err) => res.status(400).send({
              message: err.message,
            })
          );
      }, (err) => res.status(400).send(err));
  },

  listSubjectives(req, res) {
    return PatientRegistry
      .findById(req.params.patientRegistryId)
      .then((patientRegistry) => {
        if (!patientRegistry) {
          return res.status(404).send({
            message: 'No patient registry found',
          });
        }
        return patientRegistry
          .getSubjectives()
          .then((data) => res.status(201).send({
            data,
          }), (err) => res.status(400).send(err));
      });
  },

  selectSubjective(req, res) {
    return PatientRegistry
      .findById(req.params.patientRegistryId)
      .then((patientRegistry) => {
        if (!patientRegistry) {
          return res.status(404).send({
            message: 'No patient registry found',
          });
        }
        return patientRegistry
          .getSubjective({
            where: {
              subjective_id: req.params.subjectiveId,
            },
          })
          .then((data) => res.status(201).send({
            data,
          }), (err) => res.status(400).send(err));
      });
  },

  removeSubjectives(req, res) {
    return PatientRegistry
      .findById(req.params.patientRegistryId)
      .then((patientRegistry) => {
        if (!patientRegistry) {
          return res.status(404).send({
            message: 'No patient registry found',
          });
        }
        return patientRegistry
          .setSubjectives([])
          .then((data) => res.status(201).send({
            data: data.reduce((nest) => nest.concat(data)),
          }), (err) => res.status(400).send(err));
      });
  },
};

export default SubjectiveAssessmentController;
