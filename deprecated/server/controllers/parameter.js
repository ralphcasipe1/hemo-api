import db from '../models';

const { Parameter, PatientRegistry, } = db.sequelize.models;

const ParameterController = {
  create(req, res) {
    return PatientRegistry
      .findById(req.params.patientRegistryId)
      .then((patientRegistry) => {
        if (!patientRegistry) {
          return res.status(404).send({
            message: 'No patient registry found',
          });
        }
        return patientRegistry
          .createParameter({
            patient_registry_id: req.params.patientRegistryId,
            duration: req.body.duration,
            blood_flow_rate: req.body.blood_flow_rate,
            dialysate_bath: req.body.dialysate_bath,
            dialysate_additive: req.body.dialysate_additive,
            dialysate_flow_rate: req.body.dialysate_flow_rate,
            ufv_goal: req.body.ufv_goal,
            created_by: req.user.user_id,
            updated_by: req.user.user_id,
          })
          .then((parameter) => res.status(201)
            .send({
              data: parameter,
            }), (err) => res.status(400).send({
            name: err.name,
            message: err.message,
            detail: err.parent.detail,
          }));
      });
  },

  destroy(req, res) {
    return Parameter
      .findById(req.params.parameterId)
      .then((parameter) => {
        if (!parameter) {
          return res.status(400).send({
            message: 'No parameter found',
          });
        }
        return parameter
          .destroy()
          .then(() => res.status(200).send({
            message: 'Parameter successfully deleted',
          }), (err) => res.status(400).send({
            message: err.message,
          }));
      });
  },

  get(request, response) {
    return PatientRegistry
      .findById(request.params.patientRegistryId)
      .then((patientRegistry) => {
        if (!patientRegistry) {
          return response.status(404).send({
            message: 'No patient registry found',
          });
        }
        return patientRegistry
          .getParameter()
          .then((parameter) => response.status(200)
            .send({
              data: parameter,
            }), (err) => response.status(400).send({
            message: err.message,
          }));
      });
  },

  select(request, res) {
    return Parameter
      .findById(request.params.parameterId)
      .then((parameter) => res.status(200)
        .send({
          data: parameter,
        }), (err) => res.status(400).send({
        message: err.message,
      }));
  },

  update(req, res) {
    return Parameter
      .findById(req.params.parameterId)
      .then((parameter) => {
        if (!parameter) {
          return res.status(404).send({
            message: 'No parameter found',
          });
        }
        return parameter
          .update(req.body, { fields: Object.keys(req.body), })
          .then(() => res.status(200).send({ data: parameter, }), (err) => res.status(400).send({
            message: err,
          }));
      }, (err) => res.status(400).send({
        message: err,
      }));
  },
};

export default ParameterController;
