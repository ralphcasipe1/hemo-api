import Sequelize from 'sequelize';

let sequelize;
if (process.env.NODE_ENV === 'development') {
  sequelize = new Sequelize('bizbox8', 'sa', 's@password1', {
    host: '192.168.70.83',
    dialect: 'mssql',
  });
} else if (process.env.NODE_ENV === 'test') {
  sequelize = new Sequelize('bizbox8', 'sa', 's@password1', {
    host: '192.168.70.83',
    dialect: 'mssql',
  });
} else if (process.env.NODE_ENV === 'production') {
  sequelize = new Sequelize('LiveDB_bizbox8', 'sa', 's@password1', {
    host: '192.168.7.2',
    dialect: 'mssql',
    dialectOptions: {
      encrypt: true,
    },
  });
}

const connection = sequelize;

export default connection;
