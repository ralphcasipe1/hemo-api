import db from '../models';

const { PatientRegistry, } = db.sequelize.models;

const ObjectiveAssessmentController = {
  setObjectives(req, res) {
    return PatientRegistry
      .findById(req.params.patientRegistryId)
      .then((patientRegistry) => {
        if (!patientRegistry) {
          return res.status(404).send({
            message: 'No patient registry found',
          });
        }
        return patientRegistry
          .setObjectives(req.body.objective_id, {
            through: {
              created_by: req.user.user_id,
              updated_by: req.user.user_id,
            },
          })
          .then(
            () => PatientRegistry
              .findById(req.params.patientRegistryId)
              .then(() => {
                if (!patientRegistry) {
                  return res.status(404)
                    .send({
                      message: 'No patient registry found',
                    });
                }
                return patientRegistry
                  .getObjectives()
                  .then((data) => res.status(201).send({
                    data,
                  }), (err) => res.status(400).send(err));
              }, (err) => res.status(400).send(err))
            /* data => res.status(201).send({
                            data: data.reduce((nest, data) => nest.concat(data))
                        }) */, (err) => res.status(400).send(err)
          );
      }, (err) => res.status(400).send(err));
  },

  listObjectives(req, res) {
    return PatientRegistry
      .findById(req.params.patientRegistryId)
      .then((patientRegistry) => {
        if (!patientRegistry) {
          return res.status(404).send({
            message: 'No patient registry found',
          });
        }
        return patientRegistry
          .getObjectives()
          .then((data) => res.status(201).send({
            data,
          }), (err) => res.status(400).send({
            message: err.message,
          }));
      });
  },

  selectObjective(req, res) {
    return PatientRegistry
      .findById(req.params.patientRegistryId)
      .then((patientRegistry) => {
        if (!patientRegistry) {
          return res.status(404).send({
            message: 'No patient registry found',
          });
        }
        return patientRegistry
          .getObjectives({
            where: {
              objective_id: req.params.objectiveId,
            },
          })
          .then((data) => res.status(201).send({
            data,
          }), (err) => res.status(400).send(err));
      });
  },

  removeObjectives(req, res) {
    return PatientRegistry
      .findById(req.params.patientRegistryId)
      .then((patientRegistry) => {
        if (!patientRegistry) {
          return res.status(404).send({
            message: 'No patient registry found',
          });
        }
        return patientRegistry
          .setObjectives([])
          .then((data) => res.status(201).send({
            data: data.reduce((nest) => nest.concat(data)),
          }), (err) => res.status(400).send(err));
      });
  },
};

export default ObjectiveAssessmentController;
