import sequelize from './bizboxConn';

const BizboxPhysicianController = {
  list(req, res) {
    return sequelize
      .query(
        `
          SELECT PK_emdDoctors,
              t1.pmccno,
              dbo.udf_GetFullName(PK_emdDoctors) as name,
              t2.description as specialization,
              t3.firstname,
              t3.lastname,
              t3.birthdate
          FROM emdDoctors t1
          LEFT JOIN emdTempSpecializations t2
          ON t1.FK_emdTempSpecializations = t2.PK_emdTempSpecializations
          LEFT JOIN psPersonaldata t3
          ON t1.PK_emdDoctors = t3.PK_psPersonalData
          WHERE t1.active = 1
          ORDER BY name
        `,
        {
          type: sequelize.QueryTypes.SELECT,
        }
      )
      .then(
        (data) => res.status(200).send({ data, }),
        (err) => res.status(400).send({ message: err.message, })
      );
  },

  select(req, res) {
    return sequelize
      .query(
        `
          SELECT PK_emdDoctors,
              t1.pmccno,
              t2.description as specialization,
              t3.firstname,
              t3.lastname,
              t3.birthdate
          FROM emdDoctors t1
          LEFT JOIN emdTempSpecializations t2
          ON t1.FK_emdTempSpecializations = t2.PK_emdTempSpecializations
          LEFT JOIN psPersonaldata t3
          ON t1.PK_emdDoctors = t3.PK_psPersonalData
          WHERE   PK_emdDoctors= :physicianId
          ORDER BY t3.lastname
        `,
        {
          replacements: {
            physicianId: req.params.physicianId,
          },
          type: sequelize.QueryTypes.SELECT,
        }
      )
      .then(
        (physician) =>
          res.status(200).send({
            data: physician.length === 0 ?
              {} :
              physician.reducer((data) => data),
          }),
        (err) =>
          res.status(400).send({
            message: err.message,
          })
      );
  },
};

export default BizboxPhysicianController;
// Fake Connection
/* const sequelize = new Sequelize('his', 'postgres', 'test098!?', {
    host: 'localhost',
    dialect: 'postgres'
}) */

// Fake Controller
/* module.exports = {
    list(req, res) {
        sequelize.query(`
            SELECT *
            FROM physicians
        `, {
            type: sequelize.QueryTypes.SELECT
        })
            .then(
                physicians => res.status(200).send({
                    data: physicians
                }),
                err => res.status(400).send({
                    message: err.mesage
                })
        )
},

select(req, res) {
    return sequelize.query(`
        SELECT  pk_emd_doctors,
                name,
                specialization
        FROM physicians
        WHERE pk_emd_doctors = ${req.params.physicianId}
    `, {
        type: sequelize.QueryTypes.SELECT
    })
        .spread(
            physician => res.status(200).send({
                data: physician
            }),
            err => res.status(400).send({
                message: err.message
            })
        )
}
} */
