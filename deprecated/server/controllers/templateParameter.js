import db from '../models';

const { TemplateParameter, } = db.sequelize.models;

const TemplateParameterController = {
  create(request, response) {
    return TemplateParameter
      .create({
        patient_id: request.params.patientId,
        duration: request.body.duration,
        blood_flow_rate: request.body.blood_flow_rate,
        dialysate_bath: request.body.dialysate_bath,
        dialysate_additive: request.body.dialysate_additive,
        dialysate_flow_rate: request.body.dialysate_flow_rate,
        created_by: request.user.user_id,
        updated_by: request.user.user_id,
      })
      .then((templateParameter) => response.status(201)
        .send({
          data: templateParameter,
        }), (err) => response.status(400).send({
        name: error.name,
        message: error.message,
      }));
  },

  destroy(req, res) {
    return TemplateParameter
      .findById(req.params.templateParameterId)
      .then((templateParameter) => {
        if (!templateParameter) {
          return res.status(400).send({
            message: 'No standing order parameter found',
          });
        }
        return templateParameter
          .destroy()
          .then(() => res.status(200).send({
            message: 'Standing order parameter deleted successfully',
          }), (err) => res.status(400).send({
            message: err.message,
          }));
      });
  },

  findAll(request, response) {
    return TemplateParameter
      .findAll({
        where: {
          patient_id: request.params.patientId,
        },
      })
      .then(
        (standingOrderParameters) => response
          .status(200)
          .send({
            data: standingOrderParameters,
          }),
        (error) => response
          .status(400)
          .send({
            message: error.message,
          })
      );
  },

  findOne(req, res) {
    return TemplateParameter
      .findById(req.params.templateParameterId)
      .then((templateParameter) => res.status(200)
        .send({
          data: templateParameter,
        }), (err) => res.status(400).send({
        message: err.message,
      }));
  },

  update(req, res) {
    return TemplateParameter
      .findById(req.params.templateParameterId)
      .then((templateParameter) => {
        if (!templateParameter) {
          return res.status(404).send({
            message: 'No standing order parameter found',
          });
        }
        return templateParameter
          .update(req.body, { fields: Object.keys(req.body), })
          .then(
            () => res.status(200).send({ data: templateParameter, }),
            (err) => res.status(400).send({
              name: err.name,
              message: err.message,
            })
          );
      }, (err) => res.status(400).send({
        name: err.name,
        message: err.message,
      }));
  },
};

export default TemplateParameterController;
