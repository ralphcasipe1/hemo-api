import db from '../models';

const { Subjective, } = db.sequelize.models;

const SubjectiveController = {
  create(req, res) {
    return Subjective.create({
      name: req.body.name,
      description: req.body.description,
      created_by: req.user.user_id,
      updated_by: req.user.user_id,
    }).then((subjective) => res.status(201).send({
      data: subjective,
    }), (err) => res.status(400).send({
      status: err.status,
      message: err.message,
    }));
  },

  destroy(req, res) {
    return Subjective.findById(req.params.subjectiveId).then((subjective) => {
      if (!subjective) {
        return res.status(400).send({
          message: 'No subjective assessment found',
        });
      }
      return subjective.destroy().then(() => res.status(200).send({
        data: subjective,
        message: 'Subjective assessment deleted successfully',
      }), (err) => res.status(400).send({
        message: err.message,
      }));
    });
  },

  list(req, res) {
    return Subjective.all({
      limit: 15,
    }).then((subjectives) => res.status(200).send({
      data: subjectives,
    }), (err) => res.status(400).send({
      message: err.message,
    }));
  },

  select(req, res) {
    return Subjective.findById(req.params.subjectiveId).then((subjective) => res.status(200).send({
      data: subjective,
    }), (err) => res.status(400).send({
      message: err.message,
    }));
  },

  update(req, res) {
    return Subjective.findById(req.params.subjectiveId).then((subjective) => {
      if (!subjective) {
        return res.status(404).send({
          message: 'No subjective assessment found',
        });
      }
      return subjective
        .update(req.body, { fields: Object.keys(req.body), })
        .then(() => res.status(200).send({
          data: subjective,
        }), (err) => res.status(400).send({
          message: err.message,
        }));
    }, (err) => res.status(400).send({
      message: err.message,
    }));
  },
};

export default SubjectiveController;
