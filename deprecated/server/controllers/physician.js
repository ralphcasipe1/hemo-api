import db from '../models';

const { Physician, } = db.sequelize.models;

const PhysicianController = {
  create(req, res) {
    return Physician.create({
      bizbox_physician_no: req.body.bizbox_physician_no,
      fname: req.body.fname,
      lname: req.body.lname,
      date_of_birth: req.body.date_of_birth,
      specialization: req.body.specialization,
    }).then((physicians) => res.status(201).send({
      data: physicians,
    }), (err) => res.status(400).send({
      status: err.status,
      message: err.message,
    }));
  },

  list(req, res) {
    return Physician.all({
      limit: 15,
    }).then((physician) => res.status(200).send({
      data: physician,
    }), (err) => res.status(400).send({
      message: err.message,
    }));
  },
};

export default PhysicianController;
