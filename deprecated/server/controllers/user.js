import bcrypt from 'bcrypt';
import _ from 'lodash';
import db from '../models';

const { User, } = db.sequelize.models;

const UserController = {
  create(request, response) {
    const saltRounds = 10;
    const password = request.body.password;
    const hashPassword = bcrypt.genSalt(saltRounds, (error, salt) => 
      bcrypt.hash(password, salt, (error, hash) => hash)
    );
    return User.create({
      fname: request.body.fname,
      mname: request.body.mname,
      lname: request.body.lname,
      id_number: request.body.id_number,
      password: hashPassword,
      password: request.body.password || _.lowerFirst(request.body.id_number),
      is_admin: request.body.is_admin,
      is_doctor: request.body.is_doctor,
    }).then((user) => User.findById(user.user_id, {
      attributes: [
        'user_id',
        'fname',
        'mname',
        'lname',
        'id_number',
        'password',
        'is_admin',
        'is_doctor',
        'created_at',
        'updated_at',
      ],
    }).then(() => response.status(201).send({
      message: 'User created successfully',
      data: user,
    }), (error) => response.status(400).send({
      message: error.message,
    })), (error) => response.status(400).send({
      message: error.message,
    }));
  },

  destroy(req, res) {
    return User.findById(req.params.userId).then((user) => {
      if (!user) {
        return res.status(400).send({
          message: 'No user found',
        });
      }
      return user.destroy().then(() => res.status(200).send({
        message: 'User deleted successfully',
        data: user,
      }), (err) => res.status(400).send({
        message: err.message,
      }));
    });
  },

  list(req, res) {
    const limit = 10;
    return User.findAll({
      limit,
      order: [
        [
          'created_by',
          'DESC',
        ],
      ],
      attributes: [
        'user_id',
        'fname',
        'mname',
        'lname',
        'id_number',
        'password',
        'is_admin',
        'is_doctor',
        'created_at',
        'updated_at',
      ],
    }).then((users) => {
      if (users.length === 0) {
        return res.status(200).send({
          message: 'No users found',
          data: [],
        });
      }
      return res.status(200).send({
        data: users,
        meta: {
          currentPage: 1,
          nextPage: 2,
          limit,
          offset: 0,
        },
      });
    }, (err) => res.status(400).send({
      message: err.message,
    }));
  },

  select(req, res) {
    return User.findById(req.params.userId, {
      attributes: [
        'user_id',
        'fname',
        'mname',
        'lname',
        'id_number',
        'password',
        'is_admin',
        'is_doctor',
        'created_at',
        'updated_at',
      ],
    }).then((user) => {
      if (!user) {
        return res.status(404).send({
          message: 'No user found',
        });
      }
      return res.status(200).send({
        data: user,
      });
    }, (err) => res.status(404).send({
      message: err.message,
    }));
  },

  update(req, res) {
    return User.findById(req.params.userId).then((user) => {
      if (!user) {
        return res.status(404).send({
          message: 'No user found',
        });
      }
      return user
        .update(
          req.body,
          { fields: Object.keys(req.body), }
        ).then(() => res.status(200).send({
          message: 'User updated successfully',
          data: user,
        }), (err) => res.status(400).send({
          message: err.message,
        }));
    }, (err) => res.status(400).send({
      message: err.message,
    }));
  },

  viewProfile(req, res) {
    return User.findById(req.user.user_id, {
      attributes: [
        'fname',
        'mname',
        'lname',
        'id_number',
      ],
    }).then((profile) => res.status(200).send({
      data: profile,
    }), (err) => res.status(400).send({
      message: err.message,
    }));
  },

  userRequired(req, res, next) {
    if (req.user) {
      return next();
    }
    return res.status(401).send({
      message: 'Unauthorized user!',
    });
  },
};

export default UserController;

