import db from '../models';

const { Medication, User, } = db.sequelize.models;

const MedicationController = {
  create(req, res) {
    return Medication.create({
      patient_registry_id: req.params.patientRegistryId,
      generic: req.body.generic,
      brand: req.body.brand,
      preparation: req.body.preparation,
      dosage: req.body.dosage,
      route: req.body.route,
      created_by: req.user.user_id,
      updated_by: req.user.user_id,
    }).then((medication) => {
      Medication.findById(medication.medication_id, {
        include: [
          {
            model: User,
            as: 'createdBy',
            attributes: [
              'fname',
              'mname',
              'lname',
              'id_number',
            ],
          },
        ],
        attributes: [
          'medication_id',
          'patient_registry_id',
          'generic',
          'brand',
          'preparation',
          'dosage',
          'route',
          'created_at',
          'updated_at',
          'deleted_at',
          'created_by',
          'updated_by',
        ],
      }).then(() => res.status(200).send({
        data: medication,
      }), (err) => res.status(400).send({
        name: err.name,
        message: err.message,
        detail: err.parent.detail,
      }));
    }, (err) => res.status(400).send({
      name: err.name,
      message: err.message,
      detail: err.parent.detail,
    }));
  },

  destroy(req, res) {
    return Medication.findById(req.params.medicationId).then((medication) => {
      if (!medication) {
        return res.status(400).send({
          message: 'No medication found',
        });
      }
      return medication.destroy().then(() => res.status(200).send({
        data: medication,
        message: 'Medication deleted successfully',
      }), (err) => res.status(400).send({
        message: err.message,
      }));
    });
  },

  findAll(req, res) {
    return Medication.findAll({
      where: {
        patient_registry_id: req.params.patientRegistryId,
      },
      include: [
        {
          model: User,
          as: 'createdBy',
          attributes: [
            'fname',
            'mname',
            'lname',
            'id_number',
          ],
        },
      ],
    })
      .then(
        (medications) => res
          .status(200)
          .send({
            data: medications,
          }),
        (err) => res
          .status(400)
          .send({
            message: err.message,
          })
      );
  },

  findOne(req, res) {
    return Medication.findById(req.params.medicationId).then((medication) => res.status(200).send({
      data: medication,
    }), (err) => res.status(400).send({
      message: err.message,
    }));
  },

  update(req, res) {
    return Medication.findById(req.params.medicationId).then((medication) => {
      if (!medication) {
        return res.status(404).send({
          message: 'No medication found',
        });
      }
      return medication
        .update(req.body, { fields: Object.keys(req.body), })
        .then(() => res.status(200).send({ data: medication, }), (err) => res.status(400).send({
          message: err,
        }));
    }, (err) => res.status(400).send({
      message: err,
    }));
  },
};

export default MedicationController;

