import sequelize from './bizboxConn';

export default function (req, res) {
  return sequelize
    .query(
      `
      SELECT  PK_psPatRegisters,
              registrydate
      FROM    psPatRegisters 
      WHERE   pattrantype = 'O'
      AND     FK_emdPatients = ${req.params.patientId}
      ORDER BY registrydate
      `,
      {
        type: sequelize.QueryTypes.SELECT,
      }
    )
    .then(
      (patientRegistries) =>
        res.status(200).send({
          data: patientRegistries,
        }),
      (err) =>
        res.status(400).send({
          message: err.nessage,
        })
    );
}

