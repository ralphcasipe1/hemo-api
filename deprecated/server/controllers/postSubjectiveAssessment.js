import db from '../models';

const { PatientRegistry, } = db.sequelize.models;

const PostSubjectiveAssessmentController = {
  setPostSubjectives(req, res) {
    return PatientRegistry
      .findById(req.params.patientRegistryId)
      .then((patientRegistry) => {
        if (!patientRegistry) {
          return res.status(404).send({
            message: 'No patient registry found',
          });
        }
        return patientRegistry
          .setPostSubjectives(req.body.subjective_id, {
            through: {
              created_by: req.user.user_id,
              updated_by: req.user.user_id,
            },
          })
          .then(
            () => PatientRegistry
              .findById(req.params.patientRegistryId)
              .then(() => {
                if (!patientRegistry) {
                  return res.status(404).send({
                    message: 'No patient registry found',
                  });
                }
                return patientRegistry
                  .getPostSubjectives()
                  .then((data) => res.status(201).send({
                    data,
                  }), (err) => res.status(400).send({
                    message: err.message,
                  }));
              }),
            (err) => res.status(400).send({
              message: err.message,
            })
          );
      }, (err) => res.status(400).send(err));
  },

  listPostSubjectives(req, res) {
    return PatientRegistry
      .findById(req.params.patientRegistryId)
      .then((patientRegistry) => {
        if (!patientRegistry) {
          return res.status(404).send({
            message: 'No patient registry found',
          });
        }
        return patientRegistry
          .getPostSubjectives()
          .then((data) => res.status(201).send({
            data,
          }), (err) => res.status(400).send(err));
      });
  },

  selectPostSubjective(req, res) {
    return PatientRegistry
      .findById(req.params.patientRegistryId)
      .then((patientRegistry) => {
        if (!patientRegistry) {
          return res.status(404).send({
            message: 'No patient registry found',
          });
        }
        return patientRegistry
          .getPostSubjective({
            where: {
              subjective_id: req.params.subjectiveId,
            },
          })
          .then((data) => res.status(201).send({
            data,
          }), (err) => res.status(400).send(err));
      });
  },

  removePostSubjectives(req, res) {
    return PatientRegistry
      .findById(req.params.patientRegistryId)
      .then((patientRegistry) => {
        if (!patientRegistry) {
          return res.status(404).send({
            message: 'No patient registry found',
          });
        }
        return patientRegistry
          .setPostSubjectives([])
          .then((data) => res.status(201).send({
            data: data.reduce((nest) => nest.concat(data)),
          }), (err) => res.status(400).send(err));
      });
  },
};

export default PostSubjectiveAssessmentController;
