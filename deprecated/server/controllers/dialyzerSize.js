import db from '../models';

const { DialyzerSize, } = db.sequelize.models;

const DialyzerSizeController = {
  create(req, res) {
    return DialyzerSize.create({
      size_name: req.body.size_name,
      description: req.body.description,
      created_by: req.user.user_id,
      updated_by: req.user.user_id,
    }).then((dialyzerSize) => res.status(201).send({
      data: dialyzerSize,
    }), (err) => res.status(400).send({
      status: err.status,
      message: err.message,
    }));
  },

  destroy(req, res) {
    return DialyzerSize.findById(req.params.dialyzerSizeId).then((dialyzerSize) => {
      if (!dialyzerSize) {
        return res.status(400).send({
          message: 'No dialyzer size found',
        });
      }
      return dialyzerSize.destroy().then(() => res.status(200).send({
        message: 'Dialyzer size deleted successfully',
        data: dialyzerSize,
      }), (err) => res.status(400).send({
        message: err.message,
      }));
    });
  },

  list(req, res) {
    return DialyzerSize.all({
      limit: 15,
      order: [
        [
          'created_at',
          'ASC',
        ],
      ],
    }).then((dialyzerSizes) => res.status(200).send({
      data: dialyzerSizes,
    }), (err) => res.status(400).send({
      message: err.message,
    }));
  },

  select(req, res) {
    return DialyzerSize
      .findById(req.params.dialyzerSizeId)
      .then(
        (dialyzerSize) => res.status(200).send({
          data: dialyzerSize,
        }),
        (err) => res.status(400).send({
          message: err.message,
        })
      );
  },

  update(req, res) {
    return DialyzerSize.findById(req.params.dialyzerSizeId).then((dialyzerSize) => {
      if (!dialyzerSize) {
        return res.status(404).send({
          message: 'No dialyzer size found',
        });
      }
      return dialyzerSize
        .update(req.body, { fields: Object.keys(req.body), })
        .then(() => res.status(200).send({
          data: dialyzerSize,
        }), (err) => res.status(400).send({
          message: err.message,
        }));
    }, (err) => res.status(400).send({
      message: err.message,
    }));
  },
};

export default DialyzerSizeController;

