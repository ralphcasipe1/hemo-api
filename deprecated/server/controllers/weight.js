import db from '../models';

const { PatientRegistry, Weight, } = db.sequelize.models;

const WeightController = {
  create(req, res) {
    return PatientRegistry
      .findById(req.params.patientRegistryId)
      .then((patientRegistry) => {
        if (!patientRegistry) {
          return res.status(404).send({
            message: 'No patient registry found',
          });
        }
        return patientRegistry
          .createWeight({
            ppatient_registry_id: req.params.patientRegistryId,
            last_post_hd_wt: req.body.last_post_hd_wt,
            pre_hd_weight: req.body.pre_hd_weight,
            inter_hd_wt_gain: req.body.pre_hd_weight - req.body.dry_weight,
            post_hd_weight: req.body.post_hd_weight,
            net_fluid_removed: req.body.net_fluid_removed,
            dry_weight: req.body.dry_weight,
            created_by: req.user.user_id,
            updated_by: req.user.user_id,
          })
          .then((weight) => res.status(201)
            .send({
              data: weight,
            }), (err) => res.status(400).send({
            name: err.name,
            message: err.message,
            detail: err.parent.detail,
          }));
      });
  },

  destroy(req, res) {
    return Weight
      .findById(req.params.weightId)
      .then((weight) => {
        if (!weight) {
          return res.status(400).send({
            message: 'No weight found',
          });
        }
        return weight
          .destroy()
          .then(() => res.status(200).send({
            message: 'Weight deleted successfully',
          }), (err) => res.status(400).send({
            message: err.message,
          }));
      });
  },

  get(req, res) {
    return PatientRegistry
      .findById(req.params.patientRegistryId)
      .then((patientRegistry) => {
        if (!patientRegistry) {
          return res.status(404).send({
            message: 'No patient registry found',
          });
        }
        return patientRegistry
          .getWeight()
          .then((weight) => res.status(200)
            .send({
              data: weight,
            }), (err) => res.status(400).send({
            message: err.message,
          }));
      });
  },

  select(req, res) {
    return Weight
      .findById(req.params.weightId)
      .then((weight) => res.status(200)
        .send({
          data: weight,
        }), (err) => res.status(400).send({
        message: err.message,
      }));
  },

  update(req, res) {
    return Weight
      .findById(req.params.weightId)
      .then((weight) => {
        if (!weight) {
          return res.status(404).send({
            message: 'No weight found',
          });
        }
        return weight
          .update(req.body, { fields: Object.keys(req.body), })
          .then(() => res.status(200).send({ data: weight, }), (err) => res.status(400).send({
            message: err.message,
          }));
      }, (err) => res.status(400).send({
        message: err.message,
      }));
  },
};

export default WeightController;
