import _ from 'lodash';
import db from '../models';

const { Abnormal, PatientRegistry, Vascular, } = db.sequelize.models;

const VascularController = {
  create(req, res) {
    return PatientRegistry
      .findById(req.params.patientRegistryId)
      .then((patientRegistry) => {
        if (!patientRegistry) {
          return res.status(404).send({
            message: 'No patient registry found',
          });
        }
        return patientRegistry
          .createVascular({
            patient_registry_id: req.params.patientRegistryId,
            type: req.body.type,
            operation_date: req.body.operation_date,
            surgeon: req.body.surgeon,
            location: req.body.location,
            arterial_location: req.body.arterial_location,
            arterial_needle: req.body.arterial_needle,
            venous_needle: req.body.venous_needle,
            is_bruit: req.body.venous_needle,
            is_thrill: req.body.is_thrill,
            created_by: req.user.user_id,
            updated_by: req.user.user_id,
          })
          .then((vascular) => {
            if (!_.isEmpty(req.body.abnormal_id)) {
              return Vascular
                .findOne({
                  where: {
                    patient_registry_id: req.params.patientRegistryId,
                  },
                })
                .then(() => {
                  if (!vascular) {
                    return res.status(404).send({
                      message: 'No vascular found',
                    });
                  }
                  return vascular
                    .setAbnormals(req.body.abnormal_id, {
                      through: {
                        created_by: req.user.user_id,
                        updated_by: req.user.user_id,
                      },
                    })
                    .then(() => Vascular
                      .findOne({
                        where: {
                          patient_registry_id: req.params.patientRegistryId,
                        },
                        include: [
                          {
                            model: Abnormal,
                          },
                        ],
                      })
                      .then(() => res.status(201).send({
                        data: vascular,
                      }), (err) => res.status(400).send({
                        message: err.message,
                      })), (err) => res.status(400).send({
                      message: err.message,
                    }));
                }, (err) => res.status(400).send({
                  message: err.message,
                }));
            }
            return res.status(201).send({
              data: vascular,
            });
          }, (err) => res.status(400).send({
            name: err.name,
            message: err.message,
            detail: err.parent.detail,
          }));
      });
  },

  destroy(req, res) {
    return Vascular
      .findById(req.params.vascularId)
      .then((vascular) => {
        if (!vascular) {
          return res.status(400).send({
            message: 'No vascular data found',
          });
        }
        return vascular
          .destroy()
          .then(() => res.status(200).send({
            message: 'Vascular data deleted successfully',
          }), (err) => res.status(400).send({
            message: err.message,
          }));
      });
  },

  get(req, res) {
    return Vascular
      .findOne({
        where: {
          patient_registry_id: req.params.patientRegistryId,
        },
        include: [
          {
            model: Abnormal,
          },
        ],
      })
      .then((vascular) => res.status(200)
        .send({
          data: vascular,
        }), (err) => res.status(400)
        .send({
          message: err.message,
          details: err.details,
        }));
  },

  select(req, res) {
    return Vascular
      .findById(req.params.vascularId)
      .then((vascular) => res.status(200)
        .send({
          data: vascular,
        }), (err) => res.status(400).send({
        message: err.message,
      }));
  },

  update(req, res) {
    return Vascular
      .findById(req.params.vascularId)
      .then((vascular) => {
        if (!vascular) {
          return res.status(404).send({
            message: 'No vascular data found',
          });
        }
        return vascular
          .update(req.body, { fields: Object.keys(req.body), })
          .then(() => res.status(200).send({ data: vascular, }), (err) => res.status(400).send({
            message: err,
          }));
      }, (err) => res.status(400).send({
        message: err,
      }));
  },
};

export default VascularController;
