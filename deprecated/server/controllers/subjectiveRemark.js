import db from '../models';

const { PatientRegistry, SubjectiveRemark, } = db.sequelize.models;

const SubjectiveRemarkController = {
  create(req, res) {
    return PatientRegistry.findById(req.params.patientRegistryId).then((patientRegistry) => {
      if (!patientRegistry) {
        return res.status(400).send({
          message: 'No patient registry found',
        });
      }
      return patientRegistry
        .createSubjectiveRemark({
          remark: req.body.remark,
        })
        .then(
          (data) => res.status(201).send({ data, }),
          (err) => res.status(400).send({ message: err.message, })
        );
    }, (err) => res.status(400).send({
      message: err.message,
    }));
  },

  get(req, res) {
    return PatientRegistry.findById(req.params.patientRegistryId).then((patientRegistry) => {
      if (!patientRegistry) {
        return res.status(404).send({
          message: 'No patient registry found',
        });
      }
      return patientRegistry.getSubjectiveRemark().then((data) => res.status(200).send({
        data,
      }), (err) => res.status(400).send({
        message: err.message,
      }));
    });
  },

  destroy(req, res) {
    return SubjectiveRemark.findById(req.params.subjectiveRemarkId).then((remark) => {
      if (!remark) {
        return res.status(400).send({
          message: 'No remark found',
        });
      }
      return remark.destroy().then(() => res.status(200).send({
        data: remark,
        message: "Subjective assessment's remark successfully deleted",
      }), (err) => res.status(400).send({
        message: err.message,
      }));
    }, (err) => res.status(400).send({
      message: err.message,
    }));
  },
};

export default SubjectiveRemarkController;

