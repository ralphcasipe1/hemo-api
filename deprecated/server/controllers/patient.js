// import Sequelize from 'sequelize';
import db from '../models';

const {
  Patient,
  PatientRegistry,
  TemplateParameter,
  TemplateWeight,
  TemplateAntiCoagulant,
  TemplateDialyzer,
  TemplatePhysicianOrder,
  TemplateMedication,
} = db.sequelize.models;

const PatientController = {
  create(req, res) {
    return Patient.findOrCreate({
      where: {
        bizbox_patient_no: req.body.bizbox_patient_no.toString(),
      },
      defaults: {
        fname: req.body.fname,
        mname: req.body.mname,
        lname: req.body.lname,
        age: req.body.age,
        sex: req.body.sex,
        civil_status: req.body.civil_status,
        room_no: req.body.room_no,
        hospital_no: req.body.hospital_no,
        attending_physician: req.body.attending_physician,
        created_by: req.user.user_id,
        updated_by: req.user.user_id,
      },
    }).spread((patient, isCreate) => {
      if (isCreate) {
        return res.status(200).send({
          exist: false,
          data: patient,
        });
      }
      return res.status(201).send({
        message: 'Patient already exists',
        exist: true,
        data: patient,
      });
    }, (err) => res.status(400).send({
      status: err.status,
      message: err.message,
    }));
  },

  destroy(req, res) {
    return Patient.findById(req.params.patientId).then((patient) => {
      if (!patient) {
        return res.status(400).send({
          message: 'No patient found',
        });
      }
      return patient.destroy().then(() => res.status(200).send({
        message: 'Patient deleted successfully',
      }), (err) => res.status(400).send({
        message: err.message,
      }));
    });
  },

  list(req, res) {
    return Patient
      .findAll()
      .then(
        (patients) =>
          res
            .status(200)
            .send({
              data: patients,
            }),
        (err) =>
          res
            .status(400)
            .send({
              message: err.message,
            })
      );
  },

  select(req, res) {
    return Patient.findById(req.params.patientId, {
      include: [
        {
          model: PatientRegistry,
          as: 'patientRegistries',
        },
        {
          model: TemplateParameter,
          as: 'sOParameters',
        },
        {
          model: TemplateWeight,
          as: 'sOWeights',
        },
        {
          model: TemplateAntiCoagulant,
          as: 'sOAntiCoagulants',
        },
        {
          model: TemplateDialyzer,
          as: 'sODialyzers',
        },
        {
          model: TemplateMedication,
          as: 'sOMedications',
        },
        {
          model: TemplatePhysicianOrder,
          as: 'sOPhysicianOrders',
        },
      ],
    }).then((patient) => {
      if (!patient) {
        return res.status(404).send({
          message: 'No patient found',
        });
      }
      return res.status(200).send({
        data: patient,
      });
    }, (err) => res.status(400).send({
      message: err.message,
    }));
  },

  update(req, res) {
    return Patient.findById(req.params.patientId).then((patient) => {
      if (!patient) {
        return res.status(404).send({
          message: 'No patient found',
        });
      }
      return patient
        .update(
          req.body,
          { fields: Object.keys(req.body), }
        )
        .then(
          () => res.status(200).send({
            data: patient,
          }),
          (err) => res.status(400).send({
            message: err.message,
          })
        );
    }, (err) => res.status(400).send({
      message: err.message,
    }));
  },
};

export default PatientController;
