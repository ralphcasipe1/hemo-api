import db from '../models';

const { TemplateAntiCoagulant, } = db.sequelize.models;

const TemplateAntiCoagulantController = {
  create(req, res) {
    return TemplateAntiCoagulant
      .create({
        patient_id: req.params.patientId,
        nss_flushing: req.body.nss_flushing,
        nss_flushing_every: req.body.nss_flushing_every,
        lmwh_iv: req.body.lmwh_iv,
        lmwh_iv_iu: req.body.lmwh_iv_iu,
        ufh_iv: req.body.ufh_iv,
        ufh_iv_iu: req.body.ufh_iv_iu,
        ufh_iv_iu_every: req.body.ufh_iv_iu_every,
        bleeding_desc: req.body.bleeding_desc,
        created_by: req.user.user_id,
        updated_by: req.user.user_id,
      })
      .then((templateAntiCoagulant) => res.status(201)
        .send({
          data: templateAntiCoagulant,
        }), (err) => res.status(400).send({
        name: err.name,
        message: err.message,
        detail: err.parent.detail,
      }));
  },

  destroy(req, res) {
    return TemplateAntiCoagulant
      .findById(req.params.templateAntiCoagulantId)
      .then((templateAntiCoagulant) => {
        if (!templateAntiCoagulant) {
          return res.status(400).send({
            message: 'No standing order anti-coagulant found',
          });
        }
        return templateAntiCoagulant
          .destroy()
          .then(() => res.status(200).send({
            message: 'Standing order anti-coagulant deleted successfully',
          }), (err) => res.status(400).send({
            message: err.message,
          }));
      });
  },

  findAll(req, res) {
    return TemplateAntiCoagulant
      .findAll({
        where: {
          patient_id: req.params.patientId,
        },
      })
      .then(
        (sOAntiCoagulants) => res
          .status(200)
          .send({
            data: sOAntiCoagulants,
          }),
        (err) => res
          .status(400)
          .send({
            message: err.message,
          })
      );
  },

  findOne(req, res) {
    return TemplateAntiCoagulant
      .findById(req.params.templateAntiCoagulantId)
      .then((templateAntiCoagulant) => res.status(200)
        .send({
          data: templateAntiCoagulant,
        }), (err) => res.status(400).send({
        message: err.message,
      }));
  },

  update(req, res) {
    return TemplateAntiCoagulant
      .findById(req.params.templateAntiCoagulantId)
      .then((templateAntiCoagulant) => {
        if (!templateAntiCoagulant) {
          return res.status(404).send({
            message: 'No standing order anti-coagulant found',
          });
        }
        return templateAntiCoagulant
          .update(req.body, { fields: Object.keys(req.body), })
          .then(
            () => res.status(200).send({ data: templateAntiCoagulant, }),
            (err) => res.status(400).send({
              message: err,
            })
          );
      }, (err) => res.status(400).send({
        message: err,
      }));
  },
};


export default TemplateAntiCoagulantController;
