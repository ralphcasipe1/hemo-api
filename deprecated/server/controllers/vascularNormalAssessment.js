import db from '../models';

const { Vascular, VascularNormalAssessment, } = db.sequelize.models;

const VascularNormalAssessmentController = {
  create(req, res) {
    return VascularNormalAssessment
      .bulkCreate(req.body.normal_assess.map((assess) => ({
        vascular_id: req.params.vascularId,
        normal_assess: assess,
        created_by: req.user.user_id,
        updated_by: req.user.user_id,
      })), {
        validate: true,
        benchmark: true,
        returning: true,
      })
      .then((data) => res.status(201).json({
        data,
      }), (err) => res.status(400).json({ message: err.message, }));
  },

  destroy(req, res) {
    return VascularNormalAssessment
      .findOne({
        where: {
          vascular_id: req.params.vascularId,
          normal_assess: req.params.vascularNormalAssessmentId,
        },
      })
      .then((vascularNormalAssessment) => {
        if (!vascularNormalAssessment) {
          return res.status(400).send({
            message: 'No vascular\'s normal assessmente data found',
          });
        }
        return vascularNormalAssessment
          .destroy()
          .then(() => res.status(200).send({
            message: 'Vascular\'s normal assessmente data deleted successfully',
          }), (err) => res.status(400).send({
            message: err.message,
          }));
      });
  },

  list(req, res) {
    return Vascular
      .findById(req.params.vascularId)
      .then((vascular) => {
        if (!vascular) {
          return res.status(404).send({
            message: 'No vascular found',
          });
        }
        return vascular
          .getNormalAssessment()
          .then(() => res.status(200)
            .send({
              data: vascular,
            }), (err) => res.status(400).send({
            message: err.message,
          }));
      });
  },

  select(req, res) {
    return VascularNormalAssessment
      .findById(req.params.vascularId)
      .then((vascularNormalAssessment) => res.status(200)
        .send({
          data: vascularNormalAssessment,
        }), (err) => res.status(400).send({
        message: err.message,
      }));
  },

  update(req, res) {
    return VascularNormalAssessment
      .findOne({
        where: {
          vascular_id: req.params.vascularId,
          normal_assess: req.params.vascularNormalAssessmentId,
        },
      })
      .then((normal) => {
        if (!normal) {
          return res.status(404).send({
            message: 'No normal assessment found',
          });
        }
        return normal
          .set(normal.vascular.vascular_normal_assessment_id, {
            through: {
              updated_by: req.user.user_id,
            },
          })
          .then(
            (data) =>
              res
                .status(200)
                .send({ data, }),
            (err) =>
              res
                .status(400)
                .send({ message: err.message, })
          );
      }, (err) => res.status(400).send({ message: err.message, }));

    /* return VascularNormalAssessment
            .findById(req.params.vascularNormalAssessmentId)
            .then(
                vascularNormalAssessment => {
                    if(!vascularNormalAssessment) {
                        return res.status(404).send({
                            message: 'No vascular\'s normal assessment data found'
                        });
                    }

                    return vascularNormalAssessment
                        .update(req.body, {fields: Object.keys(req.body)})
                        .then(
                            () => res.status(200).send({data: vascularNormalAssessment}),
                            err => res.status(400).send({
                                message: err.message
                            })
                        );
                },
                err => res.status(400).send({
                    message: err.message
                })
            ); */
  },
};

export default VascularNormalAssessmentController;
