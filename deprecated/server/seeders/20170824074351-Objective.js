export function up(queryInterface) {
  return queryInterface.bulkInsert(
    'objectives',
    [
      {
        name: 'Altered LOC',
        created_by: 1,
        updated_by: 1,
        created_at: 'NOW()',
        updated_at: 'NOW()',
      },
      {
        name: 'Pallor',
        created_by: 1,
        updated_by: 1,
        created_at: 'NOW()',
        updated_at: 'NOW()',
      },
      {
        name: 'Fever',
        created_by: 1,
        updated_by: 1,
        created_at: 'NOW()',
        updated_at: 'NOW()',
      },
      {
        name: 'Chills',
        created_by: 1,
        updated_by: 1,
        created_at: 'NOW()',
        updated_at: 'NOW()',
      },
      {
        name: 'Ascites',
        created_by: 1,
        updated_by: 1,
        created_at: 'NOW()',
        updated_at: 'NOW()',
      },
      {
        name: 'Vomiting',
        created_by: 1,
        updated_by: 1,
        created_at: 'NOW()',
        updated_at: 'NOW()',
      },
      {
        name: 'Murmur',
        created_by: 1,
        updated_by: 1,
        created_at: 'NOW()',
        updated_at: 'NOW()',
      },
      {
        name: 'Crackles',
        created_by: 1,
        updated_by: 1,
        created_at: 'NOW()',
        updated_at: 'NOW()',
      },
      {
        name: 'Wheezing',
        created_by: 1,
        updated_by: 1,
        created_at: 'NOW()',
        updated_at: 'NOW()',
      },
      {
        name: 'Orthopnea',
        created_by: 1,
        updated_by: 1,
        created_at: 'NOW()',
        updated_at: 'NOW()',
      },
    ],
    {}
  );
}
export function down(queryInterface) {
  return queryInterface.bulkDelete('objectives', null, {});
}
