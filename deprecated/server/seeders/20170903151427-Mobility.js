export function up(queryInterface) {
  return queryInterface.bulkInsert(
    'mobilities',
    [
      {
        name: 'Stretcher / Bed',
        created_by: 1,
        updated_by: 1,
        created_at: 'NOW()',
        updated_at: 'NOW()',
      },
      {
        name: 'Ambulatory',
        created_by: 1,
        updated_by: 1,
        created_at: 'NOW()',
        updated_at: 'NOW()',
      },
      {
        name: 'Wheelchair',
        created_by: 1,
        updated_by: 1,
        created_at: 'NOW()',
        updated_at: 'NOW()',
      },
    ],
    {}
  );
}
export function down(queryInterface) {
  return queryInterface.bulkDelete('mobilities', null, {});
}
