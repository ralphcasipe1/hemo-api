export function up(queryInterface) {
  return queryInterface.bulkInsert(
    'abnormals',
    [
      {
        name: 'Pain',
        created_by: 1,
        updated_by: 1,
        created_at: 'NOW()',
        updated_at: 'NOW()',
      },
      {
        name: 'Tenderness',
        created_by: 1,
        updated_by: 1,
        created_at: 'NOW()',
        updated_at: 'NOW()',
      },
      {
        name: 'Hematoma',
        created_by: 1,
        updated_by: 1,
        created_at: 'NOW()',
        updated_at: 'NOW()',
      },
      {
        name: 'Warmth',
        created_by: 1,
        updated_by: 1,
        created_at: 'NOW()',
        updated_at: 'NOW()',
      },
      {
        name: 'Erythma',
        created_by: 1,
        updated_by: 1,
        created_at: 'NOW()',
        updated_at: 'NOW()',
      },
      {
        name: 'Abscess',
        created_by: 1,
        updated_by: 1,
        created_at: 'NOW()',
        updated_at: 'NOW()',
      },
    ],
    {}
  );
}
export function down(queryInterface) {
  return queryInterface.bulkDelete('abnormals', null, {});
}
