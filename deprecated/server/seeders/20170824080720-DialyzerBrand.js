export function up(queryInterface) {
  return queryInterface.bulkInsert(
    'dialyzer_brands',
    [
      {
        brand_name: 'Fresenius',
        created_by: 1,
        updated_by: 1,
        created_at: 'NOW()',
        updated_at: 'NOW()',
      },
      {
        brand_name: 'BBraun',
        created_by: 1,
        updated_by: 1,
        created_at: 'NOW()',
        updated_at: 'NOW()',
      },
      {
        brand_name: 'Diacap Pro',
        created_by: 1,
        updated_by: 1,
        created_at: 'NOW()',
        updated_at: 'NOW()',
      },
    ],
    {}
  );
}
export function down(queryInterface) {
  return queryInterface.bulkDelete('dialyzer_brands', null, {});
}
