export function up(queryInterface) {
  return queryInterface.bulkInsert(
    'users',
    [
      {
        fname: 'Super',
        lname: 'Admin',
        id_number: '006436',
        password: 'test098!?',
        is_admin: true,
        is_doctor: true,
        created_by: 1,
        updated_by: 1,
        created_at: 'NOW()',
        updated_at: 'NOW()',
      },
      {
        fname: 'Nikki',
        lname: 'Udani',
        id_number: '003932',
        password: '003932',
        is_admin: true,
        is_doctor: false,
        created_by: 1,
        updated_by: 1,
        created_at: 'NOW()',
        updated_at: 'NOW()',
      },
      {
        fname: 'Manuel II',
        lname: 'Jonelas',
        id_number: '003779',
        password: '003779',
        is_admin: true,
        is_doctor: false,
        created_by: 1,
        updated_by: 1,
        created_at: 'NOW()',
        updated_at: 'NOW()',
      },
    ],
    {}
  );
}
export function down(queryInterface) {
  return queryInterface.bulkDelete('users', null, {});
}
