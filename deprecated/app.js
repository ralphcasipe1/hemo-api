import compression from 'compression';
import bodyParser from 'body-parser';
import cors from 'cors';
import express from 'express';
import jwt from 'jsonwebtoken';
import morgan from 'morgan';
import routes from './server/routes';

const app = express();
const router = express.Router();

app.use(compression());

app.use(morgan('dev'));

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: false, }));

app.use(cors());

app.use((request, response, next) => {
  if (request.headers && request.headers.authorization) {
    jwt.verify(request.headers.authorization.split(' ')[0], 'unaKonsulta888!?', (error, decode) => {
      if (error) request.user = undefined;
      request.user = decode;
      next();
    });
  } else {
    request.user = undefined;
    next();
  }
});

routes(app, router);

app
  .get('/', (_, response) =>
    response
      .status(200)
      .send({message: 'Welcome to the root'})
    );

app
  .get('/api', (_, response) =>
    response
      .status(200)
      .send({
        message: 'Welcome to the REST API of Hemodialysis' })
  );

export default app;
