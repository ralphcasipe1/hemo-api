import expect from 'expect.js'

describe('Vaccine model', () => {
  it('should return the vaccine model', () => {
    const models = require('../../../../../src/models')
    expect(models.Vaccine).to.be.ok()
  })
})
