import expect from 'expect.js'

describe('Dialyzer model', () => {
  it('should return the dialyzer model', () => {
    const models = require('../../../../../src/models')
    expect(models.Dialyzer).to.be.ok()
  })
})
