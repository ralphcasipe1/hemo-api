import expect from 'expect.js'

describe('Parameter model', () => {
  it('should return the parameter model', () => {
    const models = require('../../../../../src/models')
    expect(models.Parameter).to.be.ok()
  })
})
