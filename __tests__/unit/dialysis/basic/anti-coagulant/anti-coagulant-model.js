import expect from 'expect.js'

describe('Anti-Coagulant model', () => {
  it('should return the anti-coagulant model', () => {
    const models = require('../../../../../src/models')
    expect(models.AntiCoagulant).to.be.ok()
  })
})
