'use strict'

var expect = require('expect.js')

describe('Patient controller', function () {
  before(function () {
    return require('../../../src/models').sequelize.sync()
  })

  beforeEach(function () {
  	this.Patient = require('../../../src/models').Patient
  })

  after(function (done) {
  	require('../../../src/models').sequelize.sync({ force: true })
  })

  it('POST / patients', function (done) {
  	return this.Patient
  		.create({
	  		bizbox_patient_no: '22331',
	  		first_name: 'John',
	  		middle_name: 'Johnny',
	  		last_name: 'Doe',
	  		age: 23,
	  		sex: 1,
	  		civil_status: 1,
	  		room_no: '440-1',
	  		hospital_no: '229-11-102',
	  		attending_physician: 'Don Juan Marquez',
	  		created_by: 23,
	  		updated_by: 23
  		})
  		.then(function (patient) {
  			expect(patient).to.be.an('object')
  			done()
      })
  })

  it('GET /patients', function (done) {
    return this.Patient.findAll()
      .then(function (patients) {
        expect(patients)
          .to.be.an('array')
        done()
      })
      .catch(error => error)
  })

  it('GET /patients/:patientId', function (done) {
    return this.Patient.findById(1)
      .then(function () {
        expect(patient)
          .to.be.an('array')
        done()
      })
      .catch(error => error)
  })
})
/* import expect from 'expect.js'

describe('Patient controller', () => {
  let Patient
  before(() => {
    require('../../../src/models').sequelize.sync()
  })

  beforeEach(() => {
  	Patient = require('../../../src/models').Patient
  })

  it('GET /patients', () => {
    Patient.findAll()
      .then(patients => {
      	console.log(patients)
        expect(patients)
          .to.be.an('array')
      })
  })

  it('GET /patients/:patientId', () => {
  	const Patient = require('../../../src/models').Patient
    Patient.findById(1)
      .then(patient => {
      	console.log(patient)
        expect(patient)
          .to.be.an('array')
      })
      .catch(error => expect(error.message).to.be.an('array'))
  })
})
*/
