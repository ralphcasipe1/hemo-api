import expect from 'expect.js'

describe('Patient model', () => {
  const Patient = require('../../../src/models').Patient

  it('should return the patient model', () => {
    expect(Patient).to.be.ok()
  })

 	it('table name is "patients"', () => {
  	expect(Patient.tableName).to.equal('patients')
  })
})
