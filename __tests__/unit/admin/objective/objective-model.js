import expect from 'expect.js'

describe('Objective model', () => {
  it('should return the objective model', () => {
    const models = require('../../../../src/models')
    expect(models.Objective).to.be.ok()
  })
})
