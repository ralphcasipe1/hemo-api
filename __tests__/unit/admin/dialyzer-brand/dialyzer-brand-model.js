import expect from 'expect.js'

describe('Dialyzer brand model', () => {
  it('should return the dialyzer brand model', () => {
    const models = require('../../../../src/models')
    expect(models.DialyzerBrand).to.be.ok()
  })
})
