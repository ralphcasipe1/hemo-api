import expect from 'expect.js'

describe('Subjective model', () => {
  it('should return the subjective model', () => {
    const models = require('../../../../src/models')
    expect(models.Subjective).to.be.ok()
  })
})
