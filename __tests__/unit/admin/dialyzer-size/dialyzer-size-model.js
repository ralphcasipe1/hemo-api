import expect from 'expect.js'

describe('Dialyzer size model', () => {
  it('should return the dialyzer size model', () => {
    const models = require('../../../../src/models')
    expect(models.DialyzerSize).to.be.ok()
  })
})
