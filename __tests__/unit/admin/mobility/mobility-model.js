import expect from 'expect.js'

describe('Mobility model', () => {
  it('should return the mobility model', () => {
    const models = require('../../../../src/models')
    expect(models.Mobility).to.be.ok()
  })
})
