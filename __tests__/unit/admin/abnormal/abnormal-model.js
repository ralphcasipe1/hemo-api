import expect from 'expect.js'

describe('Abnormal model', () => {
  it('should return the abnormal model', () => {
    const models = require('../../../../src/models')
    expect(models.Abnormal).to.be.ok()
  })
})
