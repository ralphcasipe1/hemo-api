import expect from 'expect.js'

describe('Standing order Parameter model', () => {
  const models = require('../../../../src/models')

  it('should return the standing order parameter model', () => {
    expect(models.TemplateParameter).to.be.ok()
  })

  it('table name is "template_parameters"', () => {
  	expect(models.TemplateParameter.tableName).to.equal('template_parameters')
  })
})
