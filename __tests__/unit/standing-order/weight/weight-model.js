import expect from 'expect.js'

describe('Standing order Weight model', () => {
  const models = require('../../../../src/models')

  it('should return the standing order weight model', () => {
    expect(models.TemplateWeight).to.be.ok()
  })

  it('table name is "template_weights"', () => {
  	expect(models.TemplateWeight.tableName).to.equal('template_weights')
  })
})
