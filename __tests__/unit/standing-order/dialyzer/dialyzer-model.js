import expect from 'expect.js'

describe('Standing order Dialyzer model', () => {
  const models = require('../../../../src/models')

  it('should return the standing order dialyzer model', () => {
    const models = require('../../../../src/models')
    expect(models.TemplateDialyzer).to.be.ok()
  })

  it('table name is "template_dialyzers"', () => {
  	expect(models.TemplateDialyzer.tableName).to.equal('template_dialyzers')
  })
})
