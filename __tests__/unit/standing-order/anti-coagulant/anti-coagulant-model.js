import expect from 'expect.js'

describe('Standing order Anti-Coagulant model', () => {
  const models = require('../../../../src/models')

  it('should return the standing order anti-coagulant model', () => {
    expect(models.TemplateAntiCoagulant).to.be.ok()
  })

  it('table name is "template_anti_coagulants"', () => {
  	expect(models.TemplateAntiCoagulant.tableName).to.equal('template_anti_coagulants')
  })
})
