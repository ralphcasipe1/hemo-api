import expect from 'expect.js'

describe('Standing order Medication model', () => {
  const models = require('../../../../src/models')

  it('should return the standing order medication model', () => {
    expect(models.TemplateMedication).to.be.ok()
  })

  it('table name is "template_medications"', () => {
  	expect(models.TemplateMedication.tableName).to.equal('template_medications')
  })
})
